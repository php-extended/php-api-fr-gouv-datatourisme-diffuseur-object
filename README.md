# php-extended/php-api-fr-gouv-datatourisme-diffuseur-object

A library that implements the php-extended/php-api-fr-gouv-datatourisme-diffuseur-interface library.

![coverage](https://gitlab.com/php-extended/php-api-fr-gouv-datatourisme-diffuseur-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-gouv-datatourisme-diffuseur-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-gouv-datatourisme-diffuseur-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\DatatourismeDiffuseurApi\DtDiffApiEndpoint;
use PhpExtended\Endpoint\HttpEndpoint;

/** @var $client \Psr\Http\Client\ClientInterface */
// note, the client or the endpoint must do gzip
// uncompressing when retrieving the file. You may use the
// \PhpExtended\Endpoint\GzipHttpEndpoint as wrapper around
// the HttpEndpoint to do the job if the client does not.

// note2 : the client must support the X-Php-Download-File
// custm header to specify a temporary location, or do gzip
// uncompressing on the fly (as the following unzipping is
// done by this endpoint on the temp folder and the php-zip
// library needs a file to operate).

$endpoint = new DtDiffApiEndpoint('/tmp/', new GzipHttpEndpoint(new HttpEndpoint($client)));

$iterator = $endpoint->downloadFlux($fluxKey, $apiKey);

foreach($iterator as $line)
{
	try
	{
		$poi = $iterator->getPointOfInterestData($resume);
		// do something with poi object
	}
	catch(\Throwable $exc)
	{
		// do something in case object cannot be read
	}
}

```


## License

MIT (See [license file](LICENSE)).
