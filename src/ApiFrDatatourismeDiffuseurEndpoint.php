<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use InvalidArgumentException;
use PhpExtended\DataProvider\JsonFileDataProvider;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Email\EmailAddress;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use PhpExtended\Reifier\ReifierReportInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;
use RuntimeException;
use ZipArchive;

/**
 * ApiFrDatatourismeDiffuseurEndpoint class file.
 * 
 * This class is a simple implementation of the ApiFrDatatourismeDiffuseurEndpointInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiFrDatatourismeDiffuseurEndpoint implements ApiFrDatatourismeDiffuseurEndpointInterface
{
	
	public const HOST = 'https://diffuseur.datatourisme.fr/';
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * The path for temp files.
	 * 
	 * @var string
	 */
	protected string $_tempPath;
	
	/**
	 * Builds a new Endpoint with its dependancies.
	 *
	 * @param string $tempPath
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 * @throws InvalidArgumentException
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function __construct(
		string $tempPath,
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		
		$realPath = \realpath($tempPath);
		if(false === $realPath)
		{
			// @codeCoverageIgnoreStart
			$message = 'Path does not exist : {path}';
			$context = ['{path}' => $tempPath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		if(!\is_dir($realPath))
		{
			// @codeCoverageIgnoreStart
			$message = 'The path does not point to a directory : {path}';
			$context = ['{path}' => $realPath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		if(!\is_writable($realPath))
		{
			// @codeCoverageIgnoreStart
			$message = 'The directory at path is not writeable : {path}';
			$context = ['{path}' => $realPath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$this->_tempPath = $realPath;
		
		$conf = $this->_reifier->getConfiguration();
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAddress::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAddress::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAddress::class, 'schemaAddressLocality', 'schema:addressLocality');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAddress::class, 'schemaPostOfficeBoxNumber', 'schema:postOfficeBoxNumber');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAddress::class, 'schemaPostalCode', 'schema:postalCode');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAddress::class, 'schemaStreetAddress', 'schema:streetAddress');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAddress::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAddress::class, 'schema:streetAddress', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'dcIdentifier', 'dc:identifier');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'rdfsComment', 'rdfs:comment');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'schemaAddress', 'schema:address');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'schemaEmail', 'schema:email');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'schemaFamilyName', 'schema:familyName');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'schemaFaxNumber', 'schema:faxNumber');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'schemaGender', 'schema:gender');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'schemaGivenName', 'schema:givenName');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'schemaLegalName', 'schema:legalName');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'schemaTelephone', 'schema:telephone');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'schemaLogo', 'schema:logo');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'foafHomepage', 'foaf:homepage');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAgent::class, 'foafTitle', 'foaf:title');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAgent::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAgent::class, 'schema:address', ApiFrDatatourismeDiffuseurAddress::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAgent::class, 'schema:email', EmailAddress::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAgent::class, 'schema:familyName', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAgent::class, 'schema:faxNumber', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAgent::class, 'schema:givenName', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAgent::class, 'schema:telephone', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAgent::class, 'foaf:homepage', UriInterface::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAgent::class, 'foaf:title', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAgent::class, 'hasTranslatedProperty', ApiFrDatatourismeDiffuseurTranslatedProperty::class);
		$conf->addFieldAllowedToFail(ApiFrDatatourismeDiffuseurAgent::class, 'schema:email');
		$conf->addFieldAllowedToFail(ApiFrDatatourismeDiffuseurAgent::class, 'foaf:homepage');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAnnotation::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAnnotation::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAnnotation::class, 'ebucoreAbstract', 'ebucore:abstract');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAnnotation::class, 'ebucoreComments', 'ebucore:comments');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAnnotation::class, 'ebucoreIsCoveredBy', 'ebucore:isCoveredBy');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAnnotation::class, 'ebucoreTitle', 'ebucore:title');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAnnotation::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAnnotation::class, 'credits', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAnnotation::class, 'hasTranslatedProperty', ApiFrDatatourismeDiffuseurTranslatedProperty::class);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurAnnotation::class, 'rightsEndDate', ['Y-m-d']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurAnnotation::class, 'rightsStartDate', ['Y-m-d']);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAudience::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAudience::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurAudience::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurAudience::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurCity::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurCity::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurCity::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurCity::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurCountry::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurCountry::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurCountry::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurCountry::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurDayOfWeek::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurDayOfWeek::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurDayOfWeek::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurDayOfWeek::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurDepartment::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurDepartment::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurDepartment::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurDepartment::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurDescription::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurDescription::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurDescription::class, 'dcDescription', 'dc:description');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurDescription::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurDescription::class, 'hasTranslatedProperty', ApiFrDatatourismeDiffuseurTranslatedProperty::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurDescription::class, 'isDedicatedTo', ApiFrDatatourismeDiffuseurAudience::class);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurFeature::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurFeature::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurFeature::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurFeature::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurFeatureSpecification::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurFeatureSpecification::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurFeatureSpecification::class, 'schemaValue', 'schema:value');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurFeatureSpecification::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurFeatureSpecification::class, 'features', ApiFrDatatourismeDiffuseurFeature::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurFeatureSpecification::class, 'hasLayout', ApiFrDatatourismeDiffuseurLayout::class);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGender::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGender::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGender::class, 'rdfsComment', 'rdfs:comment');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGender::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurGender::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurGender::class, 'rdfs:label', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGeo::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGeo::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGeo::class, 'schemaElevation', 'schema:elevation');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGeo::class, 'schemaLatitude', 'schema:latitude');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGeo::class, 'schemaLine', 'schema:line');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGeo::class, 'schemaLongitude', 'schema:longitude');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurGeo::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGeographicReach::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGeographicReach::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurGeographicReach::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurGeographicReach::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurLayout::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurLayout::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurLayout::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurLayout::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurLocation::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurLocation::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurLocation::class, 'schemaAddress', 'schema:address');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurLocation::class, 'schemaGeo', 'schema:geo');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurLocation::class, 'schemaOpeningHoursSpecification', 'schema:openingHoursSpecification');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurLocation::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurLocation::class, 'schema:address', ApiFrDatatourismeDiffuseurAddress::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurLocation::class, 'schema:openingHoursSpecification', ApiFrDatatourismeDiffuseurOpeningHours::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurLocation::class, 'altInsee', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurMedia::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurMedia::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurMedia::class, 'ebucoreHasAnnotation', 'ebucore:hasAnnotation');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurMedia::class, 'ebucoreHasRelatedResource', 'ebucore:hasRelatedResource');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurMedia::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurMedia::class, 'ebucore:hasAnnotation', ApiFrDatatourismeDiffuseurAnnotation::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurMedia::class, 'ebucore:hasRelatedResource', ApiFrDatatourismeDiffuseurResource::class);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurMimeType::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurMimeType::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurMimeType::class, 'rdfsLabel', 'rdfs:label');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurMimeType::class, 'rdfsComment', 'rdfs:comment');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurMimeType::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurMimeType::class, 'rdfs:label', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOffer::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOffer::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOffer::class, 'schemaAcceptedPaymentMethod', 'schema:acceptedPaymentMethod');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOffer::class, 'schemaPriceSpecification', 'schema:priceSpecification');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOffer::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOffer::class, 'schema:acceptedPaymentMethod', ApiFrDatatourismeDiffuseurPaymentMethod::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOffer::class, 'schema:priceSpecification', ApiFrDatatourismeDiffuseurPriceSpecification::class);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOpeningHours::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOpeningHours::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOpeningHours::class, 'schemaCloses', 'schema:closes');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOpeningHours::class, 'schemaDayOfWeek', 'schema:dayOfWeek');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOpeningHours::class, 'schemaOpens', 'schema:opens');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOpeningHours::class, 'schemaValidFrom', 'schema:validFrom');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOpeningHours::class, 'schemaValidThrough', 'schema:validThrough');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOpeningHours::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOpeningHours::class, 'schema:dayOfWeek', ApiFrDatatourismeDiffuseurDayOfWeek::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOpeningHours::class, 'hasTranslatedProperty', ApiFrDatatourismeDiffuseurTranslatedProperty::class);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurOpeningHours::class, 'schema:closes', ['H:i:s']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurOpeningHours::class, 'schema:opens', ['H:i:s']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurOpeningHours::class, 'schema:validFrom', ['Y-m-d\\TH:i:s']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurOpeningHours::class, 'schema:validThrough', ['Y-m-d\\TH:i:s']);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOrderedListItem::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOrderedListItem::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOrderedListItem::class, 'dcDescription', 'dc:description');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOrderedListItem::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOrderedListItem::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOrderedListItem::class, 'isLocatedAt', ApiFrDatatourismeDiffuseurLocation::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOrderedListItem::class, 'hasMainRepresentation', ApiFrDatatourismeDiffuseurMedia::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOrderedListItem::class, 'hasRepresentation', ApiFrDatatourismeDiffuseurMedia::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOrderedListItem::class, 'hasTranslatedProperty', ApiFrDatatourismeDiffuseurTranslatedProperty::class);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOrderedListSlot::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOrderedListSlot::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOrderedListSlot::class, 'oloIndex', 'olo:index');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurOrderedListSlot::class, 'oloItem', 'olo:item');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurOrderedListSlot::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPaymentMethod::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPaymentMethod::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPaymentMethod::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPaymentMethod::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPeriod::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPeriod::class, 'type', '@type');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPeriod::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPeriod::class, 'appliesOnDay', ApiFrDatatourismeDiffuseurDayOfWeek::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPeriod::class, 'hasTranslatedProperty', ApiFrDatatourismeDiffuseurTranslatedProperty::class);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurPeriod::class, 'endDate', ['Y-m-d']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurPeriod::class, 'endTime', ['H:i:s']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurPeriod::class, 'startDate', ['Y-m-d']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurPeriod::class, 'startTime', ['H:i:s']);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'dcIdentifier', 'dc:identifier');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'oloSlot', 'olo:slot');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'owlSameAs', 'owl:sameAs');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'schemaEndDate', 'schema:endDate');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'schemaStartDate', 'schema:startDate');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'rdfsComment', 'rdfs:comment');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'rdfsLabel', 'rdfs:label');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'covid19InOperationConfirmed', 'COVID19InOperationConfirmed');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'covid19OpeningPeriodsConfirmed', 'COVID19OpeningPeriodsConfirmed');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'covid19SpecialMeasures', 'COVID19SpecialMeasures');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'olo:slot', ApiFrDatatourismeDiffuseurOrderedListSlot::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'owl:sameAs', UriInterface::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'schema:endDate', DateTimeInterface::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'schema:startDate', DateTimeInterface::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'availableLanguage', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasAdministrativeContact', ApiFrDatatourismeDiffuseurAgent::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasAudience', ApiFrDatatourismeDiffuseurAudience::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasArchitecturalStyle', ApiFrDatatourismeDiffuseurTheme::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasBeenPublishedBy', ApiFrDatatourismeDiffuseurAgent::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasBookingContact', ApiFrDatatourismeDiffuseurAgent::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasCommunicationContact', ApiFrDatatourismeDiffuseurAgent::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasClientTarget', ApiFrDatatourismeDiffuseurAudience::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasContact', ApiFrDatatourismeDiffuseurAgent::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasDescription', ApiFrDatatourismeDiffuseurDescription::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasFeature', ApiFrDatatourismeDiffuseurFeatureSpecification::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasGeographicReach', ApiFrDatatourismeDiffuseurGeographicReach::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasMainRepresentation', ApiFrDatatourismeDiffuseurMedia::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasManagementContact', ApiFrDatatourismeDiffuseurAgent::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasNeighborhood', ApiFrDatatourismeDiffuseurTheme::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasRepresentation', ApiFrDatatourismeDiffuseurMedia::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasReview', ApiFrDatatourismeDiffuseurReview::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasTheme', ApiFrDatatourismeDiffuseurTheme::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasTourType', ApiFrDatatourismeDiffuseurTourType::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'hasTranslatedProperty', ApiFrDatatourismeDiffuseurTranslatedProperty::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'isLocatedAt', ApiFrDatatourismeDiffuseurLocation::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'isOwnedBy', ApiFrDatatourismeDiffuseurAgent::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'offers', ApiFrDatatourismeDiffuseurOffer::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'providesCuisineOfType', ApiFrDatatourismeDiffuseurTheme::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'providesFoodProduct', ApiFrDatatourismeDiffuseurTheme::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'takesPlaceAt', ApiFrDatatourismeDiffuseurPeriod::class);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'schema:endDate', ['Y-m-d']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'schema:startDate', ['Y-m-d']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'creationDate', ['Y-m-d']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurPointOfInterest::class, 'lastUpdateDatatourisme', [
			'Y-m-d\\TH:i:s.v\\Z',
			'Y-m-d\\TH:i:s.???\\Z',
			'Y-m-d\\TH:i:s.??\\Z',
			'Y-m-d\\TH:i:s.?\\Z',
			'Y-m-d\\TH:i:s\\Z',
		]);
		
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPointOfInterestIterator::class, 'data', ApiFrDatatourismeDiffuseurPointOfInterestResume::class);
		
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurPointOfInterestResume::class, 'lastUpdateDatatourisme', [
			'Y-m-d\\TH:i:s.v\\Z',
			'Y-m-d\\TH:i:s.???\\Z',
			'Y-m-d\\TH:i:s.??\\Z',
			'Y-m-d\\TH:i:s.?\\Z',
			'Y-m-d\\TH:i:s\\Z',
		]);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'schemaEligibleQuantity', 'schema:eligibleQuantity');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'schemaMaxPrice', 'schema:maxPrice');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'schemaMinPrice', 'schema:minPrice');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'schemaPrice', 'schema:price');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'schemaPriceCurrency', 'schema:priceCurrency');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'schema:maxPrice', 'float');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'schema:minPrice', 'float');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'hasAudience', ApiFrDatatourismeDiffuseurAudience::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'hasEligibleAudience', ApiFrDatatourismeDiffuseurAudience::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'hasPricingMode', ApiFrDatatourismeDiffuseurPricingMode::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'hasPricingOffer', ApiFrDatatourismeDiffuseurPricingOffer::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'hasPricingSeason', ApiFrDatatourismeDiffuseurPricingSeason::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'hasTranslatedProperty', ApiFrDatatourismeDiffuseurTranslatedProperty::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPriceSpecification::class, 'appliesOnPeriod', ApiFrDatatourismeDiffuseurPeriod::class);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingMode::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingMode::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingMode::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPricingMode::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingOffer::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingOffer::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingOffer::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPricingOffer::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingPolicy::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingPolicy::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingPolicy::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPricingPolicy::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingSeason::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingSeason::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurPricingSeason::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurPricingSeason::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurQuantitativeValue::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurQuantitativeValue::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurQuantitativeValue::class, 'schemaMaxValue', 'schema:maxValue');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurQuantitativeValue::class, 'schemaUnitText', 'schema:unitText');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurQuantitativeValue::class, 'schemaValue', 'schema:value');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurQuantitativeValue::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurQuantitativeValue::class, 'schema:maxValue', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurQuantitativeValue::class, 'schema:unitText', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurRegion::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurRegion::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurRegion::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurRegion::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurResource::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurResource::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurResource::class, 'ebucoreFileSize', 'ebucore:fileSize');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurResource::class, 'ebucoreHasMimeType', 'ebucore:hasMimeType');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurResource::class, 'ebucoreHeight', 'ebucore:height');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurResource::class, 'ebucoreHeightUnit', 'ebucore:heightUnit');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurResource::class, 'ebucoreLocator', 'ebucore:locator');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurResource::class, 'ebucoreWidth', 'ebucore:width');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurResource::class, 'ebucoreWidthUnit', 'ebucore:widthUnit');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurResource::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurResource::class, 'ebucore:fileSize', 'float');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurResource::class, 'ebucore:hasMimeType', ApiFrDatatourismeDiffuseurMimeType::class);
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurResource::class, 'ebucore:locator', UriInterface::class);
		$conf->addFieldAllowedToFail(ApiFrDatatourismeDiffuseurResource::class, 'ebucore:locator');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurReview::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurReview::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurReview::class, 'schemaDatePublished', 'schema:datePublished');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurReview::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurReview::class, 'schema:datePublished', DateTimeInterface::class);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurReview::class, 'reviewDeliveryDate', ['Y-m-d']);
		$conf->addDateTimeFormat(ApiFrDatatourismeDiffuseurReview::class, 'reviewExpirationDate', ['Y-m-d']);
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurReviewValue::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurReviewValue::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurReviewValue::class, 'schemaRatingValue', 'schema:ratingValue');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurReviewValue::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurReviewValue::class, 'type', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurReviewValue::class, 'isCompliantWith', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTheme::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTheme::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTheme::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurTheme::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTourType::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTourType::class, 'type', '@type');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTourType::class, 'rdfsLabel', 'rdfs:label');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurTourType::class, 'type', 'string');
		
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTranslatedProperty::class, 'id', '@id');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTranslatedProperty::class, 'dcContributor', 'dc:contributor');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTranslatedProperty::class, 'rdfLanguage', 'rdf:language');
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTranslatedProperty::class, 'rdfPredicate', 'rdf:predicate');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurTranslatedProperty::class, 'dc:contributor', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurTranslatedProperty::class, 'rdf:language', 'string');
		$conf->setIterableInnerType(ApiFrDatatourismeDiffuseurTranslatedProperty::class, 'rdf:predicate', 'string');
		
		// when given as an array [0 => string], map on the fr key
		$conf->addFieldNameAlias(ApiFrDatatourismeDiffuseurTranslatedText::class, 'fr', '0');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurEndpointInterface::downloadFlux()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	public function downloadFlux(string $fluxKey, string $apiKey, bool $useCache = false, ?ReifierReportInterface $report = null) : ApiFrDatatourismeDiffuseurPointOfInterestIteratorInterface
	{
		$shouldDownload = true;
		$indexPath = $this->_tempPath.'/index.json';
		
		if($useCache && \is_file($indexPath))
		{
			$wtime = \filemtime($indexPath);
			/** @psalm-suppress PossiblyFalseArgument */
			$halfdayago = (new DateTimeImmutable())->add(DateInterval::createFromDateString('-12 hours'));
			if(false !== $wtime && $halfdayago->getTimestamp() < $wtime)
			{
				$shouldDownload = false;
			}
		}
		
		if($shouldDownload)
		{
			$this->cleanupDownloadDirectory();
			
			$uri = $this->_uriFactory->createUri(self::HOST.'webservice/'.$fluxKey.'/'.$apiKey);
			$request = $this->_requestFactory->createRequest('GET', $uri);
			$request = $request->withAddedHeader('X-Php-Download-File', $this->_tempPath.'/flux-'.\date('Y-m-d').'.gz');
			$response = $this->_httpClient->sendRequest($request);
			
			$path = $response->getHeaderLine('X-Php-Uncompressed-File');
			if(empty($path))
			{
				// @codeCoverageIgnoreStart
				$message = 'Failed to find downloaded file path (missing header {header})';
				$context = ['{header}' => 'X-Php-Uncompressed-File'];
				
				throw new RuntimeException(\strtr($message, $context));
				// @codeCoverageIgnoreEnd
			}
			
			$zip = new ZipArchive();
			$err = $zip->open($path);
			if(true !== $err)
			{
				// @codeCoverageIgnoreStart
				$message = 'Failed to open resource as zip file at {path} (err {nb})';
				$context = ['{path}' => $path, '{nb}' => $err];
				
				throw new RuntimeException(\strtr($message, $context));
				// @codeCoverageIgnoreEnd
			}
			$zip->extractTo($this->_tempPath);
			$zip->close();
			unset($zip);
			\unlink($path);
		}
		
		$iterator = (new JsonFileDataProvider($indexPath))->provideIterator();
		
		$data = null === $report 
			? $this->_reifier->reifyIterator(ApiFrDatatourismeDiffuseurPointOfInterestResume::class, $iterator)
			: $this->_reifier->tryReifyIterator(ApiFrDatatourismeDiffuseurPointOfInterestResume::class, $iterator, $report);
		
		return new ApiFrDatatourismeDiffuseurPointOfInterestIterator(
			$this->_tempPath,
			$this->_reifier,
			$this->_reifier->getConfiguration(),
			$data,
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurEndpointInterface::cleanupDownloadDirectory()
	 */
	public function cleanupDownloadDirectory() : int
	{
		return $this->cleanupDirectoryRecursive($this->_tempPath);
	}
	
	/**
	 * Removes all files and directories for a given directory.
	 * 
	 * @param string $fullPath
	 * @return integer the count of files removed
	 * @throws RuntimeException if something cannot be removed
	 */
	protected function cleanupDirectoryRecursive(string $fullPath) : int
	{
		$count = 0;
		
		$paths = \scandir($fullPath);
		if(false === $paths)
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to scan path at {path}';
			$context = ['{path}' => $fullPath];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		foreach($paths as $pathName)
		{
			if('.' === $pathName || '..' === $pathName)
			{
				continue;
			}
			
			$newFullPath = $fullPath.\DIRECTORY_SEPARATOR.$pathName;
			
			if(\is_dir($newFullPath))
			{
				$count += $this->cleanupDirectoryRecursive($newFullPath);
				$res = \rmdir($newFullPath);
				if(false === $res)
				{
					// @codeCoverageIgnoreStart
					$message = 'Failed to remove directory {path}';
					$context = ['{path}' => $newFullPath];
					
					throw new RuntimeException(\strtr($message, $context));
					// @codeCoverageIgnoreEnd
				}
			}
			
			if(\is_file($newFullPath))
			{
				$res = \unlink($newFullPath);
				if(false === $res)
				{
					// @codeCoverageIgnoreStart
					$message = 'Failed to remove file {path}';
					$context = ['{path}' => $newFullPath];
					
					throw new RuntimeException(\strtr($message, $context));
					// @codeCoverageIgnoreEnd
				}
				$count++;
			}
		}
		
		return $count;
	}
	
}
