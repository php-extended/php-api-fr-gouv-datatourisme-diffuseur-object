<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurGeo class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurGeoInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurGeo implements ApiFrDatatourismeDiffuseurGeoInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The elevation.
	 * 
	 * @var ?float
	 */
	protected ?float $_schemaElevation = null;
	
	/**
	 * The latitude.
	 * 
	 * @var ?float
	 */
	protected ?float $_schemaLatitude = null;
	
	/**
	 * Gets the url of the line xml file.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_schemaLine = null;
	
	/**
	 * The longitude.
	 * 
	 * @var ?float
	 */
	protected ?float $_schemaLongitude = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurGeo with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurGeoInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurGeoInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurGeoInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurGeoInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the elevation.
	 * 
	 * @param ?float $schemaElevation
	 * @return ApiFrDatatourismeDiffuseurGeoInterface
	 */
	public function setSchemaElevation(?float $schemaElevation) : ApiFrDatatourismeDiffuseurGeoInterface
	{
		$this->_schemaElevation = $schemaElevation;
		
		return $this;
	}
	
	/**
	 * Gets the elevation.
	 * 
	 * @return ?float
	 */
	public function getSchemaElevation() : ?float
	{
		return $this->_schemaElevation;
	}
	
	/**
	 * Sets the latitude.
	 * 
	 * @param ?float $schemaLatitude
	 * @return ApiFrDatatourismeDiffuseurGeoInterface
	 */
	public function setSchemaLatitude(?float $schemaLatitude) : ApiFrDatatourismeDiffuseurGeoInterface
	{
		$this->_schemaLatitude = $schemaLatitude;
		
		return $this;
	}
	
	/**
	 * Gets the latitude.
	 * 
	 * @return ?float
	 */
	public function getSchemaLatitude() : ?float
	{
		return $this->_schemaLatitude;
	}
	
	/**
	 * Sets gets the url of the line xml file.
	 * 
	 * @param ?UriInterface $schemaLine
	 * @return ApiFrDatatourismeDiffuseurGeoInterface
	 */
	public function setSchemaLine(?UriInterface $schemaLine) : ApiFrDatatourismeDiffuseurGeoInterface
	{
		$this->_schemaLine = $schemaLine;
		
		return $this;
	}
	
	/**
	 * Gets gets the url of the line xml file.
	 * 
	 * @return ?UriInterface
	 */
	public function getSchemaLine() : ?UriInterface
	{
		return $this->_schemaLine;
	}
	
	/**
	 * Sets the longitude.
	 * 
	 * @param ?float $schemaLongitude
	 * @return ApiFrDatatourismeDiffuseurGeoInterface
	 */
	public function setSchemaLongitude(?float $schemaLongitude) : ApiFrDatatourismeDiffuseurGeoInterface
	{
		$this->_schemaLongitude = $schemaLongitude;
		
		return $this;
	}
	
	/**
	 * Gets the longitude.
	 * 
	 * @return ?float
	 */
	public function getSchemaLongitude() : ?float
	{
		return $this->_schemaLongitude;
	}
	
}
