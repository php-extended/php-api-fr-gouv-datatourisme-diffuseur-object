<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

/**
 * ApiFrDatatourismeDiffuseurDepartment class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurDepartmentInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurDepartment implements ApiFrDatatourismeDiffuseurDepartmentInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The label of the day of week.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_rdfsLabel = null;
	
	/**
	 * The insee code of the department.
	 * 
	 * @var ?string
	 */
	protected ?string $_insee = null;
	
	/**
	 * The region of the department.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurRegionInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurRegionInterface $_isPartOfRegion = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurDepartment with private members.
	 * 
	 * @param string $id
	 * @param array<int, string> $type
	 */
	public function __construct(string $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param string $id
	 * @return ApiFrDatatourismeDiffuseurDepartmentInterface
	 */
	public function setId(string $id) : ApiFrDatatourismeDiffuseurDepartmentInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurDepartmentInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurDepartmentInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the label of the day of week.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel
	 * @return ApiFrDatatourismeDiffuseurDepartmentInterface
	 */
	public function setRdfsLabel(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel) : ApiFrDatatourismeDiffuseurDepartmentInterface
	{
		$this->_rdfsLabel = $rdfsLabel;
		
		return $this;
	}
	
	/**
	 * Gets the label of the day of week.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsLabel() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_rdfsLabel;
	}
	
	/**
	 * Sets the insee code of the department.
	 * 
	 * @param ?string $insee
	 * @return ApiFrDatatourismeDiffuseurDepartmentInterface
	 */
	public function setInsee(?string $insee) : ApiFrDatatourismeDiffuseurDepartmentInterface
	{
		$this->_insee = $insee;
		
		return $this;
	}
	
	/**
	 * Gets the insee code of the department.
	 * 
	 * @return ?string
	 */
	public function getInsee() : ?string
	{
		return $this->_insee;
	}
	
	/**
	 * Sets the region of the department.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurRegionInterface $isPartOfRegion
	 * @return ApiFrDatatourismeDiffuseurDepartmentInterface
	 */
	public function setIsPartOfRegion(?ApiFrDatatourismeDiffuseurRegionInterface $isPartOfRegion) : ApiFrDatatourismeDiffuseurDepartmentInterface
	{
		$this->_isPartOfRegion = $isPartOfRegion;
		
		return $this;
	}
	
	/**
	 * Gets the region of the department.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurRegionInterface
	 */
	public function getIsPartOfRegion() : ?ApiFrDatatourismeDiffuseurRegionInterface
	{
		return $this->_isPartOfRegion;
	}
	
}
