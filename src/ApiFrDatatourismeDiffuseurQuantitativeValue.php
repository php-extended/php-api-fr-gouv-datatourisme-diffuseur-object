<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurQuantitativeValue class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurQuantitativeValueInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurQuantitativeValue implements ApiFrDatatourismeDiffuseurQuantitativeValueInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * Gets the max quantified value.
	 * 
	 * @var array<int, string>
	 */
	protected array $_schemaMaxValue = [];
	
	/**
	 * The unit.
	 * 
	 * @var array<int, string>
	 */
	protected array $_schemaUnitText = [];
	
	/**
	 * The value.
	 * 
	 * @var ?string
	 */
	protected ?string $_schemaValue = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurQuantitativeValue with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets gets the max quantified value.
	 * 
	 * @param array<int, string> $schemaMaxValue
	 * @return ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	public function setSchemaMaxValue(array $schemaMaxValue) : ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	{
		$this->_schemaMaxValue = $schemaMaxValue;
		
		return $this;
	}
	
	/**
	 * Gets gets the max quantified value.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaMaxValue() : array
	{
		return $this->_schemaMaxValue;
	}
	
	/**
	 * Sets the unit.
	 * 
	 * @param array<int, string> $schemaUnitText
	 * @return ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	public function setSchemaUnitText(array $schemaUnitText) : ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	{
		$this->_schemaUnitText = $schemaUnitText;
		
		return $this;
	}
	
	/**
	 * Gets the unit.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaUnitText() : array
	{
		return $this->_schemaUnitText;
	}
	
	/**
	 * Sets the value.
	 * 
	 * @param ?string $schemaValue
	 * @return ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	public function setSchemaValue(?string $schemaValue) : ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	{
		$this->_schemaValue = $schemaValue;
		
		return $this;
	}
	
	/**
	 * Gets the value.
	 * 
	 * @return ?string
	 */
	public function getSchemaValue() : ?string
	{
		return $this->_schemaValue;
	}
	
}
