<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurDescription class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurDescriptionInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurDescription implements ApiFrDatatourismeDiffuseurDescriptionInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The description.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_dcDescription = null;
	
	/**
	 * The translated properties.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	protected array $_hasTranslatedProperty = [];
	
	/**
	 * The audience the item is dedicated to.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	protected array $_isDedicatedTo = [];
	
	/**
	 * The short description of the point of interest.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_shortDescription = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurDescription with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurDescriptionInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurDescriptionInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurDescriptionInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurDescriptionInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the description.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $dcDescription
	 * @return ApiFrDatatourismeDiffuseurDescriptionInterface
	 */
	public function setDcDescription(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $dcDescription) : ApiFrDatatourismeDiffuseurDescriptionInterface
	{
		$this->_dcDescription = $dcDescription;
		
		return $this;
	}
	
	/**
	 * Gets the description.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getDcDescription() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_dcDescription;
	}
	
	/**
	 * Sets the translated properties.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface> $hasTranslatedProperty
	 * @return ApiFrDatatourismeDiffuseurDescriptionInterface
	 */
	public function setHasTranslatedProperty(array $hasTranslatedProperty) : ApiFrDatatourismeDiffuseurDescriptionInterface
	{
		$this->_hasTranslatedProperty = $hasTranslatedProperty;
		
		return $this;
	}
	
	/**
	 * Gets the translated properties.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array
	{
		return $this->_hasTranslatedProperty;
	}
	
	/**
	 * Sets the audience the item is dedicated to.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAudienceInterface> $isDedicatedTo
	 * @return ApiFrDatatourismeDiffuseurDescriptionInterface
	 */
	public function setIsDedicatedTo(array $isDedicatedTo) : ApiFrDatatourismeDiffuseurDescriptionInterface
	{
		$this->_isDedicatedTo = $isDedicatedTo;
		
		return $this;
	}
	
	/**
	 * Gets the audience the item is dedicated to.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	public function getIsDedicatedTo() : array
	{
		return $this->_isDedicatedTo;
	}
	
	/**
	 * Sets the short description of the point of interest.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $shortDescription
	 * @return ApiFrDatatourismeDiffuseurDescriptionInterface
	 */
	public function setShortDescription(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $shortDescription) : ApiFrDatatourismeDiffuseurDescriptionInterface
	{
		$this->_shortDescription = $shortDescription;
		
		return $this;
	}
	
	/**
	 * Gets the short description of the point of interest.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getShortDescription() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_shortDescription;
	}
	
}
