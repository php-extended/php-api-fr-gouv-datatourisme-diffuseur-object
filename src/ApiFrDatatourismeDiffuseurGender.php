<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

/**
 * ApiFrDatatourismeDiffuseurGender class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurGenderInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurGender implements ApiFrDatatourismeDiffuseurGenderInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The comment.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_rdfsComment = null;
	
	/**
	 * The label.
	 * 
	 * @var array<int, string>
	 */
	protected array $_rdfsLabel = [];
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurGender with private members.
	 * 
	 * @param string $id
	 * @param array<int, string> $type
	 */
	public function __construct(string $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param string $id
	 * @return ApiFrDatatourismeDiffuseurGenderInterface
	 */
	public function setId(string $id) : ApiFrDatatourismeDiffuseurGenderInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurGenderInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurGenderInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the comment.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsComment
	 * @return ApiFrDatatourismeDiffuseurGenderInterface
	 */
	public function setRdfsComment(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsComment) : ApiFrDatatourismeDiffuseurGenderInterface
	{
		$this->_rdfsComment = $rdfsComment;
		
		return $this;
	}
	
	/**
	 * Gets the comment.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsComment() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_rdfsComment;
	}
	
	/**
	 * Sets the label.
	 * 
	 * @param array<int, string> $rdfsLabel
	 * @return ApiFrDatatourismeDiffuseurGenderInterface
	 */
	public function setRdfsLabel(array $rdfsLabel) : ApiFrDatatourismeDiffuseurGenderInterface
	{
		$this->_rdfsLabel = $rdfsLabel;
		
		return $this;
	}
	
	/**
	 * Gets the label.
	 * 
	 * @return array<int, string>
	 */
	public function getRdfsLabel() : array
	{
		return $this->_rdfsLabel;
	}
	
}
