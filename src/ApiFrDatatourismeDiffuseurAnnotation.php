<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurAnnotation class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurAnnotationInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurAnnotation implements ApiFrDatatourismeDiffuseurAnnotationInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The abstract.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_ebucoreAbstract = null;
	
	/**
	 * The comments.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_ebucoreComments = null;
	
	/**
	 * The licence.
	 * 
	 * @var ?string
	 */
	protected ?string $_ebucoreIsCoveredBy = null;
	
	/**
	 * The title.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_ebucoreTitle = null;
	
	/**
	 * The credits.
	 * 
	 * @var array<int, string>
	 */
	protected array $_credits = [];
	
	/**
	 * The list of all elements that were translated for this object.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	protected array $_hasTranslatedProperty = [];
	
	/**
	 * The date the rights ends to be applied on the media.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_rightsEndDate = null;
	
	/**
	 * The date the rights starts to be applied on the media.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_rightsStartDate = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurAnnotation with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurAnnotationInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurAnnotationInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurAnnotationInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurAnnotationInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the abstract.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $ebucoreAbstract
	 * @return ApiFrDatatourismeDiffuseurAnnotationInterface
	 */
	public function setEbucoreAbstract(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $ebucoreAbstract) : ApiFrDatatourismeDiffuseurAnnotationInterface
	{
		$this->_ebucoreAbstract = $ebucoreAbstract;
		
		return $this;
	}
	
	/**
	 * Gets the abstract.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getEbucoreAbstract() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_ebucoreAbstract;
	}
	
	/**
	 * Sets the comments.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $ebucoreComments
	 * @return ApiFrDatatourismeDiffuseurAnnotationInterface
	 */
	public function setEbucoreComments(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $ebucoreComments) : ApiFrDatatourismeDiffuseurAnnotationInterface
	{
		$this->_ebucoreComments = $ebucoreComments;
		
		return $this;
	}
	
	/**
	 * Gets the comments.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getEbucoreComments() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_ebucoreComments;
	}
	
	/**
	 * Sets the licence.
	 * 
	 * @param ?string $ebucoreIsCoveredBy
	 * @return ApiFrDatatourismeDiffuseurAnnotationInterface
	 */
	public function setEbucoreIsCoveredBy(?string $ebucoreIsCoveredBy) : ApiFrDatatourismeDiffuseurAnnotationInterface
	{
		$this->_ebucoreIsCoveredBy = $ebucoreIsCoveredBy;
		
		return $this;
	}
	
	/**
	 * Gets the licence.
	 * 
	 * @return ?string
	 */
	public function getEbucoreIsCoveredBy() : ?string
	{
		return $this->_ebucoreIsCoveredBy;
	}
	
	/**
	 * Sets the title.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $ebucoreTitle
	 * @return ApiFrDatatourismeDiffuseurAnnotationInterface
	 */
	public function setEbucoreTitle(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $ebucoreTitle) : ApiFrDatatourismeDiffuseurAnnotationInterface
	{
		$this->_ebucoreTitle = $ebucoreTitle;
		
		return $this;
	}
	
	/**
	 * Gets the title.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getEbucoreTitle() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_ebucoreTitle;
	}
	
	/**
	 * Sets the credits.
	 * 
	 * @param array<int, string> $credits
	 * @return ApiFrDatatourismeDiffuseurAnnotationInterface
	 */
	public function setCredits(array $credits) : ApiFrDatatourismeDiffuseurAnnotationInterface
	{
		$this->_credits = $credits;
		
		return $this;
	}
	
	/**
	 * Gets the credits.
	 * 
	 * @return array<int, string>
	 */
	public function getCredits() : array
	{
		return $this->_credits;
	}
	
	/**
	 * Sets the list of all elements that were translated for this object.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface> $hasTranslatedProperty
	 * @return ApiFrDatatourismeDiffuseurAnnotationInterface
	 */
	public function setHasTranslatedProperty(array $hasTranslatedProperty) : ApiFrDatatourismeDiffuseurAnnotationInterface
	{
		$this->_hasTranslatedProperty = $hasTranslatedProperty;
		
		return $this;
	}
	
	/**
	 * Gets the list of all elements that were translated for this object.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array
	{
		return $this->_hasTranslatedProperty;
	}
	
	/**
	 * Sets the date the rights ends to be applied on the media.
	 * 
	 * @param ?DateTimeInterface $rightsEndDate
	 * @return ApiFrDatatourismeDiffuseurAnnotationInterface
	 */
	public function setRightsEndDate(?DateTimeInterface $rightsEndDate) : ApiFrDatatourismeDiffuseurAnnotationInterface
	{
		$this->_rightsEndDate = $rightsEndDate;
		
		return $this;
	}
	
	/**
	 * Gets the date the rights ends to be applied on the media.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getRightsEndDate() : ?DateTimeInterface
	{
		return $this->_rightsEndDate;
	}
	
	/**
	 * Sets the date the rights starts to be applied on the media.
	 * 
	 * @param ?DateTimeInterface $rightsStartDate
	 * @return ApiFrDatatourismeDiffuseurAnnotationInterface
	 */
	public function setRightsStartDate(?DateTimeInterface $rightsStartDate) : ApiFrDatatourismeDiffuseurAnnotationInterface
	{
		$this->_rightsStartDate = $rightsStartDate;
		
		return $this;
	}
	
	/**
	 * Gets the date the rights starts to be applied on the media.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getRightsStartDate() : ?DateTimeInterface
	{
		return $this->_rightsStartDate;
	}
	
}
