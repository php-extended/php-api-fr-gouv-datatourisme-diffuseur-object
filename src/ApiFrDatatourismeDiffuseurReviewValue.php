<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

/**
 * ApiFrDatatourismeDiffuseurReviewValue class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurReviewValueInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurReviewValue implements ApiFrDatatourismeDiffuseurReviewValueInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The value of the review.
	 * 
	 * @var ?string
	 */
	protected ?string $_schemaRatingValue = null;
	
	/**
	 * The label of this object.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_rdfsLabel = null;
	
	/**
	 * The classes to which this review apply.
	 * 
	 * @var array<int, string>
	 */
	protected array $_isCompliantWith = [];
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurReviewValue with private members.
	 * 
	 * @param string $id
	 * @param array<int, string> $type
	 */
	public function __construct(string $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param string $id
	 * @return ApiFrDatatourismeDiffuseurReviewValueInterface
	 */
	public function setId(string $id) : ApiFrDatatourismeDiffuseurReviewValueInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurReviewValueInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurReviewValueInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the value of the review.
	 * 
	 * @param ?string $schemaRatingValue
	 * @return ApiFrDatatourismeDiffuseurReviewValueInterface
	 */
	public function setSchemaRatingValue(?string $schemaRatingValue) : ApiFrDatatourismeDiffuseurReviewValueInterface
	{
		$this->_schemaRatingValue = $schemaRatingValue;
		
		return $this;
	}
	
	/**
	 * Gets the value of the review.
	 * 
	 * @return ?string
	 */
	public function getSchemaRatingValue() : ?string
	{
		return $this->_schemaRatingValue;
	}
	
	/**
	 * Sets the label of this object.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel
	 * @return ApiFrDatatourismeDiffuseurReviewValueInterface
	 */
	public function setRdfsLabel(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel) : ApiFrDatatourismeDiffuseurReviewValueInterface
	{
		$this->_rdfsLabel = $rdfsLabel;
		
		return $this;
	}
	
	/**
	 * Gets the label of this object.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsLabel() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_rdfsLabel;
	}
	
	/**
	 * Sets the classes to which this review apply.
	 * 
	 * @param array<int, string> $isCompliantWith
	 * @return ApiFrDatatourismeDiffuseurReviewValueInterface
	 */
	public function setIsCompliantWith(array $isCompliantWith) : ApiFrDatatourismeDiffuseurReviewValueInterface
	{
		$this->_isCompliantWith = $isCompliantWith;
		
		return $this;
	}
	
	/**
	 * Gets the classes to which this review apply.
	 * 
	 * @return array<int, string>
	 */
	public function getIsCompliantWith() : array
	{
		return $this->_isCompliantWith;
	}
	
}
