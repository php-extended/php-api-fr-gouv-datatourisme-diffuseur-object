<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use PhpExtended\Email\EmailAddressInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurAgent class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurAgentInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiFrDatatourismeDiffuseurAgent implements ApiFrDatatourismeDiffuseurAgentInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The dc:identifier of the object.
	 * 
	 * @var ?string
	 */
	protected ?string $_dcIdentifier = null;
	
	/**
	 * The comments.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_rdfsComment = null;
	
	/**
	 * The address of the agent.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAddressInterface>
	 */
	protected array $_schemaAddress = [];
	
	/**
	 * Email addresses of the agent.
	 * 
	 * @var array<int, EmailAddressInterface>
	 */
	protected array $_schemaEmail = [];
	
	/**
	 * The family name of this agent.
	 * 
	 * @var array<int, string>
	 */
	protected array $_schemaFamilyName = [];
	
	/**
	 * The fax number of this agent.
	 * 
	 * @var array<int, string>
	 */
	protected array $_schemaFaxNumber = [];
	
	/**
	 * The gender of this agent.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurGenderInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurGenderInterface $_schemaGender = null;
	
	/**
	 * The given name of this agent.
	 * 
	 * @var array<int, string>
	 */
	protected array $_schemaGivenName = [];
	
	/**
	 * The legal name of the agent.
	 * 
	 * @var ?string
	 */
	protected ?string $_schemaLegalName = null;
	
	/**
	 * Telephone numbers of the agent.
	 * 
	 * @var array<int, string>
	 */
	protected array $_schemaTelephone = [];
	
	/**
	 * The logo of the agent.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_schemaLogo = null;
	
	/**
	 * The homepage of the agent.
	 * 
	 * @var array<int, UriInterface>
	 */
	protected array $_foafHomepage = [];
	
	/**
	 * The title of the agent.
	 * 
	 * @var array<int, string>
	 */
	protected array $_foafTitle = [];
	
	/**
	 * The list of all elements that were translated for this object.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	protected array $_hasTranslatedProperty = [];
	
	/**
	 * French official agreement license number.
	 * 
	 * @var ?string
	 */
	protected ?string $_agreementLicense = null;
	
	/**
	 * A code given by French institue INSEE to identify the main activity of
	 * an Organisation.
	 * 
	 * @var ?string
	 */
	protected ?string $_apeNaf = null;
	
	/**
	 * French official ID to identify vacation rentals.
	 * 
	 * @var ?string
	 */
	protected ?string $_registeredNumber = null;
	
	/**
	 * The siret number of the agent, if any.
	 * 
	 * @var ?string
	 */
	protected ?string $_siret = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurAgent with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the dc:identifier of the object.
	 * 
	 * @param ?string $dcIdentifier
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setDcIdentifier(?string $dcIdentifier) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_dcIdentifier = $dcIdentifier;
		
		return $this;
	}
	
	/**
	 * Gets the dc:identifier of the object.
	 * 
	 * @return ?string
	 */
	public function getDcIdentifier() : ?string
	{
		return $this->_dcIdentifier;
	}
	
	/**
	 * Sets the comments.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsComment
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setRdfsComment(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsComment) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_rdfsComment = $rdfsComment;
		
		return $this;
	}
	
	/**
	 * Gets the comments.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsComment() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_rdfsComment;
	}
	
	/**
	 * Sets the address of the agent.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAddressInterface> $schemaAddress
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setSchemaAddress(array $schemaAddress) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_schemaAddress = $schemaAddress;
		
		return $this;
	}
	
	/**
	 * Gets the address of the agent.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAddressInterface>
	 */
	public function getSchemaAddress() : array
	{
		return $this->_schemaAddress;
	}
	
	/**
	 * Sets email addresses of the agent.
	 * 
	 * @param array<int, EmailAddressInterface> $schemaEmail
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setSchemaEmail(array $schemaEmail) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_schemaEmail = $schemaEmail;
		
		return $this;
	}
	
	/**
	 * Gets email addresses of the agent.
	 * 
	 * @return array<int, EmailAddressInterface>
	 */
	public function getSchemaEmail() : array
	{
		return $this->_schemaEmail;
	}
	
	/**
	 * Sets the family name of this agent.
	 * 
	 * @param array<int, string> $schemaFamilyName
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setSchemaFamilyName(array $schemaFamilyName) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_schemaFamilyName = $schemaFamilyName;
		
		return $this;
	}
	
	/**
	 * Gets the family name of this agent.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaFamilyName() : array
	{
		return $this->_schemaFamilyName;
	}
	
	/**
	 * Sets the fax number of this agent.
	 * 
	 * @param array<int, string> $schemaFaxNumber
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setSchemaFaxNumber(array $schemaFaxNumber) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_schemaFaxNumber = $schemaFaxNumber;
		
		return $this;
	}
	
	/**
	 * Gets the fax number of this agent.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaFaxNumber() : array
	{
		return $this->_schemaFaxNumber;
	}
	
	/**
	 * Sets the gender of this agent.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurGenderInterface $schemaGender
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setSchemaGender(?ApiFrDatatourismeDiffuseurGenderInterface $schemaGender) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_schemaGender = $schemaGender;
		
		return $this;
	}
	
	/**
	 * Gets the gender of this agent.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurGenderInterface
	 */
	public function getSchemaGender() : ?ApiFrDatatourismeDiffuseurGenderInterface
	{
		return $this->_schemaGender;
	}
	
	/**
	 * Sets the given name of this agent.
	 * 
	 * @param array<int, string> $schemaGivenName
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setSchemaGivenName(array $schemaGivenName) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_schemaGivenName = $schemaGivenName;
		
		return $this;
	}
	
	/**
	 * Gets the given name of this agent.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaGivenName() : array
	{
		return $this->_schemaGivenName;
	}
	
	/**
	 * Sets the legal name of the agent.
	 * 
	 * @param ?string $schemaLegalName
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setSchemaLegalName(?string $schemaLegalName) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_schemaLegalName = $schemaLegalName;
		
		return $this;
	}
	
	/**
	 * Gets the legal name of the agent.
	 * 
	 * @return ?string
	 */
	public function getSchemaLegalName() : ?string
	{
		return $this->_schemaLegalName;
	}
	
	/**
	 * Sets telephone numbers of the agent.
	 * 
	 * @param array<int, string> $schemaTelephone
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setSchemaTelephone(array $schemaTelephone) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_schemaTelephone = $schemaTelephone;
		
		return $this;
	}
	
	/**
	 * Gets telephone numbers of the agent.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaTelephone() : array
	{
		return $this->_schemaTelephone;
	}
	
	/**
	 * Sets the logo of the agent.
	 * 
	 * @param ?UriInterface $schemaLogo
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setSchemaLogo(?UriInterface $schemaLogo) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_schemaLogo = $schemaLogo;
		
		return $this;
	}
	
	/**
	 * Gets the logo of the agent.
	 * 
	 * @return ?UriInterface
	 */
	public function getSchemaLogo() : ?UriInterface
	{
		return $this->_schemaLogo;
	}
	
	/**
	 * Sets the homepage of the agent.
	 * 
	 * @param array<int, UriInterface> $foafHomepage
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setFoafHomepage(array $foafHomepage) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_foafHomepage = $foafHomepage;
		
		return $this;
	}
	
	/**
	 * Gets the homepage of the agent.
	 * 
	 * @return array<int, UriInterface>
	 */
	public function getFoafHomepage() : array
	{
		return $this->_foafHomepage;
	}
	
	/**
	 * Sets the title of the agent.
	 * 
	 * @param array<int, string> $foafTitle
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setFoafTitle(array $foafTitle) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_foafTitle = $foafTitle;
		
		return $this;
	}
	
	/**
	 * Gets the title of the agent.
	 * 
	 * @return array<int, string>
	 */
	public function getFoafTitle() : array
	{
		return $this->_foafTitle;
	}
	
	/**
	 * Sets the list of all elements that were translated for this object.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface> $hasTranslatedProperty
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setHasTranslatedProperty(array $hasTranslatedProperty) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_hasTranslatedProperty = $hasTranslatedProperty;
		
		return $this;
	}
	
	/**
	 * Gets the list of all elements that were translated for this object.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array
	{
		return $this->_hasTranslatedProperty;
	}
	
	/**
	 * Sets french official agreement license number.
	 * 
	 * @param ?string $agreementLicense
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setAgreementLicense(?string $agreementLicense) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_agreementLicense = $agreementLicense;
		
		return $this;
	}
	
	/**
	 * Gets french official agreement license number.
	 * 
	 * @return ?string
	 */
	public function getAgreementLicense() : ?string
	{
		return $this->_agreementLicense;
	}
	
	/**
	 * Sets a code given by French institue INSEE to identify the main activity
	 * of an Organisation.
	 * 
	 * @param ?string $apeNaf
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setApeNaf(?string $apeNaf) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_apeNaf = $apeNaf;
		
		return $this;
	}
	
	/**
	 * Gets a code given by French institue INSEE to identify the main activity
	 * of an Organisation.
	 * 
	 * @return ?string
	 */
	public function getApeNaf() : ?string
	{
		return $this->_apeNaf;
	}
	
	/**
	 * Sets french official ID to identify vacation rentals.
	 * 
	 * @param ?string $registeredNumber
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setRegisteredNumber(?string $registeredNumber) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_registeredNumber = $registeredNumber;
		
		return $this;
	}
	
	/**
	 * Gets french official ID to identify vacation rentals.
	 * 
	 * @return ?string
	 */
	public function getRegisteredNumber() : ?string
	{
		return $this->_registeredNumber;
	}
	
	/**
	 * Sets the siret number of the agent, if any.
	 * 
	 * @param ?string $siret
	 * @return ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function setSiret(?string $siret) : ApiFrDatatourismeDiffuseurAgentInterface
	{
		$this->_siret = $siret;
		
		return $this;
	}
	
	/**
	 * Gets the siret number of the agent, if any.
	 * 
	 * @return ?string
	 */
	public function getSiret() : ?string
	{
		return $this->_siret;
	}
	
}
