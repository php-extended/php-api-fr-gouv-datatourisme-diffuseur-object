<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

/**
 * ApiFrDatatourismeDiffuseurRegion class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurRegionInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurRegion implements ApiFrDatatourismeDiffuseurRegionInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The label.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_rdfsLabel = null;
	
	/**
	 * The insee code of the department.
	 * 
	 * @var ?string
	 */
	protected ?string $_insee = null;
	
	/**
	 * The country this region is in.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurCountryInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurCountryInterface $_isPartOfCountry = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurRegion with private members.
	 * 
	 * @param string $id
	 * @param array<int, string> $type
	 */
	public function __construct(string $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param string $id
	 * @return ApiFrDatatourismeDiffuseurRegionInterface
	 */
	public function setId(string $id) : ApiFrDatatourismeDiffuseurRegionInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurRegionInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurRegionInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the label.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel
	 * @return ApiFrDatatourismeDiffuseurRegionInterface
	 */
	public function setRdfsLabel(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel) : ApiFrDatatourismeDiffuseurRegionInterface
	{
		$this->_rdfsLabel = $rdfsLabel;
		
		return $this;
	}
	
	/**
	 * Gets the label.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsLabel() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_rdfsLabel;
	}
	
	/**
	 * Sets the insee code of the department.
	 * 
	 * @param ?string $insee
	 * @return ApiFrDatatourismeDiffuseurRegionInterface
	 */
	public function setInsee(?string $insee) : ApiFrDatatourismeDiffuseurRegionInterface
	{
		$this->_insee = $insee;
		
		return $this;
	}
	
	/**
	 * Gets the insee code of the department.
	 * 
	 * @return ?string
	 */
	public function getInsee() : ?string
	{
		return $this->_insee;
	}
	
	/**
	 * Sets the country this region is in.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurCountryInterface $isPartOfCountry
	 * @return ApiFrDatatourismeDiffuseurRegionInterface
	 */
	public function setIsPartOfCountry(?ApiFrDatatourismeDiffuseurCountryInterface $isPartOfCountry) : ApiFrDatatourismeDiffuseurRegionInterface
	{
		$this->_isPartOfCountry = $isPartOfCountry;
		
		return $this;
	}
	
	/**
	 * Gets the country this region is in.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurCountryInterface
	 */
	public function getIsPartOfCountry() : ?ApiFrDatatourismeDiffuseurCountryInterface
	{
		return $this->_isPartOfCountry;
	}
	
}
