<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurReview class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurReviewInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurReview implements ApiFrDatatourismeDiffuseurReviewInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The date published.
	 * 
	 * @var array<int, DateTimeInterface>
	 */
	protected array $_schemaDatePublished = [];
	
	/**
	 * The review values.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurReviewValueInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurReviewValueInterface $_hasReviewValue = null;
	
	/**
	 * True if the review is pending.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_pending = null;
	
	/**
	 * The date on which the review has been delivered.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_reviewDeliveryDate = null;
	
	/**
	 * The date on which the review will expire.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_reviewExpirationDate = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurReview with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurReviewInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurReviewInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurReviewInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurReviewInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the date published.
	 * 
	 * @param array<int, DateTimeInterface> $schemaDatePublished
	 * @return ApiFrDatatourismeDiffuseurReviewInterface
	 */
	public function setSchemaDatePublished(array $schemaDatePublished) : ApiFrDatatourismeDiffuseurReviewInterface
	{
		$this->_schemaDatePublished = $schemaDatePublished;
		
		return $this;
	}
	
	/**
	 * Gets the date published.
	 * 
	 * @return array<int, DateTimeInterface>
	 */
	public function getSchemaDatePublished() : array
	{
		return $this->_schemaDatePublished;
	}
	
	/**
	 * Sets the review values.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurReviewValueInterface $hasReviewValue
	 * @return ApiFrDatatourismeDiffuseurReviewInterface
	 */
	public function setHasReviewValue(?ApiFrDatatourismeDiffuseurReviewValueInterface $hasReviewValue) : ApiFrDatatourismeDiffuseurReviewInterface
	{
		$this->_hasReviewValue = $hasReviewValue;
		
		return $this;
	}
	
	/**
	 * Gets the review values.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurReviewValueInterface
	 */
	public function getHasReviewValue() : ?ApiFrDatatourismeDiffuseurReviewValueInterface
	{
		return $this->_hasReviewValue;
	}
	
	/**
	 * Sets true if the review is pending.
	 * 
	 * @param ?bool $pending
	 * @return ApiFrDatatourismeDiffuseurReviewInterface
	 */
	public function setPending(?bool $pending) : ApiFrDatatourismeDiffuseurReviewInterface
	{
		$this->_pending = $pending;
		
		return $this;
	}
	
	/**
	 * Gets true if the review is pending.
	 * 
	 * @return ?bool
	 */
	public function hasPending() : ?bool
	{
		return $this->_pending;
	}
	
	/**
	 * Sets the date on which the review has been delivered.
	 * 
	 * @param ?DateTimeInterface $reviewDeliveryDate
	 * @return ApiFrDatatourismeDiffuseurReviewInterface
	 */
	public function setReviewDeliveryDate(?DateTimeInterface $reviewDeliveryDate) : ApiFrDatatourismeDiffuseurReviewInterface
	{
		$this->_reviewDeliveryDate = $reviewDeliveryDate;
		
		return $this;
	}
	
	/**
	 * Gets the date on which the review has been delivered.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getReviewDeliveryDate() : ?DateTimeInterface
	{
		return $this->_reviewDeliveryDate;
	}
	
	/**
	 * Sets the date on which the review will expire.
	 * 
	 * @param ?DateTimeInterface $reviewExpirationDate
	 * @return ApiFrDatatourismeDiffuseurReviewInterface
	 */
	public function setReviewExpirationDate(?DateTimeInterface $reviewExpirationDate) : ApiFrDatatourismeDiffuseurReviewInterface
	{
		$this->_reviewExpirationDate = $reviewExpirationDate;
		
		return $this;
	}
	
	/**
	 * Gets the date on which the review will expire.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getReviewExpirationDate() : ?DateTimeInterface
	{
		return $this->_reviewExpirationDate;
	}
	
}
