<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Iterator;
use IteratorIterator;
use PhpExtended\DataProvider\JsonFileDataProvider;
use PhpExtended\Reifier\ReifierConfigurationInterface;
use PhpExtended\Reifier\ReifierInterface;
use PhpExtended\Reifier\ReifierReportInterface;
use RuntimeException;
use Throwable;

/**
 * ApiFrDatatourismeDiffuseurPointOfInterestIterator class file.
 * 
 * This class is a simple implementation of the ApiFrDatatourismeDiffuseurPointOfInterestIteratorInterface.
 * 
 * @author Anastaszor
 * @extends \IteratorIterator<integer, ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface, \Iterator<integer, ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface>>
 * @SuppressWarnings("PHPMD.LongClassName")
 */
class ApiFrDatatourismeDiffuseurPointOfInterestIterator extends IteratorIterator implements ApiFrDatatourismeDiffuseurPointOfInterestIteratorInterface
{
	
	/**
	 * The base path of the folder that holds the data.
	 * 
	 * @var string
	 */
	protected string $_basePath;
	
	/**
	 * The reifier.
	 * 
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * The reification configuration.
	 * 
	 * @var ReifierConfigurationInterface
	 */
	protected ReifierConfigurationInterface $_config;
	
	/**
	 * Builds a new ApiFrDatatourismeDiffuseurPointOfInterestIterator.
	 * 
	 * @param string $basePath
	 * @param ReifierInterface $reifier
	 * @param ReifierConfigurationInterface $config
	 * @param Iterator<integer, ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface> $data
	 */
	public function __construct(
		string $basePath,
		ReifierInterface $reifier,
		ReifierConfigurationInterface $config,
		Iterator $data
	) {
		$this->_basePath = $basePath;
		$this->_reifier = $reifier;
		$this->_config = $config;
		parent::__construct($data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::current()
	 * @psalm-suppress InvalidNullableReturnType
	 */
	public function current() : ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface
	{
		/** @psalm-suppress NullableReturnStatement */
		return parent::current();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPointOfInterestIteratorInterface::getPointOfInterestData()
	 */
	public function getPointOfInterestData(ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface $resume, ?ReifierReportInterface $report = null) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$filePath = \rtrim($this->_basePath, '/\\').\DIRECTORY_SEPARATOR.'objects'.\DIRECTORY_SEPARATOR.\trim((string) $resume->getFile(), '/\\');
		$dataProvider = new JsonFileDataProvider($filePath);
		
		try
		{
			$this->_reifier->setConfiguration($this->_config);
			
			return $this->_reifier->reify(ApiFrDatatourismeDiffuseurPointOfInterest::class, $dataProvider->provideOne());
		}
		// @codeCoverageIgnoreStart
		catch(Throwable $t)
		{
			$message = 'Failed to reify {class} at {path}';
			$context = ['{class}' => ApiFrDatatourismeDiffuseurPointOfInterest::class, '{path}' => $filePath];
			
			throw new RuntimeException(\strtr($message, $context), -1, $t);
		}
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPointOfInterestIteratorInterface::tryGetPointOfInterestData()
	 */
	public function tryGetPointOfInterestData(ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface $resume, ?ReifierReportInterface $report = null) : ?ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$filePath = \rtrim($this->_basePath, '/\\').\DIRECTORY_SEPARATOR.'objects'.\DIRECTORY_SEPARATOR.\trim((string) $resume->getFile(), '/\\');
		$dataProvider = new JsonFileDataProvider($filePath);
		$this->_reifier->setConfiguration($this->_config);
		
		return $this->_reifier->tryReify(ApiFrDatatourismeDiffuseurPointOfInterest::class, $dataProvider->provideOne(), $report);
	}
}
