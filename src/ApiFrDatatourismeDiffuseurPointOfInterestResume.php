<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurPointOfInterestResume class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurPointOfInterestResume implements ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_id = null;
	
	/**
	 * The label of the object.
	 * 
	 * @var ?string
	 */
	protected ?string $_label = null;
	
	/**
	 * The date of last udpate of the object in datatourisme.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_lastUpdateDatatourisme;
	
	/**
	 * The full relative path.
	 * 
	 * @var string
	 */
	protected string $_file;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurPointOfInterestResume with private members.
	 * 
	 * @param DateTimeInterface $lastUpdateDatatourisme
	 * @param string $file
	 */
	public function __construct(DateTimeInterface $lastUpdateDatatourisme, string $file)
	{
		$this->setLastUpdateDatatourisme($lastUpdateDatatourisme);
		$this->setFile($file);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param ?UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface
	 */
	public function setId(?UriInterface $id) : ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return ?UriInterface
	 */
	public function getId() : ?UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the label of the object.
	 * 
	 * @param ?string $label
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface
	 */
	public function setLabel(?string $label) : ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface
	{
		$this->_label = $label;
		
		return $this;
	}
	
	/**
	 * Gets the label of the object.
	 * 
	 * @return ?string
	 */
	public function getLabel() : ?string
	{
		return $this->_label;
	}
	
	/**
	 * Sets the date of last udpate of the object in datatourisme.
	 * 
	 * @param DateTimeInterface $lastUpdateDatatourisme
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface
	 */
	public function setLastUpdateDatatourisme(DateTimeInterface $lastUpdateDatatourisme) : ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface
	{
		$this->_lastUpdateDatatourisme = $lastUpdateDatatourisme;
		
		return $this;
	}
	
	/**
	 * Gets the date of last udpate of the object in datatourisme.
	 * 
	 * @return DateTimeInterface
	 */
	public function getLastUpdateDatatourisme() : DateTimeInterface
	{
		return $this->_lastUpdateDatatourisme;
	}
	
	/**
	 * Sets the full relative path.
	 * 
	 * @param string $file
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface
	 */
	public function setFile(string $file) : ApiFrDatatourismeDiffuseurPointOfInterestResumeInterface
	{
		$this->_file = $file;
		
		return $this;
	}
	
	/**
	 * Gets the full relative path.
	 * 
	 * @return string
	 */
	public function getFile() : string
	{
		return $this->_file;
	}
	
}
