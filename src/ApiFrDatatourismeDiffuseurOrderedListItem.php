<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurOrderedListItem class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurOrderedListItemInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurOrderedListItem implements ApiFrDatatourismeDiffuseurOrderedListItemInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The canonical description.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_dcDescription = null;
	
	/**
	 * The label of this object.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_rdfsLabel = null;
	
	/**
	 * Expected time to travel the stage in minutes.
	 * 
	 * @var ?float
	 */
	protected ?float $_duration = null;
	
	/**
	 * The high difference between this step and the first step.
	 * 
	 * @var ?float
	 */
	protected ?float $_highDifference = null;
	
	/**
	 * The place where the PointOfInterest is located and therefore can be
	 * potentially consumed at.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurLocationInterface>
	 */
	protected array $_isLocatedAt = [];
	
	/**
	 * A media that can be considered a main representation of the POI.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	protected array $_hasMainRepresentation = [];
	
	/**
	 * A representation is a Media that is related to the POI. e.g. : a photo
	 * of the Product, a promotional PDF document, ...
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	protected array $_hasRepresentation = [];
	
	/**
	 * List of translated elements.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	protected array $_hasTranslatedProperty = [];
	
	/**
	 * Distance between start and end of stage.
	 * 
	 * @var ?float
	 */
	protected ?float $_tourDistance = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurOrderedListItem with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the canonical description.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $dcDescription
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setDcDescription(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $dcDescription) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_dcDescription = $dcDescription;
		
		return $this;
	}
	
	/**
	 * Gets the canonical description.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getDcDescription() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_dcDescription;
	}
	
	/**
	 * Sets the label of this object.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setRdfsLabel(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_rdfsLabel = $rdfsLabel;
		
		return $this;
	}
	
	/**
	 * Gets the label of this object.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsLabel() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_rdfsLabel;
	}
	
	/**
	 * Sets expected time to travel the stage in minutes.
	 * 
	 * @param ?float $duration
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setDuration(?float $duration) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_duration = $duration;
		
		return $this;
	}
	
	/**
	 * Gets expected time to travel the stage in minutes.
	 * 
	 * @return ?float
	 */
	public function getDuration() : ?float
	{
		return $this->_duration;
	}
	
	/**
	 * Sets the high difference between this step and the first step.
	 * 
	 * @param ?float $highDifference
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setHighDifference(?float $highDifference) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_highDifference = $highDifference;
		
		return $this;
	}
	
	/**
	 * Gets the high difference between this step and the first step.
	 * 
	 * @return ?float
	 */
	public function getHighDifference() : ?float
	{
		return $this->_highDifference;
	}
	
	/**
	 * Sets the place where the PointOfInterest is located and therefore can be
	 * potentially consumed at.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurLocationInterface> $isLocatedAt
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setIsLocatedAt(array $isLocatedAt) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_isLocatedAt = $isLocatedAt;
		
		return $this;
	}
	
	/**
	 * Gets the place where the PointOfInterest is located and therefore can be
	 * potentially consumed at.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurLocationInterface>
	 */
	public function getIsLocatedAt() : array
	{
		return $this->_isLocatedAt;
	}
	
	/**
	 * Sets a media that can be considered a main representation of the POI.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurMediaInterface> $hasMainRepresentation
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setHasMainRepresentation(array $hasMainRepresentation) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_hasMainRepresentation = $hasMainRepresentation;
		
		return $this;
	}
	
	/**
	 * Gets a media that can be considered a main representation of the POI.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	public function getHasMainRepresentation() : array
	{
		return $this->_hasMainRepresentation;
	}
	
	/**
	 * Sets a representation is a Media that is related to the POI. e.g. : a
	 * photo of the Product, a promotional PDF document, ...
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurMediaInterface> $hasRepresentation
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setHasRepresentation(array $hasRepresentation) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_hasRepresentation = $hasRepresentation;
		
		return $this;
	}
	
	/**
	 * Gets a representation is a Media that is related to the POI. e.g. : a
	 * photo of the Product, a promotional PDF document, ...
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	public function getHasRepresentation() : array
	{
		return $this->_hasRepresentation;
	}
	
	/**
	 * Sets list of translated elements.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface> $hasTranslatedProperty
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setHasTranslatedProperty(array $hasTranslatedProperty) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_hasTranslatedProperty = $hasTranslatedProperty;
		
		return $this;
	}
	
	/**
	 * Gets list of translated elements.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array
	{
		return $this->_hasTranslatedProperty;
	}
	
	/**
	 * Sets distance between start and end of stage.
	 * 
	 * @param ?float $tourDistance
	 * @return ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function setTourDistance(?float $tourDistance) : ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		$this->_tourDistance = $tourDistance;
		
		return $this;
	}
	
	/**
	 * Gets distance between start and end of stage.
	 * 
	 * @return ?float
	 */
	public function getTourDistance() : ?float
	{
		return $this->_tourDistance;
	}
	
}
