<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

/**
 * ApiFrDatatourismeDiffuseurPricingMode class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurPricingModeInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurPricingMode implements ApiFrDatatourismeDiffuseurPricingModeInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The label.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_rdfsLabel = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurPricingMode with private members.
	 * 
	 * @param string $id
	 * @param array<int, string> $type
	 */
	public function __construct(string $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param string $id
	 * @return ApiFrDatatourismeDiffuseurPricingModeInterface
	 */
	public function setId(string $id) : ApiFrDatatourismeDiffuseurPricingModeInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurPricingModeInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurPricingModeInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the label.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel
	 * @return ApiFrDatatourismeDiffuseurPricingModeInterface
	 */
	public function setRdfsLabel(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel) : ApiFrDatatourismeDiffuseurPricingModeInterface
	{
		$this->_rdfsLabel = $rdfsLabel;
		
		return $this;
	}
	
	/**
	 * Gets the label.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsLabel() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_rdfsLabel;
	}
	
}
