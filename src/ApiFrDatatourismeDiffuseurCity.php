<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

/**
 * ApiFrDatatourismeDiffuseurCity class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurCityInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurCity implements ApiFrDatatourismeDiffuseurCityInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The label of the city.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_rdfsLabel = null;
	
	/**
	 * The insee code of the city.
	 * 
	 * @var ?string
	 */
	protected ?string $_insee = null;
	
	/**
	 * The department of the city.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurDepartmentInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurDepartmentInterface $_isPartOfDepartment = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurCity with private members.
	 * 
	 * @param string $id
	 * @param array<int, string> $type
	 */
	public function __construct(string $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param string $id
	 * @return ApiFrDatatourismeDiffuseurCityInterface
	 */
	public function setId(string $id) : ApiFrDatatourismeDiffuseurCityInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurCityInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurCityInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the label of the city.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel
	 * @return ApiFrDatatourismeDiffuseurCityInterface
	 */
	public function setRdfsLabel(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel) : ApiFrDatatourismeDiffuseurCityInterface
	{
		$this->_rdfsLabel = $rdfsLabel;
		
		return $this;
	}
	
	/**
	 * Gets the label of the city.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsLabel() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_rdfsLabel;
	}
	
	/**
	 * Sets the insee code of the city.
	 * 
	 * @param ?string $insee
	 * @return ApiFrDatatourismeDiffuseurCityInterface
	 */
	public function setInsee(?string $insee) : ApiFrDatatourismeDiffuseurCityInterface
	{
		$this->_insee = $insee;
		
		return $this;
	}
	
	/**
	 * Gets the insee code of the city.
	 * 
	 * @return ?string
	 */
	public function getInsee() : ?string
	{
		return $this->_insee;
	}
	
	/**
	 * Sets the department of the city.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurDepartmentInterface $isPartOfDepartment
	 * @return ApiFrDatatourismeDiffuseurCityInterface
	 */
	public function setIsPartOfDepartment(?ApiFrDatatourismeDiffuseurDepartmentInterface $isPartOfDepartment) : ApiFrDatatourismeDiffuseurCityInterface
	{
		$this->_isPartOfDepartment = $isPartOfDepartment;
		
		return $this;
	}
	
	/**
	 * Gets the department of the city.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurDepartmentInterface
	 */
	public function getIsPartOfDepartment() : ?ApiFrDatatourismeDiffuseurDepartmentInterface
	{
		return $this->_isPartOfDepartment;
	}
	
}
