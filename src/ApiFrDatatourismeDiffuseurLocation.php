<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurLocation class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurLocationInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurLocation implements ApiFrDatatourismeDiffuseurLocationInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The addresses.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAddressInterface>
	 */
	protected array $_schemaAddress = [];
	
	/**
	 * The geolocalisation.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurGeoInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurGeoInterface $_schemaGeo = null;
	
	/**
	 * The opening hours.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurOpeningHoursInterface>
	 */
	protected array $_schemaOpeningHoursSpecification = [];
	
	/**
	 * The resort name when it is different of the official city name.
	 * 
	 * @var array<int, string>
	 */
	protected array $_altInsee = [];
	
	/**
	 * Whether air conditioning is provided.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_airConditioning = null;
	
	/**
	 * Whether internet is provided.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_internetAccess = null;
	
	/**
	 * Whether smoking is forbidden.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_noSmoking = null;
	
	/**
	 * Whether pets are allowed.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_petsAllowed = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurLocation with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurLocationInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurLocationInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurLocationInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurLocationInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the addresses.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAddressInterface> $schemaAddress
	 * @return ApiFrDatatourismeDiffuseurLocationInterface
	 */
	public function setSchemaAddress(array $schemaAddress) : ApiFrDatatourismeDiffuseurLocationInterface
	{
		$this->_schemaAddress = $schemaAddress;
		
		return $this;
	}
	
	/**
	 * Gets the addresses.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAddressInterface>
	 */
	public function getSchemaAddress() : array
	{
		return $this->_schemaAddress;
	}
	
	/**
	 * Sets the geolocalisation.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurGeoInterface $schemaGeo
	 * @return ApiFrDatatourismeDiffuseurLocationInterface
	 */
	public function setSchemaGeo(?ApiFrDatatourismeDiffuseurGeoInterface $schemaGeo) : ApiFrDatatourismeDiffuseurLocationInterface
	{
		$this->_schemaGeo = $schemaGeo;
		
		return $this;
	}
	
	/**
	 * Gets the geolocalisation.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurGeoInterface
	 */
	public function getSchemaGeo() : ?ApiFrDatatourismeDiffuseurGeoInterface
	{
		return $this->_schemaGeo;
	}
	
	/**
	 * Sets the opening hours.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurOpeningHoursInterface> $schemaOpeningHoursSpecification
	 * @return ApiFrDatatourismeDiffuseurLocationInterface
	 */
	public function setSchemaOpeningHoursSpecification(array $schemaOpeningHoursSpecification) : ApiFrDatatourismeDiffuseurLocationInterface
	{
		$this->_schemaOpeningHoursSpecification = $schemaOpeningHoursSpecification;
		
		return $this;
	}
	
	/**
	 * Gets the opening hours.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurOpeningHoursInterface>
	 */
	public function getSchemaOpeningHoursSpecification() : array
	{
		return $this->_schemaOpeningHoursSpecification;
	}
	
	/**
	 * Sets the resort name when it is different of the official city name.
	 * 
	 * @param array<int, string> $altInsee
	 * @return ApiFrDatatourismeDiffuseurLocationInterface
	 */
	public function setAltInsee(array $altInsee) : ApiFrDatatourismeDiffuseurLocationInterface
	{
		$this->_altInsee = $altInsee;
		
		return $this;
	}
	
	/**
	 * Gets the resort name when it is different of the official city name.
	 * 
	 * @return array<int, string>
	 */
	public function getAltInsee() : array
	{
		return $this->_altInsee;
	}
	
	/**
	 * Sets whether air conditioning is provided.
	 * 
	 * @param ?bool $airConditioning
	 * @return ApiFrDatatourismeDiffuseurLocationInterface
	 */
	public function setAirConditioning(?bool $airConditioning) : ApiFrDatatourismeDiffuseurLocationInterface
	{
		$this->_airConditioning = $airConditioning;
		
		return $this;
	}
	
	/**
	 * Gets whether air conditioning is provided.
	 * 
	 * @return ?bool
	 */
	public function hasAirConditioning() : ?bool
	{
		return $this->_airConditioning;
	}
	
	/**
	 * Sets whether internet is provided.
	 * 
	 * @param ?bool $internetAccess
	 * @return ApiFrDatatourismeDiffuseurLocationInterface
	 */
	public function setInternetAccess(?bool $internetAccess) : ApiFrDatatourismeDiffuseurLocationInterface
	{
		$this->_internetAccess = $internetAccess;
		
		return $this;
	}
	
	/**
	 * Gets whether internet is provided.
	 * 
	 * @return ?bool
	 */
	public function hasInternetAccess() : ?bool
	{
		return $this->_internetAccess;
	}
	
	/**
	 * Sets whether smoking is forbidden.
	 * 
	 * @param ?bool $noSmoking
	 * @return ApiFrDatatourismeDiffuseurLocationInterface
	 */
	public function setNoSmoking(?bool $noSmoking) : ApiFrDatatourismeDiffuseurLocationInterface
	{
		$this->_noSmoking = $noSmoking;
		
		return $this;
	}
	
	/**
	 * Gets whether smoking is forbidden.
	 * 
	 * @return ?bool
	 */
	public function hasNoSmoking() : ?bool
	{
		return $this->_noSmoking;
	}
	
	/**
	 * Sets whether pets are allowed.
	 * 
	 * @param ?bool $petsAllowed
	 * @return ApiFrDatatourismeDiffuseurLocationInterface
	 */
	public function setPetsAllowed(?bool $petsAllowed) : ApiFrDatatourismeDiffuseurLocationInterface
	{
		$this->_petsAllowed = $petsAllowed;
		
		return $this;
	}
	
	/**
	 * Gets whether pets are allowed.
	 * 
	 * @return ?bool
	 */
	public function hasPetsAllowed() : ?bool
	{
		return $this->_petsAllowed;
	}
	
}
