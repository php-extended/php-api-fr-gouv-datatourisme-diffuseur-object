<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurResource class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurResourceInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurResource implements ApiFrDatatourismeDiffuseurResourceInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The size of the resource.
	 * 
	 * @var array<int, float>
	 */
	protected array $_ebucoreFileSize = [];
	
	/**
	 * The mime type.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurMimeTypeInterface>
	 */
	protected array $_ebucoreHasMimeType = [];
	
	/**
	 * The height.
	 * 
	 * @var ?float
	 */
	protected ?float $_ebucoreHeight = null;
	
	/**
	 * The unit of the height.
	 * 
	 * @var ?string
	 */
	protected ?string $_ebucoreHeightUnit = null;
	
	/**
	 * The locations.
	 * 
	 * @var array<int, UriInterface>
	 */
	protected array $_ebucoreLocator = [];
	
	/**
	 * The width.
	 * 
	 * @var ?float
	 */
	protected ?float $_ebucoreWidth = null;
	
	/**
	 * The width unit.
	 * 
	 * @var ?string
	 */
	protected ?string $_ebucoreWidthUnit = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurResource with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurResourceInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurResourceInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurResourceInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurResourceInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the size of the resource.
	 * 
	 * @param array<int, float> $ebucoreFileSize
	 * @return ApiFrDatatourismeDiffuseurResourceInterface
	 */
	public function setEbucoreFileSize(array $ebucoreFileSize) : ApiFrDatatourismeDiffuseurResourceInterface
	{
		$this->_ebucoreFileSize = $ebucoreFileSize;
		
		return $this;
	}
	
	/**
	 * Gets the size of the resource.
	 * 
	 * @return array<int, float>
	 */
	public function getEbucoreFileSize() : array
	{
		return $this->_ebucoreFileSize;
	}
	
	/**
	 * Sets the mime type.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurMimeTypeInterface> $ebucoreHasMimeType
	 * @return ApiFrDatatourismeDiffuseurResourceInterface
	 */
	public function setEbucoreHasMimeType(array $ebucoreHasMimeType) : ApiFrDatatourismeDiffuseurResourceInterface
	{
		$this->_ebucoreHasMimeType = $ebucoreHasMimeType;
		
		return $this;
	}
	
	/**
	 * Gets the mime type.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurMimeTypeInterface>
	 */
	public function getEbucoreHasMimeType() : array
	{
		return $this->_ebucoreHasMimeType;
	}
	
	/**
	 * Sets the height.
	 * 
	 * @param ?float $ebucoreHeight
	 * @return ApiFrDatatourismeDiffuseurResourceInterface
	 */
	public function setEbucoreHeight(?float $ebucoreHeight) : ApiFrDatatourismeDiffuseurResourceInterface
	{
		$this->_ebucoreHeight = $ebucoreHeight;
		
		return $this;
	}
	
	/**
	 * Gets the height.
	 * 
	 * @return ?float
	 */
	public function getEbucoreHeight() : ?float
	{
		return $this->_ebucoreHeight;
	}
	
	/**
	 * Sets the unit of the height.
	 * 
	 * @param ?string $ebucoreHeightUnit
	 * @return ApiFrDatatourismeDiffuseurResourceInterface
	 */
	public function setEbucoreHeightUnit(?string $ebucoreHeightUnit) : ApiFrDatatourismeDiffuseurResourceInterface
	{
		$this->_ebucoreHeightUnit = $ebucoreHeightUnit;
		
		return $this;
	}
	
	/**
	 * Gets the unit of the height.
	 * 
	 * @return ?string
	 */
	public function getEbucoreHeightUnit() : ?string
	{
		return $this->_ebucoreHeightUnit;
	}
	
	/**
	 * Sets the locations.
	 * 
	 * @param array<int, UriInterface> $ebucoreLocator
	 * @return ApiFrDatatourismeDiffuseurResourceInterface
	 */
	public function setEbucoreLocator(array $ebucoreLocator) : ApiFrDatatourismeDiffuseurResourceInterface
	{
		$this->_ebucoreLocator = $ebucoreLocator;
		
		return $this;
	}
	
	/**
	 * Gets the locations.
	 * 
	 * @return array<int, UriInterface>
	 */
	public function getEbucoreLocator() : array
	{
		return $this->_ebucoreLocator;
	}
	
	/**
	 * Sets the width.
	 * 
	 * @param ?float $ebucoreWidth
	 * @return ApiFrDatatourismeDiffuseurResourceInterface
	 */
	public function setEbucoreWidth(?float $ebucoreWidth) : ApiFrDatatourismeDiffuseurResourceInterface
	{
		$this->_ebucoreWidth = $ebucoreWidth;
		
		return $this;
	}
	
	/**
	 * Gets the width.
	 * 
	 * @return ?float
	 */
	public function getEbucoreWidth() : ?float
	{
		return $this->_ebucoreWidth;
	}
	
	/**
	 * Sets the width unit.
	 * 
	 * @param ?string $ebucoreWidthUnit
	 * @return ApiFrDatatourismeDiffuseurResourceInterface
	 */
	public function setEbucoreWidthUnit(?string $ebucoreWidthUnit) : ApiFrDatatourismeDiffuseurResourceInterface
	{
		$this->_ebucoreWidthUnit = $ebucoreWidthUnit;
		
		return $this;
	}
	
	/**
	 * Gets the width unit.
	 * 
	 * @return ?string
	 */
	public function getEbucoreWidthUnit() : ?string
	{
		return $this->_ebucoreWidthUnit;
	}
	
}
