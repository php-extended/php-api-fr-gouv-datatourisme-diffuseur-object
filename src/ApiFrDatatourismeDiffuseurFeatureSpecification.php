<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurFeatureSpecification class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurFeatureSpecificationInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurFeatureSpecification implements ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The value of this feature specification.
	 * 
	 * @var ?string
	 */
	protected ?string $_schemaValue = null;
	
	/**
	 * Whether air conditioning is present.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_airConditioning = null;
	
	/**
	 * Whether the associated facility is charged.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_charged = null;
	
	/**
	 * The features of this specification.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurFeatureInterface>
	 */
	protected array $_features = [];
	
	/**
	 * The floor size.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface $_hasFloorSize = null;
	
	/**
	 * The layout of the room associated to this specification.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurLayoutInterface>
	 */
	protected array $_hasLayout = [];
	
	/**
	 * Whether internet is accessible.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_internetAccess = null;
	
	/**
	 * The max number of people in the area.
	 * 
	 * @var ?int
	 */
	protected ?int $_occupancy = null;
	
	/**
	 * Whether pets are allowed.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_petsAllowed = null;
	
	/**
	 * The count of available seats.
	 * 
	 * @var ?int
	 */
	protected ?int $_seatCount = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurFeatureSpecification with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the value of this feature specification.
	 * 
	 * @param ?string $schemaValue
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setSchemaValue(?string $schemaValue) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_schemaValue = $schemaValue;
		
		return $this;
	}
	
	/**
	 * Gets the value of this feature specification.
	 * 
	 * @return ?string
	 */
	public function getSchemaValue() : ?string
	{
		return $this->_schemaValue;
	}
	
	/**
	 * Sets whether air conditioning is present.
	 * 
	 * @param ?bool $airConditioning
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setAirConditioning(?bool $airConditioning) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_airConditioning = $airConditioning;
		
		return $this;
	}
	
	/**
	 * Gets whether air conditioning is present.
	 * 
	 * @return ?bool
	 */
	public function hasAirConditioning() : ?bool
	{
		return $this->_airConditioning;
	}
	
	/**
	 * Sets whether the associated facility is charged.
	 * 
	 * @param ?bool $charged
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setCharged(?bool $charged) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_charged = $charged;
		
		return $this;
	}
	
	/**
	 * Gets whether the associated facility is charged.
	 * 
	 * @return ?bool
	 */
	public function hasCharged() : ?bool
	{
		return $this->_charged;
	}
	
	/**
	 * Sets the features of this specification.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurFeatureInterface> $features
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setFeatures(array $features) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_features = $features;
		
		return $this;
	}
	
	/**
	 * Gets the features of this specification.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurFeatureInterface>
	 */
	public function getFeatures() : array
	{
		return $this->_features;
	}
	
	/**
	 * Sets the floor size.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface $hasFloorSize
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setHasFloorSize(?ApiFrDatatourismeDiffuseurQuantitativeValueInterface $hasFloorSize) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_hasFloorSize = $hasFloorSize;
		
		return $this;
	}
	
	/**
	 * Gets the floor size.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	public function getHasFloorSize() : ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	{
		return $this->_hasFloorSize;
	}
	
	/**
	 * Sets the layout of the room associated to this specification.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurLayoutInterface> $hasLayout
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setHasLayout(array $hasLayout) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_hasLayout = $hasLayout;
		
		return $this;
	}
	
	/**
	 * Gets the layout of the room associated to this specification.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurLayoutInterface>
	 */
	public function getHasLayout() : array
	{
		return $this->_hasLayout;
	}
	
	/**
	 * Sets whether internet is accessible.
	 * 
	 * @param ?bool $internetAccess
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setInternetAccess(?bool $internetAccess) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_internetAccess = $internetAccess;
		
		return $this;
	}
	
	/**
	 * Gets whether internet is accessible.
	 * 
	 * @return ?bool
	 */
	public function hasInternetAccess() : ?bool
	{
		return $this->_internetAccess;
	}
	
	/**
	 * Sets the max number of people in the area.
	 * 
	 * @param ?int $occupancy
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setOccupancy(?int $occupancy) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_occupancy = $occupancy;
		
		return $this;
	}
	
	/**
	 * Gets the max number of people in the area.
	 * 
	 * @return ?int
	 */
	public function getOccupancy() : ?int
	{
		return $this->_occupancy;
	}
	
	/**
	 * Sets whether pets are allowed.
	 * 
	 * @param ?bool $petsAllowed
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setPetsAllowed(?bool $petsAllowed) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_petsAllowed = $petsAllowed;
		
		return $this;
	}
	
	/**
	 * Gets whether pets are allowed.
	 * 
	 * @return ?bool
	 */
	public function hasPetsAllowed() : ?bool
	{
		return $this->_petsAllowed;
	}
	
	/**
	 * Sets the count of available seats.
	 * 
	 * @param ?int $seatCount
	 * @return ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	 */
	public function setSeatCount(?int $seatCount) : ApiFrDatatourismeDiffuseurFeatureSpecificationInterface
	{
		$this->_seatCount = $seatCount;
		
		return $this;
	}
	
	/**
	 * Gets the count of available seats.
	 * 
	 * @return ?int
	 */
	public function getSeatCount() : ?int
	{
		return $this->_seatCount;
	}
	
}
