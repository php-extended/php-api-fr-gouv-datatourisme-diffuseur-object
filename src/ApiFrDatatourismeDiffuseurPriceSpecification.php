<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurPriceSpecification class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurPriceSpecificationInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortVariable")
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiFrDatatourismeDiffuseurPriceSpecification implements ApiFrDatatourismeDiffuseurPriceSpecificationInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * Eligible quantity.
	 * 
	 * @var ?string
	 */
	protected ?string $_schemaEligibleQuantity = null;
	
	/**
	 * The max price.
	 * 
	 * @var array<int, float>
	 */
	protected array $_schemaMaxPrice = [];
	
	/**
	 * The min price.
	 * 
	 * @var array<int, float>
	 */
	protected array $_schemaMinPrice = [];
	
	/**
	 * The price.
	 * 
	 * @var ?float
	 */
	protected ?float $_schemaPrice = null;
	
	/**
	 * The price currency.
	 * 
	 * @var ?string
	 */
	protected ?string $_schemaPriceCurrency = null;
	
	/**
	 * Further information.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_additionalInformation = null;
	
	/**
	 * The audience targeted by this object.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	protected array $_hasAudience = [];
	
	/**
	 * The people audience this pricing applies on.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	protected array $_hasEligibleAudience = [];
	
	/**
	 * The pricing policy the pricing applies on. Ex: Base rate.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurPricingPolicyInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurPricingPolicyInterface $_hasEligiblePolicy = null;
	
	/**
	 * The pricing mode this pricing applies on : per day, per person...
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurPricingModeInterface>
	 */
	protected array $_hasPricingMode = [];
	
	/**
	 * Detailled offer on which the price applies. e.g. Menu, Bungalow ...
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurPricingOfferInterface>
	 */
	protected array $_hasPricingOffer = [];
	
	/**
	 * Season on which applies the price specification.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurPricingSeasonInterface>
	 */
	protected array $_hasPricingSeason = [];
	
	/**
	 * The translated properties.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	protected array $_hasTranslatedProperty = [];
	
	/**
	 * The period this pricing applies on.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurPeriodInterface>
	 */
	protected array $_appliesOnPeriod = [];
	
	/**
	 * The name of the specification.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_name = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurPriceSpecification with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets eligible quantity.
	 * 
	 * @param ?string $schemaEligibleQuantity
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setSchemaEligibleQuantity(?string $schemaEligibleQuantity) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_schemaEligibleQuantity = $schemaEligibleQuantity;
		
		return $this;
	}
	
	/**
	 * Gets eligible quantity.
	 * 
	 * @return ?string
	 */
	public function getSchemaEligibleQuantity() : ?string
	{
		return $this->_schemaEligibleQuantity;
	}
	
	/**
	 * Sets the max price.
	 * 
	 * @param array<int, float> $schemaMaxPrice
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setSchemaMaxPrice(array $schemaMaxPrice) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_schemaMaxPrice = $schemaMaxPrice;
		
		return $this;
	}
	
	/**
	 * Gets the max price.
	 * 
	 * @return array<int, float>
	 */
	public function getSchemaMaxPrice() : array
	{
		return $this->_schemaMaxPrice;
	}
	
	/**
	 * Sets the min price.
	 * 
	 * @param array<int, float> $schemaMinPrice
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setSchemaMinPrice(array $schemaMinPrice) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_schemaMinPrice = $schemaMinPrice;
		
		return $this;
	}
	
	/**
	 * Gets the min price.
	 * 
	 * @return array<int, float>
	 */
	public function getSchemaMinPrice() : array
	{
		return $this->_schemaMinPrice;
	}
	
	/**
	 * Sets the price.
	 * 
	 * @param ?float $schemaPrice
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setSchemaPrice(?float $schemaPrice) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_schemaPrice = $schemaPrice;
		
		return $this;
	}
	
	/**
	 * Gets the price.
	 * 
	 * @return ?float
	 */
	public function getSchemaPrice() : ?float
	{
		return $this->_schemaPrice;
	}
	
	/**
	 * Sets the price currency.
	 * 
	 * @param ?string $schemaPriceCurrency
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setSchemaPriceCurrency(?string $schemaPriceCurrency) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_schemaPriceCurrency = $schemaPriceCurrency;
		
		return $this;
	}
	
	/**
	 * Gets the price currency.
	 * 
	 * @return ?string
	 */
	public function getSchemaPriceCurrency() : ?string
	{
		return $this->_schemaPriceCurrency;
	}
	
	/**
	 * Sets further information.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $additionalInformation
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setAdditionalInformation(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $additionalInformation) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_additionalInformation = $additionalInformation;
		
		return $this;
	}
	
	/**
	 * Gets further information.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getAdditionalInformation() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_additionalInformation;
	}
	
	/**
	 * Sets the audience targeted by this object.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAudienceInterface> $hasAudience
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setHasAudience(array $hasAudience) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_hasAudience = $hasAudience;
		
		return $this;
	}
	
	/**
	 * Gets the audience targeted by this object.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	public function getHasAudience() : array
	{
		return $this->_hasAudience;
	}
	
	/**
	 * Sets the people audience this pricing applies on.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAudienceInterface> $hasEligibleAudience
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setHasEligibleAudience(array $hasEligibleAudience) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_hasEligibleAudience = $hasEligibleAudience;
		
		return $this;
	}
	
	/**
	 * Gets the people audience this pricing applies on.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	public function getHasEligibleAudience() : array
	{
		return $this->_hasEligibleAudience;
	}
	
	/**
	 * Sets the pricing policy the pricing applies on. Ex: Base rate.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurPricingPolicyInterface $hasEligiblePolicy
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setHasEligiblePolicy(?ApiFrDatatourismeDiffuseurPricingPolicyInterface $hasEligiblePolicy) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_hasEligiblePolicy = $hasEligiblePolicy;
		
		return $this;
	}
	
	/**
	 * Gets the pricing policy the pricing applies on. Ex: Base rate.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurPricingPolicyInterface
	 */
	public function getHasEligiblePolicy() : ?ApiFrDatatourismeDiffuseurPricingPolicyInterface
	{
		return $this->_hasEligiblePolicy;
	}
	
	/**
	 * Sets the pricing mode this pricing applies on : per day, per person...
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurPricingModeInterface> $hasPricingMode
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setHasPricingMode(array $hasPricingMode) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_hasPricingMode = $hasPricingMode;
		
		return $this;
	}
	
	/**
	 * Gets the pricing mode this pricing applies on : per day, per person...
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPricingModeInterface>
	 */
	public function getHasPricingMode() : array
	{
		return $this->_hasPricingMode;
	}
	
	/**
	 * Sets detailled offer on which the price applies. e.g. Menu, Bungalow ...
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurPricingOfferInterface> $hasPricingOffer
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setHasPricingOffer(array $hasPricingOffer) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_hasPricingOffer = $hasPricingOffer;
		
		return $this;
	}
	
	/**
	 * Gets detailled offer on which the price applies. e.g. Menu, Bungalow ...
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPricingOfferInterface>
	 */
	public function getHasPricingOffer() : array
	{
		return $this->_hasPricingOffer;
	}
	
	/**
	 * Sets season on which applies the price specification.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurPricingSeasonInterface> $hasPricingSeason
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setHasPricingSeason(array $hasPricingSeason) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_hasPricingSeason = $hasPricingSeason;
		
		return $this;
	}
	
	/**
	 * Gets season on which applies the price specification.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPricingSeasonInterface>
	 */
	public function getHasPricingSeason() : array
	{
		return $this->_hasPricingSeason;
	}
	
	/**
	 * Sets the translated properties.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface> $hasTranslatedProperty
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setHasTranslatedProperty(array $hasTranslatedProperty) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_hasTranslatedProperty = $hasTranslatedProperty;
		
		return $this;
	}
	
	/**
	 * Gets the translated properties.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array
	{
		return $this->_hasTranslatedProperty;
	}
	
	/**
	 * Sets the period this pricing applies on.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurPeriodInterface> $appliesOnPeriod
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setAppliesOnPeriod(array $appliesOnPeriod) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_appliesOnPeriod = $appliesOnPeriod;
		
		return $this;
	}
	
	/**
	 * Gets the period this pricing applies on.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPeriodInterface>
	 */
	public function getAppliesOnPeriod() : array
	{
		return $this->_appliesOnPeriod;
	}
	
	/**
	 * Sets the name of the specification.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $name
	 * @return ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	 */
	public function setName(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $name) : ApiFrDatatourismeDiffuseurPriceSpecificationInterface
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * Gets the name of the specification.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getName() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_name;
	}
	
}
