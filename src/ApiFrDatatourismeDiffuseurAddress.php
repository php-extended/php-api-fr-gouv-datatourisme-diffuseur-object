<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurAddress class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurAddressInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurAddress implements ApiFrDatatourismeDiffuseurAddressInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The locality.
	 * 
	 * @var ?string
	 */
	protected ?string $_schemaAddressLocality = null;
	
	/**
	 * The post office box number.
	 * 
	 * @var ?string
	 */
	protected ?string $_schemaPostOfficeBoxNumber = null;
	
	/**
	 * The postal code.
	 * 
	 * @var ?string
	 */
	protected ?string $_schemaPostalCode = null;
	
	/**
	 * The street address.
	 * 
	 * @var array<int, string>
	 */
	protected array $_schemaStreetAddress = [];
	
	/**
	 * The cedex.
	 * 
	 * @var ?string
	 */
	protected ?string $_cedex = null;
	
	/**
	 * The city.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurCityInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurCityInterface $_hasAddressCity = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurAddress with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurAddressInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurAddressInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurAddressInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurAddressInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the locality.
	 * 
	 * @param ?string $schemaAddressLocality
	 * @return ApiFrDatatourismeDiffuseurAddressInterface
	 */
	public function setSchemaAddressLocality(?string $schemaAddressLocality) : ApiFrDatatourismeDiffuseurAddressInterface
	{
		$this->_schemaAddressLocality = $schemaAddressLocality;
		
		return $this;
	}
	
	/**
	 * Gets the locality.
	 * 
	 * @return ?string
	 */
	public function getSchemaAddressLocality() : ?string
	{
		return $this->_schemaAddressLocality;
	}
	
	/**
	 * Sets the post office box number.
	 * 
	 * @param ?string $schemaPostOfficeBoxNumber
	 * @return ApiFrDatatourismeDiffuseurAddressInterface
	 */
	public function setSchemaPostOfficeBoxNumber(?string $schemaPostOfficeBoxNumber) : ApiFrDatatourismeDiffuseurAddressInterface
	{
		$this->_schemaPostOfficeBoxNumber = $schemaPostOfficeBoxNumber;
		
		return $this;
	}
	
	/**
	 * Gets the post office box number.
	 * 
	 * @return ?string
	 */
	public function getSchemaPostOfficeBoxNumber() : ?string
	{
		return $this->_schemaPostOfficeBoxNumber;
	}
	
	/**
	 * Sets the postal code.
	 * 
	 * @param ?string $schemaPostalCode
	 * @return ApiFrDatatourismeDiffuseurAddressInterface
	 */
	public function setSchemaPostalCode(?string $schemaPostalCode) : ApiFrDatatourismeDiffuseurAddressInterface
	{
		$this->_schemaPostalCode = $schemaPostalCode;
		
		return $this;
	}
	
	/**
	 * Gets the postal code.
	 * 
	 * @return ?string
	 */
	public function getSchemaPostalCode() : ?string
	{
		return $this->_schemaPostalCode;
	}
	
	/**
	 * Sets the street address.
	 * 
	 * @param array<int, string> $schemaStreetAddress
	 * @return ApiFrDatatourismeDiffuseurAddressInterface
	 */
	public function setSchemaStreetAddress(array $schemaStreetAddress) : ApiFrDatatourismeDiffuseurAddressInterface
	{
		$this->_schemaStreetAddress = $schemaStreetAddress;
		
		return $this;
	}
	
	/**
	 * Gets the street address.
	 * 
	 * @return array<int, string>
	 */
	public function getSchemaStreetAddress() : array
	{
		return $this->_schemaStreetAddress;
	}
	
	/**
	 * Sets the cedex.
	 * 
	 * @param ?string $cedex
	 * @return ApiFrDatatourismeDiffuseurAddressInterface
	 */
	public function setCedex(?string $cedex) : ApiFrDatatourismeDiffuseurAddressInterface
	{
		$this->_cedex = $cedex;
		
		return $this;
	}
	
	/**
	 * Gets the cedex.
	 * 
	 * @return ?string
	 */
	public function getCedex() : ?string
	{
		return $this->_cedex;
	}
	
	/**
	 * Sets the city.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurCityInterface $hasAddressCity
	 * @return ApiFrDatatourismeDiffuseurAddressInterface
	 */
	public function setHasAddressCity(?ApiFrDatatourismeDiffuseurCityInterface $hasAddressCity) : ApiFrDatatourismeDiffuseurAddressInterface
	{
		$this->_hasAddressCity = $hasAddressCity;
		
		return $this;
	}
	
	/**
	 * Gets the city.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurCityInterface
	 */
	public function getHasAddressCity() : ?ApiFrDatatourismeDiffuseurCityInterface
	{
		return $this->_hasAddressCity;
	}
	
}
