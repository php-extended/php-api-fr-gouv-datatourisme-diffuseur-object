<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurPeriod class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurPeriodInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurPeriod implements ApiFrDatatourismeDiffuseurPeriodInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The day of week this Period applies on.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurDayOfWeekInterface>
	 */
	protected array $_appliesOnDay = [];
	
	/**
	 * End date.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_endDate = null;
	
	/**
	 * End time.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_endTime = null;
	
	/**
	 * The list of all elements that were translated for this object.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	protected array $_hasTranslatedProperty = [];
	
	/**
	 * More information about opening period.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_openingDetails = null;
	
	/**
	 * Start date.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_startDate = null;
	
	/**
	 * Start time.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_startTime = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurPeriod with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurPeriodInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurPeriodInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurPeriodInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurPeriodInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the day of week this Period applies on.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurDayOfWeekInterface> $appliesOnDay
	 * @return ApiFrDatatourismeDiffuseurPeriodInterface
	 */
	public function setAppliesOnDay(array $appliesOnDay) : ApiFrDatatourismeDiffuseurPeriodInterface
	{
		$this->_appliesOnDay = $appliesOnDay;
		
		return $this;
	}
	
	/**
	 * Gets the day of week this Period applies on.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurDayOfWeekInterface>
	 */
	public function getAppliesOnDay() : array
	{
		return $this->_appliesOnDay;
	}
	
	/**
	 * Sets end date.
	 * 
	 * @param ?DateTimeInterface $endDate
	 * @return ApiFrDatatourismeDiffuseurPeriodInterface
	 */
	public function setEndDate(?DateTimeInterface $endDate) : ApiFrDatatourismeDiffuseurPeriodInterface
	{
		$this->_endDate = $endDate;
		
		return $this;
	}
	
	/**
	 * Gets end date.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getEndDate() : ?DateTimeInterface
	{
		return $this->_endDate;
	}
	
	/**
	 * Sets end time.
	 * 
	 * @param ?DateTimeInterface $endTime
	 * @return ApiFrDatatourismeDiffuseurPeriodInterface
	 */
	public function setEndTime(?DateTimeInterface $endTime) : ApiFrDatatourismeDiffuseurPeriodInterface
	{
		$this->_endTime = $endTime;
		
		return $this;
	}
	
	/**
	 * Gets end time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getEndTime() : ?DateTimeInterface
	{
		return $this->_endTime;
	}
	
	/**
	 * Sets the list of all elements that were translated for this object.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface> $hasTranslatedProperty
	 * @return ApiFrDatatourismeDiffuseurPeriodInterface
	 */
	public function setHasTranslatedProperty(array $hasTranslatedProperty) : ApiFrDatatourismeDiffuseurPeriodInterface
	{
		$this->_hasTranslatedProperty = $hasTranslatedProperty;
		
		return $this;
	}
	
	/**
	 * Gets the list of all elements that were translated for this object.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array
	{
		return $this->_hasTranslatedProperty;
	}
	
	/**
	 * Sets more information about opening period.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $openingDetails
	 * @return ApiFrDatatourismeDiffuseurPeriodInterface
	 */
	public function setOpeningDetails(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $openingDetails) : ApiFrDatatourismeDiffuseurPeriodInterface
	{
		$this->_openingDetails = $openingDetails;
		
		return $this;
	}
	
	/**
	 * Gets more information about opening period.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getOpeningDetails() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_openingDetails;
	}
	
	/**
	 * Sets start date.
	 * 
	 * @param ?DateTimeInterface $startDate
	 * @return ApiFrDatatourismeDiffuseurPeriodInterface
	 */
	public function setStartDate(?DateTimeInterface $startDate) : ApiFrDatatourismeDiffuseurPeriodInterface
	{
		$this->_startDate = $startDate;
		
		return $this;
	}
	
	/**
	 * Gets start date.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getStartDate() : ?DateTimeInterface
	{
		return $this->_startDate;
	}
	
	/**
	 * Sets start time.
	 * 
	 * @param ?DateTimeInterface $startTime
	 * @return ApiFrDatatourismeDiffuseurPeriodInterface
	 */
	public function setStartTime(?DateTimeInterface $startTime) : ApiFrDatatourismeDiffuseurPeriodInterface
	{
		$this->_startTime = $startTime;
		
		return $this;
	}
	
	/**
	 * Gets start time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getStartTime() : ?DateTimeInterface
	{
		return $this->_startTime;
	}
	
}
