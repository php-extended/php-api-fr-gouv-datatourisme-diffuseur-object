<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurPointOfInterest class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurPointOfInterestInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortVariable")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ApiFrDatatourismeDiffuseurPointOfInterest implements ApiFrDatatourismeDiffuseurPointOfInterestInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The source identifier of the PointOfInterest.
	 * 
	 * @var ?string
	 */
	protected ?string $_dcIdentifier = null;
	
	/**
	 * The slots of list.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurOrderedListSlotInterface>
	 */
	protected array $_oloSlot = [];
	
	/**
	 * (FMA) The dates at the end of periods.
	 * 
	 * @var array<int, DateTimeInterface>
	 */
	protected array $_schemaEndDate = [];
	
	/**
	 * (FMA) The dates at the beginning of periods.
	 * 
	 * @var array<int, DateTimeInterface>
	 */
	protected array $_schemaStartDate = [];
	
	/**
	 * The comments.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_rdfsComment = null;
	
	/**
	 * The label.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_rdfsLabel = null;
	
	/**
	 * The same resource pointers.
	 * 
	 * @var array<int, UriInterface>
	 */
	protected array $_owlSameAs = [];
	
	/**
	 * Whether the PointOfInterest is opened under covid rules.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_covid19InOperationConfirmed = null;
	
	/**
	 * Whether the periods of the PointOfInterest are updated under covid
	 * rules.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_covid19OpeningPeriodsConfirmed = null;
	
	/**
	 * Special measures put in place.
	 * 
	 * @var ?string
	 */
	protected ?string $_covid19SpecialMeasures = null;
	
	/**
	 * Number of persons allowed in the POI.
	 * 
	 * @var ?int
	 */
	protected ?int $_allowedPersons = null;
	
	/**
	 * The languages.
	 * 
	 * @var array<int, string>
	 */
	protected array $_availableLanguage = [];
	
	/**
	 * The date of creation of the resource.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_creationDate = null;
	
	/**
	 * (TOUR) The expected time to travel the path or path-stage. Unit is
	 * minute.
	 * 
	 * @var ?float
	 */
	protected ?float $_duration = null;
	
	/**
	 * Whether the visit is guided.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_guided = null;
	
	/**
	 * The agent to contact in case of administrative matters.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	protected array $_hasAdministrativeContact = [];
	
	/**
	 * The audiences this poi is diriged to.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	protected array $_hasAudience = [];
	
	/**
	 * The architectural style.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	protected array $_hasArchitecturalStyle = [];
	
	/**
	 * The agent that created this PointOfInterest.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurAgentInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurAgentInterface $_hasBeenCreatedBy = null;
	
	/**
	 * The people that published this PointOfInterest.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	protected array $_hasBeenPublishedBy = [];
	
	/**
	 * The agent to contact in case of booking matters.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	protected array $_hasBookingContact = [];
	
	/**
	 * The agent to contact in case of communication matters.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	protected array $_hasCommunicationContact = [];
	
	/**
	 * The targets.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	protected array $_hasClientTarget = [];
	
	/**
	 * The contacts.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	protected array $_hasContact = [];
	
	/**
	 * The descriptions.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurDescriptionInterface>
	 */
	protected array $_hasDescription = [];
	
	/**
	 * The features information (amenities, services infos) this POI provides.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurFeatureSpecificationInterface>
	 */
	protected array $_hasFeature = [];
	
	/**
	 * Has floor size.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface $_hasFloorSize = null;
	
	/**
	 * The geographical reach of this POI: international... local.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurGeographicReachInterface>
	 */
	protected array $_hasGeographicReach = [];
	
	/**
	 * The main representation.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	protected array $_hasMainRepresentation = [];
	
	/**
	 * The agent to contact in case of management / director matters.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	protected array $_hasManagementContact = [];
	
	/**
	 * The themes.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	protected array $_hasNeighborhood = [];
	
	/**
	 * The representation.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	protected array $_hasRepresentation = [];
	
	/**
	 * A Review which rates this POI.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurReviewInterface>
	 */
	protected array $_hasReview = [];
	
	/**
	 * The theme.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	protected array $_hasTheme = [];
	
	/**
	 * (TOUR) The route type : round trip, loop or open jaw itinerary.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurTourTypeInterface>
	 */
	protected array $_hasTourType = [];
	
	/**
	 * The translated properties.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	protected array $_hasTranslatedProperty = [];
	
	/**
	 * The high difference of the path (difference between the lowest and the
	 * higest altitude). Unit is meters.
	 * 
	 * @var ?float
	 */
	protected ?float $_highDifference = null;
	
	/**
	 * The locations.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurLocationInterface>
	 */
	protected array $_isLocatedAt = [];
	
	/**
	 * The owner.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	protected array $_isOwnedBy = [];
	
	/**
	 * Last update in provider.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_lastUpdate = null;
	
	/**
	 * Last update in datatourisme.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_lastUpdateDatatourisme;
	
	/**
	 * (TOUR) The altitude of the highest point in the path. Unit is meter.
	 * 
	 * @var ?float
	 */
	protected ?float $_maxAltitude = null;
	
	/**
	 * (TOUR) The altitude of the lowest point in the path. Unit is meter.
	 * 
	 * @var ?float
	 */
	protected ?float $_minAltitude = null;
	
	/**
	 * The payment services provided by this Product (cost per day, per person,
	 * price...).
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurOfferInterface>
	 */
	protected array $_offers = [];
	
	/**
	 * The type of food provided.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	protected array $_providesCuisineOfType = [];
	
	/**
	 * The type of provided food.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	protected array $_providesFoodProduct = [];
	
	/**
	 * Gets whether the PointOfInterest is suitable for people with reduced
	 * mobility.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_reducedMobilityAccess = null;
	
	/**
	 * True when the Tasting provides saling on site.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_saleOnSite = null;
	
	/**
	 * True if food takeaway is allowed.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_takeAway = null;
	
	/**
	 * (FMA) The period this event takes place at.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurPeriodInterface>
	 */
	protected array $_takesPlaceAt = [];
	
	/**
	 * (TOUR) The distance between the start and the end of the stage or the
	 * path. Unit in meters.
	 * 
	 * @var ?float
	 */
	protected ?float $_tourDistance = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurPointOfInterest with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 * @param DateTimeInterface $lastUpdateDatatourisme
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(UriInterface $id, array $type, DateTimeInterface $lastUpdateDatatourisme)
	{
		$this->setId($id);
		$this->setType($type);
		$this->setLastUpdateDatatourisme($lastUpdateDatatourisme);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the source identifier of the PointOfInterest.
	 * 
	 * @param ?string $dcIdentifier
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setDcIdentifier(?string $dcIdentifier) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_dcIdentifier = $dcIdentifier;
		
		return $this;
	}
	
	/**
	 * Gets the source identifier of the PointOfInterest.
	 * 
	 * @return ?string
	 */
	public function getDcIdentifier() : ?string
	{
		return $this->_dcIdentifier;
	}
	
	/**
	 * Sets the slots of list.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurOrderedListSlotInterface> $oloSlot
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setOloSlot(array $oloSlot) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_oloSlot = $oloSlot;
		
		return $this;
	}
	
	/**
	 * Gets the slots of list.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurOrderedListSlotInterface>
	 */
	public function getOloSlot() : array
	{
		return $this->_oloSlot;
	}
	
	/**
	 * Sets (FMA) The dates at the end of periods.
	 * 
	 * @param array<int, DateTimeInterface> $schemaEndDate
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setSchemaEndDate(array $schemaEndDate) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_schemaEndDate = $schemaEndDate;
		
		return $this;
	}
	
	/**
	 * Gets (FMA) The dates at the end of periods.
	 * 
	 * @return array<int, DateTimeInterface>
	 */
	public function getSchemaEndDate() : array
	{
		return $this->_schemaEndDate;
	}
	
	/**
	 * Sets (FMA) The dates at the beginning of periods.
	 * 
	 * @param array<int, DateTimeInterface> $schemaStartDate
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setSchemaStartDate(array $schemaStartDate) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_schemaStartDate = $schemaStartDate;
		
		return $this;
	}
	
	/**
	 * Gets (FMA) The dates at the beginning of periods.
	 * 
	 * @return array<int, DateTimeInterface>
	 */
	public function getSchemaStartDate() : array
	{
		return $this->_schemaStartDate;
	}
	
	/**
	 * Sets the comments.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsComment
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setRdfsComment(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsComment) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_rdfsComment = $rdfsComment;
		
		return $this;
	}
	
	/**
	 * Gets the comments.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsComment() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_rdfsComment;
	}
	
	/**
	 * Sets the label.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setRdfsLabel(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $rdfsLabel) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_rdfsLabel = $rdfsLabel;
		
		return $this;
	}
	
	/**
	 * Gets the label.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getRdfsLabel() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_rdfsLabel;
	}
	
	/**
	 * Sets the same resource pointers.
	 * 
	 * @param array<int, UriInterface> $owlSameAs
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setOwlSameAs(array $owlSameAs) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_owlSameAs = $owlSameAs;
		
		return $this;
	}
	
	/**
	 * Gets the same resource pointers.
	 * 
	 * @return array<int, UriInterface>
	 */
	public function getOwlSameAs() : array
	{
		return $this->_owlSameAs;
	}
	
	/**
	 * Sets whether the PointOfInterest is opened under covid rules.
	 * 
	 * @param ?bool $covid19InOperationConfirmed
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setCovid19InOperationConfirmed(?bool $covid19InOperationConfirmed) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_covid19InOperationConfirmed = $covid19InOperationConfirmed;
		
		return $this;
	}
	
	/**
	 * Gets whether the PointOfInterest is opened under covid rules.
	 * 
	 * @return ?bool
	 */
	public function hasCovid19InOperationConfirmed() : ?bool
	{
		return $this->_covid19InOperationConfirmed;
	}
	
	/**
	 * Sets whether the periods of the PointOfInterest are updated under covid
	 * rules.
	 * 
	 * @param ?bool $covid19OpeningPeriodsConfirmed
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setCovid19OpeningPeriodsConfirmed(?bool $covid19OpeningPeriodsConfirmed) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_covid19OpeningPeriodsConfirmed = $covid19OpeningPeriodsConfirmed;
		
		return $this;
	}
	
	/**
	 * Gets whether the periods of the PointOfInterest are updated under covid
	 * rules.
	 * 
	 * @return ?bool
	 */
	public function hasCovid19OpeningPeriodsConfirmed() : ?bool
	{
		return $this->_covid19OpeningPeriodsConfirmed;
	}
	
	/**
	 * Sets special measures put in place.
	 * 
	 * @param ?string $covid19SpecialMeasures
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setCovid19SpecialMeasures(?string $covid19SpecialMeasures) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_covid19SpecialMeasures = $covid19SpecialMeasures;
		
		return $this;
	}
	
	/**
	 * Gets special measures put in place.
	 * 
	 * @return ?string
	 */
	public function getCovid19SpecialMeasures() : ?string
	{
		return $this->_covid19SpecialMeasures;
	}
	
	/**
	 * Sets number of persons allowed in the POI.
	 * 
	 * @param ?int $allowedPersons
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setAllowedPersons(?int $allowedPersons) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_allowedPersons = $allowedPersons;
		
		return $this;
	}
	
	/**
	 * Gets number of persons allowed in the POI.
	 * 
	 * @return ?int
	 */
	public function getAllowedPersons() : ?int
	{
		return $this->_allowedPersons;
	}
	
	/**
	 * Sets the languages.
	 * 
	 * @param array<int, string> $availableLanguage
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setAvailableLanguage(array $availableLanguage) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_availableLanguage = $availableLanguage;
		
		return $this;
	}
	
	/**
	 * Gets the languages.
	 * 
	 * @return array<int, string>
	 */
	public function getAvailableLanguage() : array
	{
		return $this->_availableLanguage;
	}
	
	/**
	 * Sets the date of creation of the resource.
	 * 
	 * @param ?DateTimeInterface $creationDate
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setCreationDate(?DateTimeInterface $creationDate) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_creationDate = $creationDate;
		
		return $this;
	}
	
	/**
	 * Gets the date of creation of the resource.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getCreationDate() : ?DateTimeInterface
	{
		return $this->_creationDate;
	}
	
	/**
	 * Sets (TOUR) The expected time to travel the path or path-stage. Unit is
	 * minute.
	 * 
	 * @param ?float $duration
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setDuration(?float $duration) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_duration = $duration;
		
		return $this;
	}
	
	/**
	 * Gets (TOUR) The expected time to travel the path or path-stage. Unit is
	 * minute.
	 * 
	 * @return ?float
	 */
	public function getDuration() : ?float
	{
		return $this->_duration;
	}
	
	/**
	 * Sets whether the visit is guided.
	 * 
	 * @param ?bool $guided
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setGuided(?bool $guided) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_guided = $guided;
		
		return $this;
	}
	
	/**
	 * Gets whether the visit is guided.
	 * 
	 * @return ?bool
	 */
	public function hasGuided() : ?bool
	{
		return $this->_guided;
	}
	
	/**
	 * Sets the agent to contact in case of administrative matters.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAgentInterface> $hasAdministrativeContact
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasAdministrativeContact(array $hasAdministrativeContact) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasAdministrativeContact = $hasAdministrativeContact;
		
		return $this;
	}
	
	/**
	 * Gets the agent to contact in case of administrative matters.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasAdministrativeContact() : array
	{
		return $this->_hasAdministrativeContact;
	}
	
	/**
	 * Sets the audiences this poi is diriged to.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAudienceInterface> $hasAudience
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasAudience(array $hasAudience) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasAudience = $hasAudience;
		
		return $this;
	}
	
	/**
	 * Gets the audiences this poi is diriged to.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	public function getHasAudience() : array
	{
		return $this->_hasAudience;
	}
	
	/**
	 * Sets the architectural style.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurThemeInterface> $hasArchitecturalStyle
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasArchitecturalStyle(array $hasArchitecturalStyle) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasArchitecturalStyle = $hasArchitecturalStyle;
		
		return $this;
	}
	
	/**
	 * Gets the architectural style.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	public function getHasArchitecturalStyle() : array
	{
		return $this->_hasArchitecturalStyle;
	}
	
	/**
	 * Sets the agent that created this PointOfInterest.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurAgentInterface $hasBeenCreatedBy
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasBeenCreatedBy(?ApiFrDatatourismeDiffuseurAgentInterface $hasBeenCreatedBy) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasBeenCreatedBy = $hasBeenCreatedBy;
		
		return $this;
	}
	
	/**
	 * Gets the agent that created this PointOfInterest.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurAgentInterface
	 */
	public function getHasBeenCreatedBy() : ?ApiFrDatatourismeDiffuseurAgentInterface
	{
		return $this->_hasBeenCreatedBy;
	}
	
	/**
	 * Sets the people that published this PointOfInterest.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAgentInterface> $hasBeenPublishedBy
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasBeenPublishedBy(array $hasBeenPublishedBy) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasBeenPublishedBy = $hasBeenPublishedBy;
		
		return $this;
	}
	
	/**
	 * Gets the people that published this PointOfInterest.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasBeenPublishedBy() : array
	{
		return $this->_hasBeenPublishedBy;
	}
	
	/**
	 * Sets the agent to contact in case of booking matters.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAgentInterface> $hasBookingContact
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasBookingContact(array $hasBookingContact) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasBookingContact = $hasBookingContact;
		
		return $this;
	}
	
	/**
	 * Gets the agent to contact in case of booking matters.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasBookingContact() : array
	{
		return $this->_hasBookingContact;
	}
	
	/**
	 * Sets the agent to contact in case of communication matters.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAgentInterface> $hasCommunicationContact
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasCommunicationContact(array $hasCommunicationContact) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasCommunicationContact = $hasCommunicationContact;
		
		return $this;
	}
	
	/**
	 * Gets the agent to contact in case of communication matters.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasCommunicationContact() : array
	{
		return $this->_hasCommunicationContact;
	}
	
	/**
	 * Sets the targets.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAudienceInterface> $hasClientTarget
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasClientTarget(array $hasClientTarget) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasClientTarget = $hasClientTarget;
		
		return $this;
	}
	
	/**
	 * Gets the targets.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAudienceInterface>
	 */
	public function getHasClientTarget() : array
	{
		return $this->_hasClientTarget;
	}
	
	/**
	 * Sets the contacts.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAgentInterface> $hasContact
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasContact(array $hasContact) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasContact = $hasContact;
		
		return $this;
	}
	
	/**
	 * Gets the contacts.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasContact() : array
	{
		return $this->_hasContact;
	}
	
	/**
	 * Sets the descriptions.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurDescriptionInterface> $hasDescription
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasDescription(array $hasDescription) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasDescription = $hasDescription;
		
		return $this;
	}
	
	/**
	 * Gets the descriptions.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurDescriptionInterface>
	 */
	public function getHasDescription() : array
	{
		return $this->_hasDescription;
	}
	
	/**
	 * Sets the features information (amenities, services infos) this POI
	 * provides.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurFeatureSpecificationInterface> $hasFeature
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasFeature(array $hasFeature) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasFeature = $hasFeature;
		
		return $this;
	}
	
	/**
	 * Gets the features information (amenities, services infos) this POI
	 * provides.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurFeatureSpecificationInterface>
	 */
	public function getHasFeature() : array
	{
		return $this->_hasFeature;
	}
	
	/**
	 * Sets has floor size.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface $hasFloorSize
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasFloorSize(?ApiFrDatatourismeDiffuseurQuantitativeValueInterface $hasFloorSize) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasFloorSize = $hasFloorSize;
		
		return $this;
	}
	
	/**
	 * Gets has floor size.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	 */
	public function getHasFloorSize() : ?ApiFrDatatourismeDiffuseurQuantitativeValueInterface
	{
		return $this->_hasFloorSize;
	}
	
	/**
	 * Sets the geographical reach of this POI: international... local.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurGeographicReachInterface> $hasGeographicReach
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasGeographicReach(array $hasGeographicReach) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasGeographicReach = $hasGeographicReach;
		
		return $this;
	}
	
	/**
	 * Gets the geographical reach of this POI: international... local.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurGeographicReachInterface>
	 */
	public function getHasGeographicReach() : array
	{
		return $this->_hasGeographicReach;
	}
	
	/**
	 * Sets the main representation.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurMediaInterface> $hasMainRepresentation
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasMainRepresentation(array $hasMainRepresentation) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasMainRepresentation = $hasMainRepresentation;
		
		return $this;
	}
	
	/**
	 * Gets the main representation.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	public function getHasMainRepresentation() : array
	{
		return $this->_hasMainRepresentation;
	}
	
	/**
	 * Sets the agent to contact in case of management / director matters.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAgentInterface> $hasManagementContact
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasManagementContact(array $hasManagementContact) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasManagementContact = $hasManagementContact;
		
		return $this;
	}
	
	/**
	 * Gets the agent to contact in case of management / director matters.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getHasManagementContact() : array
	{
		return $this->_hasManagementContact;
	}
	
	/**
	 * Sets the themes.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurThemeInterface> $hasNeighborhood
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasNeighborhood(array $hasNeighborhood) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasNeighborhood = $hasNeighborhood;
		
		return $this;
	}
	
	/**
	 * Gets the themes.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	public function getHasNeighborhood() : array
	{
		return $this->_hasNeighborhood;
	}
	
	/**
	 * Sets the representation.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurMediaInterface> $hasRepresentation
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasRepresentation(array $hasRepresentation) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasRepresentation = $hasRepresentation;
		
		return $this;
	}
	
	/**
	 * Gets the representation.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurMediaInterface>
	 */
	public function getHasRepresentation() : array
	{
		return $this->_hasRepresentation;
	}
	
	/**
	 * Sets a Review which rates this POI.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurReviewInterface> $hasReview
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasReview(array $hasReview) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasReview = $hasReview;
		
		return $this;
	}
	
	/**
	 * Gets a Review which rates this POI.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurReviewInterface>
	 */
	public function getHasReview() : array
	{
		return $this->_hasReview;
	}
	
	/**
	 * Sets the theme.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurThemeInterface> $hasTheme
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasTheme(array $hasTheme) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasTheme = $hasTheme;
		
		return $this;
	}
	
	/**
	 * Gets the theme.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	public function getHasTheme() : array
	{
		return $this->_hasTheme;
	}
	
	/**
	 * Sets (TOUR) The route type : round trip, loop or open jaw itinerary.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurTourTypeInterface> $hasTourType
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasTourType(array $hasTourType) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasTourType = $hasTourType;
		
		return $this;
	}
	
	/**
	 * Gets (TOUR) The route type : round trip, loop or open jaw itinerary.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTourTypeInterface>
	 */
	public function getHasTourType() : array
	{
		return $this->_hasTourType;
	}
	
	/**
	 * Sets the translated properties.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface> $hasTranslatedProperty
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHasTranslatedProperty(array $hasTranslatedProperty) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_hasTranslatedProperty = $hasTranslatedProperty;
		
		return $this;
	}
	
	/**
	 * Gets the translated properties.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array
	{
		return $this->_hasTranslatedProperty;
	}
	
	/**
	 * Sets the high difference of the path (difference between the lowest and
	 * the higest altitude). Unit is meters.
	 * 
	 * @param ?float $highDifference
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setHighDifference(?float $highDifference) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_highDifference = $highDifference;
		
		return $this;
	}
	
	/**
	 * Gets the high difference of the path (difference between the lowest and
	 * the higest altitude). Unit is meters.
	 * 
	 * @return ?float
	 */
	public function getHighDifference() : ?float
	{
		return $this->_highDifference;
	}
	
	/**
	 * Sets the locations.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurLocationInterface> $isLocatedAt
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setIsLocatedAt(array $isLocatedAt) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_isLocatedAt = $isLocatedAt;
		
		return $this;
	}
	
	/**
	 * Gets the locations.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurLocationInterface>
	 */
	public function getIsLocatedAt() : array
	{
		return $this->_isLocatedAt;
	}
	
	/**
	 * Sets the owner.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAgentInterface> $isOwnedBy
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setIsOwnedBy(array $isOwnedBy) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_isOwnedBy = $isOwnedBy;
		
		return $this;
	}
	
	/**
	 * Gets the owner.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAgentInterface>
	 */
	public function getIsOwnedBy() : array
	{
		return $this->_isOwnedBy;
	}
	
	/**
	 * Sets last update in provider.
	 * 
	 * @param ?DateTimeInterface $lastUpdate
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setLastUpdate(?DateTimeInterface $lastUpdate) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_lastUpdate = $lastUpdate;
		
		return $this;
	}
	
	/**
	 * Gets last update in provider.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getLastUpdate() : ?DateTimeInterface
	{
		return $this->_lastUpdate;
	}
	
	/**
	 * Sets last update in datatourisme.
	 * 
	 * @param DateTimeInterface $lastUpdateDatatourisme
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setLastUpdateDatatourisme(DateTimeInterface $lastUpdateDatatourisme) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_lastUpdateDatatourisme = $lastUpdateDatatourisme;
		
		return $this;
	}
	
	/**
	 * Gets last update in datatourisme.
	 * 
	 * @return DateTimeInterface
	 */
	public function getLastUpdateDatatourisme() : DateTimeInterface
	{
		return $this->_lastUpdateDatatourisme;
	}
	
	/**
	 * Sets (TOUR) The altitude of the highest point in the path. Unit is
	 * meter.
	 * 
	 * @param ?float $maxAltitude
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setMaxAltitude(?float $maxAltitude) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_maxAltitude = $maxAltitude;
		
		return $this;
	}
	
	/**
	 * Gets (TOUR) The altitude of the highest point in the path. Unit is
	 * meter.
	 * 
	 * @return ?float
	 */
	public function getMaxAltitude() : ?float
	{
		return $this->_maxAltitude;
	}
	
	/**
	 * Sets (TOUR) The altitude of the lowest point in the path. Unit is meter.
	 * 
	 * @param ?float $minAltitude
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setMinAltitude(?float $minAltitude) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_minAltitude = $minAltitude;
		
		return $this;
	}
	
	/**
	 * Gets (TOUR) The altitude of the lowest point in the path. Unit is meter.
	 * 
	 * @return ?float
	 */
	public function getMinAltitude() : ?float
	{
		return $this->_minAltitude;
	}
	
	/**
	 * Sets the payment services provided by this Product (cost per day, per
	 * person, price...).
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurOfferInterface> $offers
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setOffers(array $offers) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_offers = $offers;
		
		return $this;
	}
	
	/**
	 * Gets the payment services provided by this Product (cost per day, per
	 * person, price...).
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurOfferInterface>
	 */
	public function getOffers() : array
	{
		return $this->_offers;
	}
	
	/**
	 * Sets the type of food provided.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurThemeInterface> $providesCuisineOfType
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setProvidesCuisineOfType(array $providesCuisineOfType) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_providesCuisineOfType = $providesCuisineOfType;
		
		return $this;
	}
	
	/**
	 * Gets the type of food provided.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	public function getProvidesCuisineOfType() : array
	{
		return $this->_providesCuisineOfType;
	}
	
	/**
	 * Sets the type of provided food.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurThemeInterface> $providesFoodProduct
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setProvidesFoodProduct(array $providesFoodProduct) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_providesFoodProduct = $providesFoodProduct;
		
		return $this;
	}
	
	/**
	 * Gets the type of provided food.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurThemeInterface>
	 */
	public function getProvidesFoodProduct() : array
	{
		return $this->_providesFoodProduct;
	}
	
	/**
	 * Sets gets whether the PointOfInterest is suitable for people with
	 * reduced mobility.
	 * 
	 * @param ?bool $reducedMobilityAccess
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setReducedMobilityAccess(?bool $reducedMobilityAccess) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_reducedMobilityAccess = $reducedMobilityAccess;
		
		return $this;
	}
	
	/**
	 * Gets gets whether the PointOfInterest is suitable for people with
	 * reduced mobility.
	 * 
	 * @return ?bool
	 */
	public function hasReducedMobilityAccess() : ?bool
	{
		return $this->_reducedMobilityAccess;
	}
	
	/**
	 * Sets true when the Tasting provides saling on site.
	 * 
	 * @param ?bool $saleOnSite
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setSaleOnSite(?bool $saleOnSite) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_saleOnSite = $saleOnSite;
		
		return $this;
	}
	
	/**
	 * Gets true when the Tasting provides saling on site.
	 * 
	 * @return ?bool
	 */
	public function hasSaleOnSite() : ?bool
	{
		return $this->_saleOnSite;
	}
	
	/**
	 * Sets true if food takeaway is allowed.
	 * 
	 * @param ?bool $takeAway
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setTakeAway(?bool $takeAway) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_takeAway = $takeAway;
		
		return $this;
	}
	
	/**
	 * Gets true if food takeaway is allowed.
	 * 
	 * @return ?bool
	 */
	public function hasTakeAway() : ?bool
	{
		return $this->_takeAway;
	}
	
	/**
	 * Sets (FMA) The period this event takes place at.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurPeriodInterface> $takesPlaceAt
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setTakesPlaceAt(array $takesPlaceAt) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_takesPlaceAt = $takesPlaceAt;
		
		return $this;
	}
	
	/**
	 * Gets (FMA) The period this event takes place at.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPeriodInterface>
	 */
	public function getTakesPlaceAt() : array
	{
		return $this->_takesPlaceAt;
	}
	
	/**
	 * Sets (TOUR) The distance between the start and the end of the stage or
	 * the path. Unit in meters.
	 * 
	 * @param ?float $tourDistance
	 * @return ApiFrDatatourismeDiffuseurPointOfInterestInterface
	 */
	public function setTourDistance(?float $tourDistance) : ApiFrDatatourismeDiffuseurPointOfInterestInterface
	{
		$this->_tourDistance = $tourDistance;
		
		return $this;
	}
	
	/**
	 * Gets (TOUR) The distance between the start and the end of the stage or
	 * the path. Unit in meters.
	 * 
	 * @return ?float
	 */
	public function getTourDistance() : ?float
	{
		return $this->_tourDistance;
	}
	
}
