<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurOpeningHours class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurOpeningHoursInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurOpeningHours implements ApiFrDatatourismeDiffuseurOpeningHoursInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The closing hour of the place or service on the given day(s) of the
	 * week.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_schemaCloses = null;
	
	/**
	 * The day of weeks when this opening hours definition apply.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurDayOfWeekInterface>
	 */
	protected array $_schemaDayOfWeek = [];
	
	/**
	 * The opening hour of the place or service on the given day(s) of the
	 * week.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_schemaOpens = null;
	
	/**
	 * The begin date.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_schemaValidFrom = null;
	
	/**
	 * The end date.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_schemaValidThrough = null;
	
	/**
	 * Further information.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $_additionalInformation = null;
	
	/**
	 * The translated fields.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	protected array $_hasTranslatedProperty = [];
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurOpeningHours with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurOpeningHoursInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurOpeningHoursInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurOpeningHoursInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurOpeningHoursInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the closing hour of the place or service on the given day(s) of the
	 * week.
	 * 
	 * @param ?DateTimeInterface $schemaCloses
	 * @return ApiFrDatatourismeDiffuseurOpeningHoursInterface
	 */
	public function setSchemaCloses(?DateTimeInterface $schemaCloses) : ApiFrDatatourismeDiffuseurOpeningHoursInterface
	{
		$this->_schemaCloses = $schemaCloses;
		
		return $this;
	}
	
	/**
	 * Gets the closing hour of the place or service on the given day(s) of the
	 * week.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getSchemaCloses() : ?DateTimeInterface
	{
		return $this->_schemaCloses;
	}
	
	/**
	 * Sets the day of weeks when this opening hours definition apply.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurDayOfWeekInterface> $schemaDayOfWeek
	 * @return ApiFrDatatourismeDiffuseurOpeningHoursInterface
	 */
	public function setSchemaDayOfWeek(array $schemaDayOfWeek) : ApiFrDatatourismeDiffuseurOpeningHoursInterface
	{
		$this->_schemaDayOfWeek = $schemaDayOfWeek;
		
		return $this;
	}
	
	/**
	 * Gets the day of weeks when this opening hours definition apply.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurDayOfWeekInterface>
	 */
	public function getSchemaDayOfWeek() : array
	{
		return $this->_schemaDayOfWeek;
	}
	
	/**
	 * Sets the opening hour of the place or service on the given day(s) of the
	 * week.
	 * 
	 * @param ?DateTimeInterface $schemaOpens
	 * @return ApiFrDatatourismeDiffuseurOpeningHoursInterface
	 */
	public function setSchemaOpens(?DateTimeInterface $schemaOpens) : ApiFrDatatourismeDiffuseurOpeningHoursInterface
	{
		$this->_schemaOpens = $schemaOpens;
		
		return $this;
	}
	
	/**
	 * Gets the opening hour of the place or service on the given day(s) of the
	 * week.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getSchemaOpens() : ?DateTimeInterface
	{
		return $this->_schemaOpens;
	}
	
	/**
	 * Sets the begin date.
	 * 
	 * @param ?DateTimeInterface $schemaValidFrom
	 * @return ApiFrDatatourismeDiffuseurOpeningHoursInterface
	 */
	public function setSchemaValidFrom(?DateTimeInterface $schemaValidFrom) : ApiFrDatatourismeDiffuseurOpeningHoursInterface
	{
		$this->_schemaValidFrom = $schemaValidFrom;
		
		return $this;
	}
	
	/**
	 * Gets the begin date.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getSchemaValidFrom() : ?DateTimeInterface
	{
		return $this->_schemaValidFrom;
	}
	
	/**
	 * Sets the end date.
	 * 
	 * @param ?DateTimeInterface $schemaValidThrough
	 * @return ApiFrDatatourismeDiffuseurOpeningHoursInterface
	 */
	public function setSchemaValidThrough(?DateTimeInterface $schemaValidThrough) : ApiFrDatatourismeDiffuseurOpeningHoursInterface
	{
		$this->_schemaValidThrough = $schemaValidThrough;
		
		return $this;
	}
	
	/**
	 * Gets the end date.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getSchemaValidThrough() : ?DateTimeInterface
	{
		return $this->_schemaValidThrough;
	}
	
	/**
	 * Sets further information.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurTranslatedTextInterface $additionalInformation
	 * @return ApiFrDatatourismeDiffuseurOpeningHoursInterface
	 */
	public function setAdditionalInformation(?ApiFrDatatourismeDiffuseurTranslatedTextInterface $additionalInformation) : ApiFrDatatourismeDiffuseurOpeningHoursInterface
	{
		$this->_additionalInformation = $additionalInformation;
		
		return $this;
	}
	
	/**
	 * Gets further information.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function getAdditionalInformation() : ?ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		return $this->_additionalInformation;
	}
	
	/**
	 * Sets the translated fields.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface> $hasTranslatedProperty
	 * @return ApiFrDatatourismeDiffuseurOpeningHoursInterface
	 */
	public function setHasTranslatedProperty(array $hasTranslatedProperty) : ApiFrDatatourismeDiffuseurOpeningHoursInterface
	{
		$this->_hasTranslatedProperty = $hasTranslatedProperty;
		
		return $this;
	}
	
	/**
	 * Gets the translated fields.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurTranslatedPropertyInterface>
	 */
	public function getHasTranslatedProperty() : array
	{
		return $this->_hasTranslatedProperty;
	}
	
}
