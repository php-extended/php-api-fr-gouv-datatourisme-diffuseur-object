<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

/**
 * ApiFrDatatourismeDiffuseurTranslatedText class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurTranslatedTextInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurTranslatedText implements ApiFrDatatourismeDiffuseurTranslatedTextInterface
{
	
	/**
	 * The german translation.
	 * 
	 * @var ?string
	 */
	protected ?string $_de = null;
	
	/**
	 * The english translation.
	 * 
	 * @var ?string
	 */
	protected ?string $_en = null;
	
	/**
	 * The spanish translation.
	 * 
	 * @var ?string
	 */
	protected ?string $_es = null;
	
	/**
	 * The french translation.
	 * 
	 * @var ?string
	 */
	protected ?string $_fr = null;
	
	/**
	 * The italian translation.
	 * 
	 * @var ?string
	 */
	protected ?string $_it = null;
	
	/**
	 * The dutch translation.
	 * 
	 * @var ?string
	 */
	protected ?string $_nl = null;
	
	/**
	 * The portuguese translation.
	 * 
	 * @var ?string
	 */
	protected ?string $_pt = null;
	
	/**
	 * The russian translation.
	 * 
	 * @var ?string
	 */
	protected ?string $_ru = null;
	
	/**
	 * The chinese translation.
	 * 
	 * @var ?string
	 */
	protected ?string $_zh = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the german translation.
	 * 
	 * @param ?string $de
	 * @return ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function setDe(?string $de) : ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		$this->_de = $de;
		
		return $this;
	}
	
	/**
	 * Gets the german translation.
	 * 
	 * @return ?string
	 */
	public function getDe() : ?string
	{
		return $this->_de;
	}
	
	/**
	 * Sets the english translation.
	 * 
	 * @param ?string $en
	 * @return ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function setEn(?string $en) : ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		$this->_en = $en;
		
		return $this;
	}
	
	/**
	 * Gets the english translation.
	 * 
	 * @return ?string
	 */
	public function getEn() : ?string
	{
		return $this->_en;
	}
	
	/**
	 * Sets the spanish translation.
	 * 
	 * @param ?string $es
	 * @return ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function setEs(?string $es) : ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		$this->_es = $es;
		
		return $this;
	}
	
	/**
	 * Gets the spanish translation.
	 * 
	 * @return ?string
	 */
	public function getEs() : ?string
	{
		return $this->_es;
	}
	
	/**
	 * Sets the french translation.
	 * 
	 * @param ?string $fr
	 * @return ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function setFr(?string $fr) : ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		$this->_fr = $fr;
		
		return $this;
	}
	
	/**
	 * Gets the french translation.
	 * 
	 * @return ?string
	 */
	public function getFr() : ?string
	{
		return $this->_fr;
	}
	
	/**
	 * Sets the italian translation.
	 * 
	 * @param ?string $it
	 * @return ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function setIt(?string $it) : ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		$this->_it = $it;
		
		return $this;
	}
	
	/**
	 * Gets the italian translation.
	 * 
	 * @return ?string
	 */
	public function getIt() : ?string
	{
		return $this->_it;
	}
	
	/**
	 * Sets the dutch translation.
	 * 
	 * @param ?string $nl
	 * @return ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function setNl(?string $nl) : ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		$this->_nl = $nl;
		
		return $this;
	}
	
	/**
	 * Gets the dutch translation.
	 * 
	 * @return ?string
	 */
	public function getNl() : ?string
	{
		return $this->_nl;
	}
	
	/**
	 * Sets the portuguese translation.
	 * 
	 * @param ?string $pt
	 * @return ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function setPt(?string $pt) : ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		$this->_pt = $pt;
		
		return $this;
	}
	
	/**
	 * Gets the portuguese translation.
	 * 
	 * @return ?string
	 */
	public function getPt() : ?string
	{
		return $this->_pt;
	}
	
	/**
	 * Sets the russian translation.
	 * 
	 * @param ?string $ru
	 * @return ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function setRu(?string $ru) : ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		$this->_ru = $ru;
		
		return $this;
	}
	
	/**
	 * Gets the russian translation.
	 * 
	 * @return ?string
	 */
	public function getRu() : ?string
	{
		return $this->_ru;
	}
	
	/**
	 * Sets the chinese translation.
	 * 
	 * @param ?string $zh
	 * @return ApiFrDatatourismeDiffuseurTranslatedTextInterface
	 */
	public function setZh(?string $zh) : ApiFrDatatourismeDiffuseurTranslatedTextInterface
	{
		$this->_zh = $zh;
		
		return $this;
	}
	
	/**
	 * Gets the chinese translation.
	 * 
	 * @return ?string
	 */
	public function getZh() : ?string
	{
		return $this->_zh;
	}
	
}
