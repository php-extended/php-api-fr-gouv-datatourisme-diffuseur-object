<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurOffer class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurOfferInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurOffer implements ApiFrDatatourismeDiffuseurOfferInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The accepted payment methods.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurPaymentMethodInterface>
	 */
	protected array $_schemaAcceptedPaymentMethod = [];
	
	/**
	 * The price specifications.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurPriceSpecificationInterface>
	 */
	protected array $_schemaPriceSpecification = [];
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurOffer with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurOfferInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurOfferInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurOfferInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurOfferInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the accepted payment methods.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurPaymentMethodInterface> $schemaAcceptedPaymentMethod
	 * @return ApiFrDatatourismeDiffuseurOfferInterface
	 */
	public function setSchemaAcceptedPaymentMethod(array $schemaAcceptedPaymentMethod) : ApiFrDatatourismeDiffuseurOfferInterface
	{
		$this->_schemaAcceptedPaymentMethod = $schemaAcceptedPaymentMethod;
		
		return $this;
	}
	
	/**
	 * Gets the accepted payment methods.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPaymentMethodInterface>
	 */
	public function getSchemaAcceptedPaymentMethod() : array
	{
		return $this->_schemaAcceptedPaymentMethod;
	}
	
	/**
	 * Sets the price specifications.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurPriceSpecificationInterface> $schemaPriceSpecification
	 * @return ApiFrDatatourismeDiffuseurOfferInterface
	 */
	public function setSchemaPriceSpecification(array $schemaPriceSpecification) : ApiFrDatatourismeDiffuseurOfferInterface
	{
		$this->_schemaPriceSpecification = $schemaPriceSpecification;
		
		return $this;
	}
	
	/**
	 * Gets the price specifications.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurPriceSpecificationInterface>
	 */
	public function getSchemaPriceSpecification() : array
	{
		return $this->_schemaPriceSpecification;
	}
	
}
