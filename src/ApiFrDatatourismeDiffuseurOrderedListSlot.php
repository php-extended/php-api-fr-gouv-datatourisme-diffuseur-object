<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurOrderedListSlot class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurOrderedListSlotInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurOrderedListSlot implements ApiFrDatatourismeDiffuseurOrderedListSlotInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The index of this slot.
	 * 
	 * @var ?int
	 */
	protected ?int $_oloIndex = null;
	
	/**
	 * This class represents an item in a list.
	 * 
	 * @var ?ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	protected ?ApiFrDatatourismeDiffuseurOrderedListItemInterface $_oloItem = null;
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurOrderedListSlot with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurOrderedListSlotInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurOrderedListSlotInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurOrderedListSlotInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurOrderedListSlotInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the index of this slot.
	 * 
	 * @param ?int $oloIndex
	 * @return ApiFrDatatourismeDiffuseurOrderedListSlotInterface
	 */
	public function setOloIndex(?int $oloIndex) : ApiFrDatatourismeDiffuseurOrderedListSlotInterface
	{
		$this->_oloIndex = $oloIndex;
		
		return $this;
	}
	
	/**
	 * Gets the index of this slot.
	 * 
	 * @return ?int
	 */
	public function getOloIndex() : ?int
	{
		return $this->_oloIndex;
	}
	
	/**
	 * Sets this class represents an item in a list.
	 * 
	 * @param ?ApiFrDatatourismeDiffuseurOrderedListItemInterface $oloItem
	 * @return ApiFrDatatourismeDiffuseurOrderedListSlotInterface
	 */
	public function setOloItem(?ApiFrDatatourismeDiffuseurOrderedListItemInterface $oloItem) : ApiFrDatatourismeDiffuseurOrderedListSlotInterface
	{
		$this->_oloItem = $oloItem;
		
		return $this;
	}
	
	/**
	 * Gets this class represents an item in a list.
	 * 
	 * @return ?ApiFrDatatourismeDiffuseurOrderedListItemInterface
	 */
	public function getOloItem() : ?ApiFrDatatourismeDiffuseurOrderedListItemInterface
	{
		return $this->_oloItem;
	}
	
}
