<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

/**
 * ApiFrDatatourismeDiffuseurTranslatedProperty class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurTranslatedPropertyInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurTranslatedProperty implements ApiFrDatatourismeDiffuseurTranslatedPropertyInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The contributors.
	 * 
	 * @var array<int, string>
	 */
	protected array $_dcContributor = [];
	
	/**
	 * The languages.
	 * 
	 * @var array<int, string>
	 */
	protected array $_rdfLanguage = [];
	
	/**
	 * The properties.
	 * 
	 * @var array<int, string>
	 */
	protected array $_rdfPredicate = [];
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurTranslatedProperty with private members.
	 * 
	 * @param string $id
	 */
	public function __construct(string $id)
	{
		$this->setId($id);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param string $id
	 * @return ApiFrDatatourismeDiffuseurTranslatedPropertyInterface
	 */
	public function setId(string $id) : ApiFrDatatourismeDiffuseurTranslatedPropertyInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the contributors.
	 * 
	 * @param array<int, string> $dcContributor
	 * @return ApiFrDatatourismeDiffuseurTranslatedPropertyInterface
	 */
	public function setDcContributor(array $dcContributor) : ApiFrDatatourismeDiffuseurTranslatedPropertyInterface
	{
		$this->_dcContributor = $dcContributor;
		
		return $this;
	}
	
	/**
	 * Gets the contributors.
	 * 
	 * @return array<int, string>
	 */
	public function getDcContributor() : array
	{
		return $this->_dcContributor;
	}
	
	/**
	 * Sets the languages.
	 * 
	 * @param array<int, string> $rdfLanguage
	 * @return ApiFrDatatourismeDiffuseurTranslatedPropertyInterface
	 */
	public function setRdfLanguage(array $rdfLanguage) : ApiFrDatatourismeDiffuseurTranslatedPropertyInterface
	{
		$this->_rdfLanguage = $rdfLanguage;
		
		return $this;
	}
	
	/**
	 * Gets the languages.
	 * 
	 * @return array<int, string>
	 */
	public function getRdfLanguage() : array
	{
		return $this->_rdfLanguage;
	}
	
	/**
	 * Sets the properties.
	 * 
	 * @param array<int, string> $rdfPredicate
	 * @return ApiFrDatatourismeDiffuseurTranslatedPropertyInterface
	 */
	public function setRdfPredicate(array $rdfPredicate) : ApiFrDatatourismeDiffuseurTranslatedPropertyInterface
	{
		$this->_rdfPredicate = $rdfPredicate;
		
		return $this;
	}
	
	/**
	 * Gets the properties.
	 * 
	 * @return array<int, string>
	 */
	public function getRdfPredicate() : array
	{
		return $this->_rdfPredicate;
	}
	
}
