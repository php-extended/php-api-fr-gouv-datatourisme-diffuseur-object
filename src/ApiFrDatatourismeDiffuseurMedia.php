<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur;

use Psr\Http\Message\UriInterface;

/**
 * ApiFrDatatourismeDiffuseurMedia class file.
 * 
 * This is a simple implementation of the
 * ApiFrDatatourismeDiffuseurMediaInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrDatatourismeDiffuseurMedia implements ApiFrDatatourismeDiffuseurMediaInterface
{
	
	/**
	 * The id of the object.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_id;
	
	/**
	 * The type of object.
	 * 
	 * @var array<int, string>
	 */
	protected array $_type = [];
	
	/**
	 * The annotations.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurAnnotationInterface>
	 */
	protected array $_ebucoreHasAnnotation = [];
	
	/**
	 * The resources.
	 * 
	 * @var array<int, ApiFrDatatourismeDiffuseurResourceInterface>
	 */
	protected array $_ebucoreHasRelatedResource = [];
	
	/**
	 * Constructor for ApiFrDatatourismeDiffuseurMedia with private members.
	 * 
	 * @param UriInterface $id
	 * @param array<int, string> $type
	 */
	public function __construct(UriInterface $id, array $type)
	{
		$this->setId($id);
		$this->setType($type);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the object.
	 * 
	 * @param UriInterface $id
	 * @return ApiFrDatatourismeDiffuseurMediaInterface
	 */
	public function setId(UriInterface $id) : ApiFrDatatourismeDiffuseurMediaInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of the object.
	 * 
	 * @return UriInterface
	 */
	public function getId() : UriInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the type of object.
	 * 
	 * @param array<int, string> $type
	 * @return ApiFrDatatourismeDiffuseurMediaInterface
	 */
	public function setType(array $type) : ApiFrDatatourismeDiffuseurMediaInterface
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * Gets the type of object.
	 * 
	 * @return array<int, string>
	 */
	public function getType() : array
	{
		return $this->_type;
	}
	
	/**
	 * Sets the annotations.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurAnnotationInterface> $ebucoreHasAnnotation
	 * @return ApiFrDatatourismeDiffuseurMediaInterface
	 */
	public function setEbucoreHasAnnotation(array $ebucoreHasAnnotation) : ApiFrDatatourismeDiffuseurMediaInterface
	{
		$this->_ebucoreHasAnnotation = $ebucoreHasAnnotation;
		
		return $this;
	}
	
	/**
	 * Gets the annotations.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurAnnotationInterface>
	 */
	public function getEbucoreHasAnnotation() : array
	{
		return $this->_ebucoreHasAnnotation;
	}
	
	/**
	 * Sets the resources.
	 * 
	 * @param array<int, ApiFrDatatourismeDiffuseurResourceInterface> $ebucoreHasRelatedResource
	 * @return ApiFrDatatourismeDiffuseurMediaInterface
	 */
	public function setEbucoreHasRelatedResource(array $ebucoreHasRelatedResource) : ApiFrDatatourismeDiffuseurMediaInterface
	{
		$this->_ebucoreHasRelatedResource = $ebucoreHasRelatedResource;
		
		return $this;
	}
	
	/**
	 * Gets the resources.
	 * 
	 * @return array<int, ApiFrDatatourismeDiffuseurResourceInterface>
	 */
	public function getEbucoreHasRelatedResource() : array
	{
		return $this->_ebucoreHasRelatedResource;
	}
	
}
