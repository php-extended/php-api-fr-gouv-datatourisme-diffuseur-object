<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurDayOfWeek;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPeriod;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedProperty;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedText;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurPeriodTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPeriod
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurPeriodTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurPeriod
	 */
	protected ApiFrDatatourismeDiffuseurPeriod $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetAppliesOnDay() : void
	{
		$this->assertEquals([], $this->_object->getAppliesOnDay());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurDayOfWeek::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurDayOfWeek::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setAppliesOnDay($expected);
		$this->assertEquals($expected, $this->_object->getAppliesOnDay());
	}
	
	public function testGetEndDate() : void
	{
		$this->assertNull($this->_object->getEndDate());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setEndDate($expected);
		$this->assertEquals($expected, $this->_object->getEndDate());
	}
	
	public function testGetEndTime() : void
	{
		$this->assertNull($this->_object->getEndTime());
		$expected = DateTimeImmutable::createFromFormat('!H:i:s', '01:01:01');
		$this->_object->setEndTime($expected);
		$this->assertEquals($expected, $this->_object->getEndTime());
	}
	
	public function testGetHasTranslatedProperty() : void
	{
		$this->assertEquals([], $this->_object->getHasTranslatedProperty());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasTranslatedProperty($expected);
		$this->assertEquals($expected, $this->_object->getHasTranslatedProperty());
	}
	
	public function testGetOpeningDetails() : void
	{
		$this->assertNull($this->_object->getOpeningDetails());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setOpeningDetails($expected);
		$this->assertEquals($expected, $this->_object->getOpeningDetails());
	}
	
	public function testGetStartDate() : void
	{
		$this->assertNull($this->_object->getStartDate());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setStartDate($expected);
		$this->assertEquals($expected, $this->_object->getStartDate());
	}
	
	public function testGetStartTime() : void
	{
		$this->assertNull($this->_object->getStartTime());
		$expected = DateTimeImmutable::createFromFormat('!H:i:s', '01:01:01');
		$this->_object->setStartTime($expected);
		$this->assertEquals($expected, $this->_object->getStartTime());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurPeriod((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
