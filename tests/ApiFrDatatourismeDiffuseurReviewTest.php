<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurReview;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurReviewValue;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurReviewTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurReview
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurReviewTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurReview
	 */
	protected ApiFrDatatourismeDiffuseurReview $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetSchemaDatePublished() : void
	{
		$this->assertEquals([], $this->_object->getSchemaDatePublished());
		$expected = [DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01'), DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01')];
		$this->_object->setSchemaDatePublished($expected);
		$this->assertEquals($expected, $this->_object->getSchemaDatePublished());
	}
	
	public function testGetHasReviewValue() : void
	{
		$this->assertNull($this->_object->getHasReviewValue());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurReviewValue::class)->disableOriginalConstructor()->getMock();
		$this->_object->setHasReviewValue($expected);
		$this->assertEquals($expected, $this->_object->getHasReviewValue());
	}
	
	public function testHasPending() : void
	{
		$this->assertNull($this->_object->hasPending());
		$expected = true;
		$this->_object->setPending($expected);
		$this->assertTrue($this->_object->hasPending());
	}
	
	public function testGetReviewDeliveryDate() : void
	{
		$this->assertNull($this->_object->getReviewDeliveryDate());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setReviewDeliveryDate($expected);
		$this->assertEquals($expected, $this->_object->getReviewDeliveryDate());
	}
	
	public function testGetReviewExpirationDate() : void
	{
		$this->assertNull($this->_object->getReviewExpirationDate());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setReviewExpirationDate($expected);
		$this->assertEquals($expected, $this->_object->getReviewExpirationDate());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurReview((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
