<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedProperty;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurTranslatedPropertyTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedProperty
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurTranslatedPropertyTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurTranslatedProperty
	 */
	protected ApiFrDatatourismeDiffuseurTranslatedProperty $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getId());
		$expected = 'qsdfghjklm';
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetDcContributor() : void
	{
		$this->assertEquals([], $this->_object->getDcContributor());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setDcContributor($expected);
		$this->assertEquals($expected, $this->_object->getDcContributor());
	}
	
	public function testGetRdfLanguage() : void
	{
		$this->assertEquals([], $this->_object->getRdfLanguage());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setRdfLanguage($expected);
		$this->assertEquals($expected, $this->_object->getRdfLanguage());
	}
	
	public function testGetRdfPredicate() : void
	{
		$this->assertEquals([], $this->_object->getRdfPredicate());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setRdfPredicate($expected);
		$this->assertEquals($expected, $this->_object->getRdfPredicate());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurTranslatedProperty('azertyuiop');
	}
	
}
