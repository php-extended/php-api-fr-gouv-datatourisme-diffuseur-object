<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAddress;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurCity;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurAddressTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAddress
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurAddressTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurAddress
	 */
	protected ApiFrDatatourismeDiffuseurAddress $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetSchemaAddressLocality() : void
	{
		$this->assertNull($this->_object->getSchemaAddressLocality());
		$expected = 'qsdfghjklm';
		$this->_object->setSchemaAddressLocality($expected);
		$this->assertEquals($expected, $this->_object->getSchemaAddressLocality());
	}
	
	public function testGetSchemaPostOfficeBoxNumber() : void
	{
		$this->assertNull($this->_object->getSchemaPostOfficeBoxNumber());
		$expected = 'qsdfghjklm';
		$this->_object->setSchemaPostOfficeBoxNumber($expected);
		$this->assertEquals($expected, $this->_object->getSchemaPostOfficeBoxNumber());
	}
	
	public function testGetSchemaPostalCode() : void
	{
		$this->assertNull($this->_object->getSchemaPostalCode());
		$expected = 'qsdfghjklm';
		$this->_object->setSchemaPostalCode($expected);
		$this->assertEquals($expected, $this->_object->getSchemaPostalCode());
	}
	
	public function testGetSchemaStreetAddress() : void
	{
		$this->assertEquals([], $this->_object->getSchemaStreetAddress());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setSchemaStreetAddress($expected);
		$this->assertEquals($expected, $this->_object->getSchemaStreetAddress());
	}
	
	public function testGetCedex() : void
	{
		$this->assertNull($this->_object->getCedex());
		$expected = 'qsdfghjklm';
		$this->_object->setCedex($expected);
		$this->assertEquals($expected, $this->_object->getCedex());
	}
	
	public function testGetHasAddressCity() : void
	{
		$this->assertNull($this->_object->getHasAddressCity());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurCity::class)->disableOriginalConstructor()->getMock();
		$this->_object->setHasAddressCity($expected);
		$this->assertEquals($expected, $this->_object->getHasAddressCity());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurAddress((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
