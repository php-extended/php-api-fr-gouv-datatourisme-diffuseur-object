<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedText;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurTranslatedTextTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedText
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurTranslatedTextTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurTranslatedText
	 */
	protected ApiFrDatatourismeDiffuseurTranslatedText $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetDe() : void
	{
		$this->assertNull($this->_object->getDe());
		$expected = 'qsdfghjklm';
		$this->_object->setDe($expected);
		$this->assertEquals($expected, $this->_object->getDe());
	}
	
	public function testGetEn() : void
	{
		$this->assertNull($this->_object->getEn());
		$expected = 'qsdfghjklm';
		$this->_object->setEn($expected);
		$this->assertEquals($expected, $this->_object->getEn());
	}
	
	public function testGetEs() : void
	{
		$this->assertNull($this->_object->getEs());
		$expected = 'qsdfghjklm';
		$this->_object->setEs($expected);
		$this->assertEquals($expected, $this->_object->getEs());
	}
	
	public function testGetFr() : void
	{
		$this->assertNull($this->_object->getFr());
		$expected = 'qsdfghjklm';
		$this->_object->setFr($expected);
		$this->assertEquals($expected, $this->_object->getFr());
	}
	
	public function testGetIt() : void
	{
		$this->assertNull($this->_object->getIt());
		$expected = 'qsdfghjklm';
		$this->_object->setIt($expected);
		$this->assertEquals($expected, $this->_object->getIt());
	}
	
	public function testGetNl() : void
	{
		$this->assertNull($this->_object->getNl());
		$expected = 'qsdfghjklm';
		$this->_object->setNl($expected);
		$this->assertEquals($expected, $this->_object->getNl());
	}
	
	public function testGetPt() : void
	{
		$this->assertNull($this->_object->getPt());
		$expected = 'qsdfghjklm';
		$this->_object->setPt($expected);
		$this->assertEquals($expected, $this->_object->getPt());
	}
	
	public function testGetRu() : void
	{
		$this->assertNull($this->_object->getRu());
		$expected = 'qsdfghjklm';
		$this->_object->setRu($expected);
		$this->assertEquals($expected, $this->_object->getRu());
	}
	
	public function testGetZh() : void
	{
		$this->assertNull($this->_object->getZh());
		$expected = 'qsdfghjklm';
		$this->_object->setZh($expected);
		$this->assertEquals($expected, $this->_object->getZh());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurTranslatedText();
	}
	
}
