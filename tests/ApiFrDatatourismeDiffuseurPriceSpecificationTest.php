<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAudience;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPeriod;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPriceSpecification;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPricingMode;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPricingOffer;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPricingPolicy;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPricingSeason;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedProperty;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedText;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurPriceSpecificationTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPriceSpecification
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurPriceSpecificationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurPriceSpecification
	 */
	protected ApiFrDatatourismeDiffuseurPriceSpecification $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetSchemaEligibleQuantity() : void
	{
		$this->assertNull($this->_object->getSchemaEligibleQuantity());
		$expected = 'qsdfghjklm';
		$this->_object->setSchemaEligibleQuantity($expected);
		$this->assertEquals($expected, $this->_object->getSchemaEligibleQuantity());
	}
	
	public function testGetSchemaMaxPrice() : void
	{
		$this->assertEquals([], $this->_object->getSchemaMaxPrice());
		$expected = [15.2, 15.2];
		$this->_object->setSchemaMaxPrice($expected);
		$this->assertEquals($expected, $this->_object->getSchemaMaxPrice());
	}
	
	public function testGetSchemaMinPrice() : void
	{
		$this->assertEquals([], $this->_object->getSchemaMinPrice());
		$expected = [15.2, 15.2];
		$this->_object->setSchemaMinPrice($expected);
		$this->assertEquals($expected, $this->_object->getSchemaMinPrice());
	}
	
	public function testGetSchemaPrice() : void
	{
		$this->assertNull($this->_object->getSchemaPrice());
		$expected = 15.2;
		$this->_object->setSchemaPrice($expected);
		$this->assertEquals($expected, $this->_object->getSchemaPrice());
	}
	
	public function testGetSchemaPriceCurrency() : void
	{
		$this->assertNull($this->_object->getSchemaPriceCurrency());
		$expected = 'qsdfghjklm';
		$this->_object->setSchemaPriceCurrency($expected);
		$this->assertEquals($expected, $this->_object->getSchemaPriceCurrency());
	}
	
	public function testGetAdditionalInformation() : void
	{
		$this->assertNull($this->_object->getAdditionalInformation());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setAdditionalInformation($expected);
		$this->assertEquals($expected, $this->_object->getAdditionalInformation());
	}
	
	public function testGetHasAudience() : void
	{
		$this->assertEquals([], $this->_object->getHasAudience());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAudience::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAudience::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasAudience($expected);
		$this->assertEquals($expected, $this->_object->getHasAudience());
	}
	
	public function testGetHasEligibleAudience() : void
	{
		$this->assertEquals([], $this->_object->getHasEligibleAudience());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAudience::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAudience::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasEligibleAudience($expected);
		$this->assertEquals($expected, $this->_object->getHasEligibleAudience());
	}
	
	public function testGetHasEligiblePolicy() : void
	{
		$this->assertNull($this->_object->getHasEligiblePolicy());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurPricingPolicy::class)->disableOriginalConstructor()->getMock();
		$this->_object->setHasEligiblePolicy($expected);
		$this->assertEquals($expected, $this->_object->getHasEligiblePolicy());
	}
	
	public function testGetHasPricingMode() : void
	{
		$this->assertEquals([], $this->_object->getHasPricingMode());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurPricingMode::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurPricingMode::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasPricingMode($expected);
		$this->assertEquals($expected, $this->_object->getHasPricingMode());
	}
	
	public function testGetHasPricingOffer() : void
	{
		$this->assertEquals([], $this->_object->getHasPricingOffer());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurPricingOffer::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurPricingOffer::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasPricingOffer($expected);
		$this->assertEquals($expected, $this->_object->getHasPricingOffer());
	}
	
	public function testGetHasPricingSeason() : void
	{
		$this->assertEquals([], $this->_object->getHasPricingSeason());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurPricingSeason::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurPricingSeason::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasPricingSeason($expected);
		$this->assertEquals($expected, $this->_object->getHasPricingSeason());
	}
	
	public function testGetHasTranslatedProperty() : void
	{
		$this->assertEquals([], $this->_object->getHasTranslatedProperty());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasTranslatedProperty($expected);
		$this->assertEquals($expected, $this->_object->getHasTranslatedProperty());
	}
	
	public function testGetAppliesOnPeriod() : void
	{
		$this->assertEquals([], $this->_object->getAppliesOnPeriod());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurPeriod::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurPeriod::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setAppliesOnPeriod($expected);
		$this->assertEquals($expected, $this->_object->getAppliesOnPeriod());
	}
	
	public function testGetName() : void
	{
		$this->assertNull($this->_object->getName());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setName($expected);
		$this->assertEquals($expected, $this->_object->getName());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurPriceSpecification((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
