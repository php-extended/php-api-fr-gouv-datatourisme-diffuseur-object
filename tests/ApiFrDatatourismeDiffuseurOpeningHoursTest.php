<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurDayOfWeek;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOpeningHours;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedProperty;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedText;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurOpeningHoursTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOpeningHours
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurOpeningHoursTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurOpeningHours
	 */
	protected ApiFrDatatourismeDiffuseurOpeningHours $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetSchemaCloses() : void
	{
		$this->assertNull($this->_object->getSchemaCloses());
		$expected = DateTimeImmutable::createFromFormat('!H:i:s', '01:01:01');
		$this->_object->setSchemaCloses($expected);
		$this->assertEquals($expected, $this->_object->getSchemaCloses());
	}
	
	public function testGetSchemaDayOfWeek() : void
	{
		$this->assertEquals([], $this->_object->getSchemaDayOfWeek());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurDayOfWeek::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurDayOfWeek::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setSchemaDayOfWeek($expected);
		$this->assertEquals($expected, $this->_object->getSchemaDayOfWeek());
	}
	
	public function testGetSchemaOpens() : void
	{
		$this->assertNull($this->_object->getSchemaOpens());
		$expected = DateTimeImmutable::createFromFormat('!H:i:s', '01:01:01');
		$this->_object->setSchemaOpens($expected);
		$this->assertEquals($expected, $this->_object->getSchemaOpens());
	}
	
	public function testGetSchemaValidFrom() : void
	{
		$this->assertNull($this->_object->getSchemaValidFrom());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setSchemaValidFrom($expected);
		$this->assertEquals($expected, $this->_object->getSchemaValidFrom());
	}
	
	public function testGetSchemaValidThrough() : void
	{
		$this->assertNull($this->_object->getSchemaValidThrough());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setSchemaValidThrough($expected);
		$this->assertEquals($expected, $this->_object->getSchemaValidThrough());
	}
	
	public function testGetAdditionalInformation() : void
	{
		$this->assertNull($this->_object->getAdditionalInformation());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setAdditionalInformation($expected);
		$this->assertEquals($expected, $this->_object->getAdditionalInformation());
	}
	
	public function testGetHasTranslatedProperty() : void
	{
		$this->assertEquals([], $this->_object->getHasTranslatedProperty());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasTranslatedProperty($expected);
		$this->assertEquals($expected, $this->_object->getHasTranslatedProperty());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurOpeningHours((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
