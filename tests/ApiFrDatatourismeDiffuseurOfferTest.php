<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOffer;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPaymentMethod;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPriceSpecification;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurOfferTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOffer
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurOfferTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurOffer
	 */
	protected ApiFrDatatourismeDiffuseurOffer $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetSchemaAcceptedPaymentMethod() : void
	{
		$this->assertEquals([], $this->_object->getSchemaAcceptedPaymentMethod());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurPaymentMethod::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurPaymentMethod::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setSchemaAcceptedPaymentMethod($expected);
		$this->assertEquals($expected, $this->_object->getSchemaAcceptedPaymentMethod());
	}
	
	public function testGetSchemaPriceSpecification() : void
	{
		$this->assertEquals([], $this->_object->getSchemaPriceSpecification());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurPriceSpecification::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurPriceSpecification::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setSchemaPriceSpecification($expected);
		$this->assertEquals($expected, $this->_object->getSchemaPriceSpecification());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurOffer((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
