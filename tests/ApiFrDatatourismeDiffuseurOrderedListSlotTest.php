<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOrderedListItem;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOrderedListSlot;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurOrderedListSlotTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOrderedListSlot
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurOrderedListSlotTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurOrderedListSlot
	 */
	protected ApiFrDatatourismeDiffuseurOrderedListSlot $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetOloIndex() : void
	{
		$this->assertNull($this->_object->getOloIndex());
		$expected = 25;
		$this->_object->setOloIndex($expected);
		$this->assertEquals($expected, $this->_object->getOloIndex());
	}
	
	public function testGetOloItem() : void
	{
		$this->assertNull($this->_object->getOloItem());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurOrderedListItem::class)->disableOriginalConstructor()->getMock();
		$this->_object->setOloItem($expected);
		$this->assertEquals($expected, $this->_object->getOloItem());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurOrderedListSlot((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
