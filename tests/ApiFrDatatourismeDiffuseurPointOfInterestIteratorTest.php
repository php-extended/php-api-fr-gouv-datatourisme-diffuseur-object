<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurEndpoint;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPointOfInterest;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPointOfInterestIterator;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPointOfInterestResume;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\Uri;
use PhpExtended\Reifier\Reifier;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiFrDatatourismeDiffuseurPointOfInterestIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPointOfInterestIterator
 *
 * @internal
 *
 * @small
 */
class ApiFrDatatourismeDiffuseurPointOfInterestIteratorTest extends TestCase
{
	
	/**
	 * Gets a suitable directory to download and uncompress data.
	 *
	 * @return string
	 * @throws RuntimeException
	 */
	protected static function getTempPath() : string
	{
		$base = \is_dir('/media/anastaszor/RUNTIME/') ? '/media/anastaszor/RUNTIME/' : '/tmp/';
		$real = $base.'php-extended__php-api-fr-gouv-datatourisme-diffuseur-object';
		if(!\is_dir($real))
		{
			if(!\mkdir($real))
			{
				throw new RuntimeException('Failed to make temp directory at '.$real);
			}
		}
		
		return $real;
	}
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurPointOfInterestIterator
	 */
	protected ApiFrDatatourismeDiffuseurPointOfInterestIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testIteration() : void
	{
		$k = 0;
		
		foreach($this->_object as $datum)
		{
			$this->assertInstanceOf(ApiFrDatatourismeDiffuseurPointOfInterestResume::class, $datum);
			$k++;
		}
		
		$this->assertEquals(1, $k);
	}
	public function testData() : void
	{
		$failed = 0;
		
		/** @var ApiFrDatatourismeDiffuseurPointOfInterestResume $resume */
		foreach($this->_object as $resume)
		{
			try
			{
				$reified = $this->_object->getPointOfInterestData($resume);
				$this->assertInstanceOf(ApiFrDatatourismeDiffuseurPointOfInterest::class, $reified);
			}
			catch(Exception $exc)
			{
				echo $exc->getMessage()."\n";
				$failed++;
				$messages = [];
				$prev = $exc;
				
				while(null !== $prev)
				{
					$messages[] = $prev->getMessage();
					$messages = \array_merge($messages, \explode("\n", $prev->getTraceAsString()));
					$prev = $prev->getPrevious();
				}
				
				@\file_put_contents($this->getTempPath().'/'.\str_replace('/', '_', $resume->getFile()).'.exception.txt', \json_encode($messages, \JSON_PRETTY_PRINT));
			}
		}
		
		$this->assertEquals(0, $failed);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		new ApiFrDatatourismeDiffuseurEndpoint(
			__DIR__,
			new class() implements ClientInterface
			{
				
				public function sendRequest(RequestInterface $request) : ResponseInterface
				{
					return new Response();
				}
			},
			null,
			null,
			null,
			$reifier = new Reifier(),
		);
		
		$resume = new ApiFrDatatourismeDiffuseurPointOfInterestResume(DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'), 'test.json');
		$resume->setId(new Uri());
		$resume->setLabel('TEST ETABLISSEMENT');
		
		$this->_object = new ApiFrDatatourismeDiffuseurPointOfInterestIterator(
			__DIR__,
			$reifier,
			$reifier->getConfiguration(),
			new ArrayIterator([$resume]),
		);
	}
	
}
