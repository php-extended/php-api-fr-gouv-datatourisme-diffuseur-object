<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPointOfInterestResume;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurPointOfInterestResumeTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPointOfInterestResume
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurPointOfInterestResumeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurPointOfInterestResume
	 */
	protected ApiFrDatatourismeDiffuseurPointOfInterestResume $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertNull($this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetLabel() : void
	{
		$this->assertNull($this->_object->getLabel());
		$expected = 'qsdfghjklm';
		$this->_object->setLabel($expected);
		$this->assertEquals($expected, $this->_object->getLabel());
	}
	
	public function testGetLastUpdateDatatourisme() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), $this->_object->getLastUpdateDatatourisme());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setLastUpdateDatatourisme($expected);
		$this->assertEquals($expected, $this->_object->getLastUpdateDatatourisme());
	}
	
	public function testGetFile() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFile());
		$expected = 'qsdfghjklm';
		$this->_object->setFile($expected);
		$this->assertEquals($expected, $this->_object->getFile());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurPointOfInterestResume(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), 'azertyuiop');
	}
	
}
