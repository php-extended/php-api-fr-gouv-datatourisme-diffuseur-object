<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurMimeType;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurResource;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurResourceTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurResource
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurResourceTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurResource
	 */
	protected ApiFrDatatourismeDiffuseurResource $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetEbucoreFileSize() : void
	{
		$this->assertEquals([], $this->_object->getEbucoreFileSize());
		$expected = [15.2, 15.2];
		$this->_object->setEbucoreFileSize($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreFileSize());
	}
	
	public function testGetEbucoreHasMimeType() : void
	{
		$this->assertEquals([], $this->_object->getEbucoreHasMimeType());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurMimeType::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurMimeType::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setEbucoreHasMimeType($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreHasMimeType());
	}
	
	public function testGetEbucoreHeight() : void
	{
		$this->assertNull($this->_object->getEbucoreHeight());
		$expected = 15.2;
		$this->_object->setEbucoreHeight($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreHeight());
	}
	
	public function testGetEbucoreHeightUnit() : void
	{
		$this->assertNull($this->_object->getEbucoreHeightUnit());
		$expected = 'qsdfghjklm';
		$this->_object->setEbucoreHeightUnit($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreHeightUnit());
	}
	
	public function testGetEbucoreLocator() : void
	{
		$this->assertEquals([], $this->_object->getEbucoreLocator());
		$expected = [(new UriParser())->parse('https://admin.example.com'), (new UriParser())->parse('https://admin.example.com')];
		$this->_object->setEbucoreLocator($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreLocator());
	}
	
	public function testGetEbucoreWidth() : void
	{
		$this->assertNull($this->_object->getEbucoreWidth());
		$expected = 15.2;
		$this->_object->setEbucoreWidth($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreWidth());
	}
	
	public function testGetEbucoreWidthUnit() : void
	{
		$this->assertNull($this->_object->getEbucoreWidthUnit());
		$expected = 'qsdfghjklm';
		$this->_object->setEbucoreWidthUnit($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreWidthUnit());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurResource((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
