<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurReviewValue;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedText;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurReviewValueTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurReviewValue
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurReviewValueTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurReviewValue
	 */
	protected ApiFrDatatourismeDiffuseurReviewValue $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getId());
		$expected = 'qsdfghjklm';
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetSchemaRatingValue() : void
	{
		$this->assertNull($this->_object->getSchemaRatingValue());
		$expected = 'qsdfghjklm';
		$this->_object->setSchemaRatingValue($expected);
		$this->assertEquals($expected, $this->_object->getSchemaRatingValue());
	}
	
	public function testGetRdfsLabel() : void
	{
		$this->assertNull($this->_object->getRdfsLabel());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setRdfsLabel($expected);
		$this->assertEquals($expected, $this->_object->getRdfsLabel());
	}
	
	public function testGetIsCompliantWith() : void
	{
		$this->assertEquals([], $this->_object->getIsCompliantWith());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setIsCompliantWith($expected);
		$this->assertEquals($expected, $this->_object->getIsCompliantWith());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurReviewValue('azertyuiop', ['azertyuiop']);
	}
	
}
