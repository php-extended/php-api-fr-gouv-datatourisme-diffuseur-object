<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurGeo;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurGeoTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurGeo
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurGeoTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurGeo
	 */
	protected ApiFrDatatourismeDiffuseurGeo $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetSchemaElevation() : void
	{
		$this->assertNull($this->_object->getSchemaElevation());
		$expected = 15.2;
		$this->_object->setSchemaElevation($expected);
		$this->assertEquals($expected, $this->_object->getSchemaElevation());
	}
	
	public function testGetSchemaLatitude() : void
	{
		$this->assertNull($this->_object->getSchemaLatitude());
		$expected = 15.2;
		$this->_object->setSchemaLatitude($expected);
		$this->assertEquals($expected, $this->_object->getSchemaLatitude());
	}
	
	public function testGetSchemaLine() : void
	{
		$this->assertNull($this->_object->getSchemaLine());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setSchemaLine($expected);
		$this->assertEquals($expected, $this->_object->getSchemaLine());
	}
	
	public function testGetSchemaLongitude() : void
	{
		$this->assertNull($this->_object->getSchemaLongitude());
		$expected = 15.2;
		$this->_object->setSchemaLongitude($expected);
		$this->assertEquals($expected, $this->_object->getSchemaLongitude());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurGeo((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
