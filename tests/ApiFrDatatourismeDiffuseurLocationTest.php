<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAddress;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurGeo;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurLocation;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOpeningHours;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurLocationTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurLocation
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurLocationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurLocation
	 */
	protected ApiFrDatatourismeDiffuseurLocation $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetSchemaAddress() : void
	{
		$this->assertEquals([], $this->_object->getSchemaAddress());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAddress::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAddress::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setSchemaAddress($expected);
		$this->assertEquals($expected, $this->_object->getSchemaAddress());
	}
	
	public function testGetSchemaGeo() : void
	{
		$this->assertNull($this->_object->getSchemaGeo());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurGeo::class)->disableOriginalConstructor()->getMock();
		$this->_object->setSchemaGeo($expected);
		$this->assertEquals($expected, $this->_object->getSchemaGeo());
	}
	
	public function testGetSchemaOpeningHoursSpecification() : void
	{
		$this->assertEquals([], $this->_object->getSchemaOpeningHoursSpecification());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurOpeningHours::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurOpeningHours::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setSchemaOpeningHoursSpecification($expected);
		$this->assertEquals($expected, $this->_object->getSchemaOpeningHoursSpecification());
	}
	
	public function testGetAltInsee() : void
	{
		$this->assertEquals([], $this->_object->getAltInsee());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setAltInsee($expected);
		$this->assertEquals($expected, $this->_object->getAltInsee());
	}
	
	public function testHasAirConditioning() : void
	{
		$this->assertNull($this->_object->hasAirConditioning());
		$expected = true;
		$this->_object->setAirConditioning($expected);
		$this->assertTrue($this->_object->hasAirConditioning());
	}
	
	public function testHasInternetAccess() : void
	{
		$this->assertNull($this->_object->hasInternetAccess());
		$expected = true;
		$this->_object->setInternetAccess($expected);
		$this->assertTrue($this->_object->hasInternetAccess());
	}
	
	public function testHasNoSmoking() : void
	{
		$this->assertNull($this->_object->hasNoSmoking());
		$expected = true;
		$this->_object->setNoSmoking($expected);
		$this->assertTrue($this->_object->hasNoSmoking());
	}
	
	public function testHasPetsAllowed() : void
	{
		$this->assertNull($this->_object->hasPetsAllowed());
		$expected = true;
		$this->_object->setPetsAllowed($expected);
		$this->assertTrue($this->_object->hasPetsAllowed());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurLocation((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
