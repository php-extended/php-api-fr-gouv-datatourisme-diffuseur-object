<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAddress;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAgent;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurGender;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedProperty;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedText;
use PhpExtended\Email\EmailAddressParser;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurAgentTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAgent
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurAgentTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurAgent
	 */
	protected ApiFrDatatourismeDiffuseurAgent $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetDcIdentifier() : void
	{
		$this->assertNull($this->_object->getDcIdentifier());
		$expected = 'qsdfghjklm';
		$this->_object->setDcIdentifier($expected);
		$this->assertEquals($expected, $this->_object->getDcIdentifier());
	}
	
	public function testGetRdfsComment() : void
	{
		$this->assertNull($this->_object->getRdfsComment());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setRdfsComment($expected);
		$this->assertEquals($expected, $this->_object->getRdfsComment());
	}
	
	public function testGetSchemaAddress() : void
	{
		$this->assertEquals([], $this->_object->getSchemaAddress());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAddress::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAddress::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setSchemaAddress($expected);
		$this->assertEquals($expected, $this->_object->getSchemaAddress());
	}
	
	public function testGetSchemaEmail() : void
	{
		$this->assertEquals([], $this->_object->getSchemaEmail());
		$expected = [(new EmailAddressParser())->parse('admin@example.com'), (new EmailAddressParser())->parse('admin@example.com')];
		$this->_object->setSchemaEmail($expected);
		$this->assertEquals($expected, $this->_object->getSchemaEmail());
	}
	
	public function testGetSchemaFamilyName() : void
	{
		$this->assertEquals([], $this->_object->getSchemaFamilyName());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setSchemaFamilyName($expected);
		$this->assertEquals($expected, $this->_object->getSchemaFamilyName());
	}
	
	public function testGetSchemaFaxNumber() : void
	{
		$this->assertEquals([], $this->_object->getSchemaFaxNumber());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setSchemaFaxNumber($expected);
		$this->assertEquals($expected, $this->_object->getSchemaFaxNumber());
	}
	
	public function testGetSchemaGender() : void
	{
		$this->assertNull($this->_object->getSchemaGender());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurGender::class)->disableOriginalConstructor()->getMock();
		$this->_object->setSchemaGender($expected);
		$this->assertEquals($expected, $this->_object->getSchemaGender());
	}
	
	public function testGetSchemaGivenName() : void
	{
		$this->assertEquals([], $this->_object->getSchemaGivenName());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setSchemaGivenName($expected);
		$this->assertEquals($expected, $this->_object->getSchemaGivenName());
	}
	
	public function testGetSchemaLegalName() : void
	{
		$this->assertNull($this->_object->getSchemaLegalName());
		$expected = 'qsdfghjklm';
		$this->_object->setSchemaLegalName($expected);
		$this->assertEquals($expected, $this->_object->getSchemaLegalName());
	}
	
	public function testGetSchemaTelephone() : void
	{
		$this->assertEquals([], $this->_object->getSchemaTelephone());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setSchemaTelephone($expected);
		$this->assertEquals($expected, $this->_object->getSchemaTelephone());
	}
	
	public function testGetSchemaLogo() : void
	{
		$this->assertNull($this->_object->getSchemaLogo());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setSchemaLogo($expected);
		$this->assertEquals($expected, $this->_object->getSchemaLogo());
	}
	
	public function testGetFoafHomepage() : void
	{
		$this->assertEquals([], $this->_object->getFoafHomepage());
		$expected = [(new UriParser())->parse('https://admin.example.com'), (new UriParser())->parse('https://admin.example.com')];
		$this->_object->setFoafHomepage($expected);
		$this->assertEquals($expected, $this->_object->getFoafHomepage());
	}
	
	public function testGetFoafTitle() : void
	{
		$this->assertEquals([], $this->_object->getFoafTitle());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setFoafTitle($expected);
		$this->assertEquals($expected, $this->_object->getFoafTitle());
	}
	
	public function testGetHasTranslatedProperty() : void
	{
		$this->assertEquals([], $this->_object->getHasTranslatedProperty());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasTranslatedProperty($expected);
		$this->assertEquals($expected, $this->_object->getHasTranslatedProperty());
	}
	
	public function testGetAgreementLicense() : void
	{
		$this->assertNull($this->_object->getAgreementLicense());
		$expected = 'qsdfghjklm';
		$this->_object->setAgreementLicense($expected);
		$this->assertEquals($expected, $this->_object->getAgreementLicense());
	}
	
	public function testGetApeNaf() : void
	{
		$this->assertNull($this->_object->getApeNaf());
		$expected = 'qsdfghjklm';
		$this->_object->setApeNaf($expected);
		$this->assertEquals($expected, $this->_object->getApeNaf());
	}
	
	public function testGetRegisteredNumber() : void
	{
		$this->assertNull($this->_object->getRegisteredNumber());
		$expected = 'qsdfghjklm';
		$this->_object->setRegisteredNumber($expected);
		$this->assertEquals($expected, $this->_object->getRegisteredNumber());
	}
	
	public function testGetSiret() : void
	{
		$this->assertNull($this->_object->getSiret());
		$expected = 'qsdfghjklm';
		$this->_object->setSiret($expected);
		$this->assertEquals($expected, $this->_object->getSiret());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurAgent((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
