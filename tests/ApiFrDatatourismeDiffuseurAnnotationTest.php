<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAnnotation;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedProperty;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedText;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurAnnotationTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAnnotation
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurAnnotationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurAnnotation
	 */
	protected ApiFrDatatourismeDiffuseurAnnotation $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetEbucoreAbstract() : void
	{
		$this->assertNull($this->_object->getEbucoreAbstract());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setEbucoreAbstract($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreAbstract());
	}
	
	public function testGetEbucoreComments() : void
	{
		$this->assertNull($this->_object->getEbucoreComments());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setEbucoreComments($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreComments());
	}
	
	public function testGetEbucoreIsCoveredBy() : void
	{
		$this->assertNull($this->_object->getEbucoreIsCoveredBy());
		$expected = 'qsdfghjklm';
		$this->_object->setEbucoreIsCoveredBy($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreIsCoveredBy());
	}
	
	public function testGetEbucoreTitle() : void
	{
		$this->assertNull($this->_object->getEbucoreTitle());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setEbucoreTitle($expected);
		$this->assertEquals($expected, $this->_object->getEbucoreTitle());
	}
	
	public function testGetCredits() : void
	{
		$this->assertEquals([], $this->_object->getCredits());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setCredits($expected);
		$this->assertEquals($expected, $this->_object->getCredits());
	}
	
	public function testGetHasTranslatedProperty() : void
	{
		$this->assertEquals([], $this->_object->getHasTranslatedProperty());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasTranslatedProperty($expected);
		$this->assertEquals($expected, $this->_object->getHasTranslatedProperty());
	}
	
	public function testGetRightsEndDate() : void
	{
		$this->assertNull($this->_object->getRightsEndDate());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setRightsEndDate($expected);
		$this->assertEquals($expected, $this->_object->getRightsEndDate());
	}
	
	public function testGetRightsStartDate() : void
	{
		$this->assertNull($this->_object->getRightsStartDate());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setRightsStartDate($expected);
		$this->assertEquals($expected, $this->_object->getRightsStartDate());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurAnnotation((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
