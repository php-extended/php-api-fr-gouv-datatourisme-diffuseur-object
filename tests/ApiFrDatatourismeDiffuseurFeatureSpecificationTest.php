<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurFeature;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurFeatureSpecification;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurLayout;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurQuantitativeValue;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurFeatureSpecificationTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurFeatureSpecification
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurFeatureSpecificationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurFeatureSpecification
	 */
	protected ApiFrDatatourismeDiffuseurFeatureSpecification $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetSchemaValue() : void
	{
		$this->assertNull($this->_object->getSchemaValue());
		$expected = 'qsdfghjklm';
		$this->_object->setSchemaValue($expected);
		$this->assertEquals($expected, $this->_object->getSchemaValue());
	}
	
	public function testHasAirConditioning() : void
	{
		$this->assertNull($this->_object->hasAirConditioning());
		$expected = true;
		$this->_object->setAirConditioning($expected);
		$this->assertTrue($this->_object->hasAirConditioning());
	}
	
	public function testHasCharged() : void
	{
		$this->assertNull($this->_object->hasCharged());
		$expected = true;
		$this->_object->setCharged($expected);
		$this->assertTrue($this->_object->hasCharged());
	}
	
	public function testGetFeatures() : void
	{
		$this->assertEquals([], $this->_object->getFeatures());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurFeature::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurFeature::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setFeatures($expected);
		$this->assertEquals($expected, $this->_object->getFeatures());
	}
	
	public function testGetHasFloorSize() : void
	{
		$this->assertNull($this->_object->getHasFloorSize());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurQuantitativeValue::class)->disableOriginalConstructor()->getMock();
		$this->_object->setHasFloorSize($expected);
		$this->assertEquals($expected, $this->_object->getHasFloorSize());
	}
	
	public function testGetHasLayout() : void
	{
		$this->assertEquals([], $this->_object->getHasLayout());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurLayout::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurLayout::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasLayout($expected);
		$this->assertEquals($expected, $this->_object->getHasLayout());
	}
	
	public function testHasInternetAccess() : void
	{
		$this->assertNull($this->_object->hasInternetAccess());
		$expected = true;
		$this->_object->setInternetAccess($expected);
		$this->assertTrue($this->_object->hasInternetAccess());
	}
	
	public function testGetOccupancy() : void
	{
		$this->assertNull($this->_object->getOccupancy());
		$expected = 25;
		$this->_object->setOccupancy($expected);
		$this->assertEquals($expected, $this->_object->getOccupancy());
	}
	
	public function testHasPetsAllowed() : void
	{
		$this->assertNull($this->_object->hasPetsAllowed());
		$expected = true;
		$this->_object->setPetsAllowed($expected);
		$this->assertTrue($this->_object->hasPetsAllowed());
	}
	
	public function testGetSeatCount() : void
	{
		$this->assertNull($this->_object->getSeatCount());
		$expected = 25;
		$this->_object->setSeatCount($expected);
		$this->assertEquals($expected, $this->_object->getSeatCount());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurFeatureSpecification((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
