<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurQuantitativeValue;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurQuantitativeValueTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurQuantitativeValue
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurQuantitativeValueTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurQuantitativeValue
	 */
	protected ApiFrDatatourismeDiffuseurQuantitativeValue $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetSchemaMaxValue() : void
	{
		$this->assertEquals([], $this->_object->getSchemaMaxValue());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setSchemaMaxValue($expected);
		$this->assertEquals($expected, $this->_object->getSchemaMaxValue());
	}
	
	public function testGetSchemaUnitText() : void
	{
		$this->assertEquals([], $this->_object->getSchemaUnitText());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setSchemaUnitText($expected);
		$this->assertEquals($expected, $this->_object->getSchemaUnitText());
	}
	
	public function testGetSchemaValue() : void
	{
		$this->assertNull($this->_object->getSchemaValue());
		$expected = 'qsdfghjklm';
		$this->_object->setSchemaValue($expected);
		$this->assertEquals($expected, $this->_object->getSchemaValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurQuantitativeValue((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
