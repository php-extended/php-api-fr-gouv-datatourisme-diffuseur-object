<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAgent;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurAudience;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurDescription;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurFeatureSpecification;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurGeographicReach;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurLocation;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurMedia;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOffer;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOrderedListSlot;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPeriod;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPointOfInterest;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurQuantitativeValue;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurReview;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTheme;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTourType;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedProperty;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedText;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurPointOfInterestTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurPointOfInterest
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurPointOfInterestTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurPointOfInterest
	 */
	protected ApiFrDatatourismeDiffuseurPointOfInterest $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetDcIdentifier() : void
	{
		$this->assertNull($this->_object->getDcIdentifier());
		$expected = 'qsdfghjklm';
		$this->_object->setDcIdentifier($expected);
		$this->assertEquals($expected, $this->_object->getDcIdentifier());
	}
	
	public function testGetOloSlot() : void
	{
		$this->assertEquals([], $this->_object->getOloSlot());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurOrderedListSlot::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurOrderedListSlot::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setOloSlot($expected);
		$this->assertEquals($expected, $this->_object->getOloSlot());
	}
	
	public function testGetSchemaEndDate() : void
	{
		$this->assertEquals([], $this->_object->getSchemaEndDate());
		$expected = [DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'), DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01')];
		$this->_object->setSchemaEndDate($expected);
		$this->assertEquals($expected, $this->_object->getSchemaEndDate());
	}
	
	public function testGetSchemaStartDate() : void
	{
		$this->assertEquals([], $this->_object->getSchemaStartDate());
		$expected = [DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'), DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01')];
		$this->_object->setSchemaStartDate($expected);
		$this->assertEquals($expected, $this->_object->getSchemaStartDate());
	}
	
	public function testGetRdfsComment() : void
	{
		$this->assertNull($this->_object->getRdfsComment());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setRdfsComment($expected);
		$this->assertEquals($expected, $this->_object->getRdfsComment());
	}
	
	public function testGetRdfsLabel() : void
	{
		$this->assertNull($this->_object->getRdfsLabel());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setRdfsLabel($expected);
		$this->assertEquals($expected, $this->_object->getRdfsLabel());
	}
	
	public function testGetOwlSameAs() : void
	{
		$this->assertEquals([], $this->_object->getOwlSameAs());
		$expected = [(new UriParser())->parse('https://admin.example.com'), (new UriParser())->parse('https://admin.example.com')];
		$this->_object->setOwlSameAs($expected);
		$this->assertEquals($expected, $this->_object->getOwlSameAs());
	}
	
	public function testHasCovid19InOperationConfirmed() : void
	{
		$this->assertNull($this->_object->hasCovid19InOperationConfirmed());
		$expected = true;
		$this->_object->setCovid19InOperationConfirmed($expected);
		$this->assertTrue($this->_object->hasCovid19InOperationConfirmed());
	}
	
	public function testHasCovid19OpeningPeriodsConfirmed() : void
	{
		$this->assertNull($this->_object->hasCovid19OpeningPeriodsConfirmed());
		$expected = true;
		$this->_object->setCovid19OpeningPeriodsConfirmed($expected);
		$this->assertTrue($this->_object->hasCovid19OpeningPeriodsConfirmed());
	}
	
	public function testGetCovid19SpecialMeasures() : void
	{
		$this->assertNull($this->_object->getCovid19SpecialMeasures());
		$expected = 'qsdfghjklm';
		$this->_object->setCovid19SpecialMeasures($expected);
		$this->assertEquals($expected, $this->_object->getCovid19SpecialMeasures());
	}
	
	public function testGetAllowedPersons() : void
	{
		$this->assertNull($this->_object->getAllowedPersons());
		$expected = 25;
		$this->_object->setAllowedPersons($expected);
		$this->assertEquals($expected, $this->_object->getAllowedPersons());
	}
	
	public function testGetAvailableLanguage() : void
	{
		$this->assertEquals([], $this->_object->getAvailableLanguage());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setAvailableLanguage($expected);
		$this->assertEquals($expected, $this->_object->getAvailableLanguage());
	}
	
	public function testGetCreationDate() : void
	{
		$this->assertNull($this->_object->getCreationDate());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setCreationDate($expected);
		$this->assertEquals($expected, $this->_object->getCreationDate());
	}
	
	public function testGetDuration() : void
	{
		$this->assertNull($this->_object->getDuration());
		$expected = 15.2;
		$this->_object->setDuration($expected);
		$this->assertEquals($expected, $this->_object->getDuration());
	}
	
	public function testHasGuided() : void
	{
		$this->assertNull($this->_object->hasGuided());
		$expected = true;
		$this->_object->setGuided($expected);
		$this->assertTrue($this->_object->hasGuided());
	}
	
	public function testGetHasAdministrativeContact() : void
	{
		$this->assertEquals([], $this->_object->getHasAdministrativeContact());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasAdministrativeContact($expected);
		$this->assertEquals($expected, $this->_object->getHasAdministrativeContact());
	}
	
	public function testGetHasAudience() : void
	{
		$this->assertEquals([], $this->_object->getHasAudience());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAudience::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAudience::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasAudience($expected);
		$this->assertEquals($expected, $this->_object->getHasAudience());
	}
	
	public function testGetHasArchitecturalStyle() : void
	{
		$this->assertEquals([], $this->_object->getHasArchitecturalStyle());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTheme::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTheme::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasArchitecturalStyle($expected);
		$this->assertEquals($expected, $this->_object->getHasArchitecturalStyle());
	}
	
	public function testGetHasBeenCreatedBy() : void
	{
		$this->assertNull($this->_object->getHasBeenCreatedBy());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock();
		$this->_object->setHasBeenCreatedBy($expected);
		$this->assertEquals($expected, $this->_object->getHasBeenCreatedBy());
	}
	
	public function testGetHasBeenPublishedBy() : void
	{
		$this->assertEquals([], $this->_object->getHasBeenPublishedBy());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasBeenPublishedBy($expected);
		$this->assertEquals($expected, $this->_object->getHasBeenPublishedBy());
	}
	
	public function testGetHasBookingContact() : void
	{
		$this->assertEquals([], $this->_object->getHasBookingContact());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasBookingContact($expected);
		$this->assertEquals($expected, $this->_object->getHasBookingContact());
	}
	
	public function testGetHasCommunicationContact() : void
	{
		$this->assertEquals([], $this->_object->getHasCommunicationContact());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasCommunicationContact($expected);
		$this->assertEquals($expected, $this->_object->getHasCommunicationContact());
	}
	
	public function testGetHasClientTarget() : void
	{
		$this->assertEquals([], $this->_object->getHasClientTarget());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAudience::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAudience::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasClientTarget($expected);
		$this->assertEquals($expected, $this->_object->getHasClientTarget());
	}
	
	public function testGetHasContact() : void
	{
		$this->assertEquals([], $this->_object->getHasContact());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasContact($expected);
		$this->assertEquals($expected, $this->_object->getHasContact());
	}
	
	public function testGetHasDescription() : void
	{
		$this->assertEquals([], $this->_object->getHasDescription());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurDescription::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurDescription::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasDescription($expected);
		$this->assertEquals($expected, $this->_object->getHasDescription());
	}
	
	public function testGetHasFeature() : void
	{
		$this->assertEquals([], $this->_object->getHasFeature());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurFeatureSpecification::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurFeatureSpecification::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasFeature($expected);
		$this->assertEquals($expected, $this->_object->getHasFeature());
	}
	
	public function testGetHasFloorSize() : void
	{
		$this->assertNull($this->_object->getHasFloorSize());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurQuantitativeValue::class)->disableOriginalConstructor()->getMock();
		$this->_object->setHasFloorSize($expected);
		$this->assertEquals($expected, $this->_object->getHasFloorSize());
	}
	
	public function testGetHasGeographicReach() : void
	{
		$this->assertEquals([], $this->_object->getHasGeographicReach());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurGeographicReach::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurGeographicReach::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasGeographicReach($expected);
		$this->assertEquals($expected, $this->_object->getHasGeographicReach());
	}
	
	public function testGetHasMainRepresentation() : void
	{
		$this->assertEquals([], $this->_object->getHasMainRepresentation());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurMedia::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurMedia::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasMainRepresentation($expected);
		$this->assertEquals($expected, $this->_object->getHasMainRepresentation());
	}
	
	public function testGetHasManagementContact() : void
	{
		$this->assertEquals([], $this->_object->getHasManagementContact());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasManagementContact($expected);
		$this->assertEquals($expected, $this->_object->getHasManagementContact());
	}
	
	public function testGetHasNeighborhood() : void
	{
		$this->assertEquals([], $this->_object->getHasNeighborhood());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTheme::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTheme::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasNeighborhood($expected);
		$this->assertEquals($expected, $this->_object->getHasNeighborhood());
	}
	
	public function testGetHasRepresentation() : void
	{
		$this->assertEquals([], $this->_object->getHasRepresentation());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurMedia::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurMedia::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasRepresentation($expected);
		$this->assertEquals($expected, $this->_object->getHasRepresentation());
	}
	
	public function testGetHasReview() : void
	{
		$this->assertEquals([], $this->_object->getHasReview());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurReview::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurReview::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasReview($expected);
		$this->assertEquals($expected, $this->_object->getHasReview());
	}
	
	public function testGetHasTheme() : void
	{
		$this->assertEquals([], $this->_object->getHasTheme());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTheme::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTheme::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasTheme($expected);
		$this->assertEquals($expected, $this->_object->getHasTheme());
	}
	
	public function testGetHasTourType() : void
	{
		$this->assertEquals([], $this->_object->getHasTourType());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTourType::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTourType::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasTourType($expected);
		$this->assertEquals($expected, $this->_object->getHasTourType());
	}
	
	public function testGetHasTranslatedProperty() : void
	{
		$this->assertEquals([], $this->_object->getHasTranslatedProperty());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasTranslatedProperty($expected);
		$this->assertEquals($expected, $this->_object->getHasTranslatedProperty());
	}
	
	public function testGetHighDifference() : void
	{
		$this->assertNull($this->_object->getHighDifference());
		$expected = 15.2;
		$this->_object->setHighDifference($expected);
		$this->assertEquals($expected, $this->_object->getHighDifference());
	}
	
	public function testGetIsLocatedAt() : void
	{
		$this->assertEquals([], $this->_object->getIsLocatedAt());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurLocation::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurLocation::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setIsLocatedAt($expected);
		$this->assertEquals($expected, $this->_object->getIsLocatedAt());
	}
	
	public function testGetIsOwnedBy() : void
	{
		$this->assertEquals([], $this->_object->getIsOwnedBy());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurAgent::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setIsOwnedBy($expected);
		$this->assertEquals($expected, $this->_object->getIsOwnedBy());
	}
	
	public function testGetLastUpdate() : void
	{
		$this->assertNull($this->_object->getLastUpdate());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setLastUpdate($expected);
		$this->assertEquals($expected, $this->_object->getLastUpdate());
	}
	
	public function testGetLastUpdateDatatourisme() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), $this->_object->getLastUpdateDatatourisme());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setLastUpdateDatatourisme($expected);
		$this->assertEquals($expected, $this->_object->getLastUpdateDatatourisme());
	}
	
	public function testGetMaxAltitude() : void
	{
		$this->assertNull($this->_object->getMaxAltitude());
		$expected = 15.2;
		$this->_object->setMaxAltitude($expected);
		$this->assertEquals($expected, $this->_object->getMaxAltitude());
	}
	
	public function testGetMinAltitude() : void
	{
		$this->assertNull($this->_object->getMinAltitude());
		$expected = 15.2;
		$this->_object->setMinAltitude($expected);
		$this->assertEquals($expected, $this->_object->getMinAltitude());
	}
	
	public function testGetOffers() : void
	{
		$this->assertEquals([], $this->_object->getOffers());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurOffer::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurOffer::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setOffers($expected);
		$this->assertEquals($expected, $this->_object->getOffers());
	}
	
	public function testGetProvidesCuisineOfType() : void
	{
		$this->assertEquals([], $this->_object->getProvidesCuisineOfType());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTheme::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTheme::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setProvidesCuisineOfType($expected);
		$this->assertEquals($expected, $this->_object->getProvidesCuisineOfType());
	}
	
	public function testGetProvidesFoodProduct() : void
	{
		$this->assertEquals([], $this->_object->getProvidesFoodProduct());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTheme::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTheme::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setProvidesFoodProduct($expected);
		$this->assertEquals($expected, $this->_object->getProvidesFoodProduct());
	}
	
	public function testHasReducedMobilityAccess() : void
	{
		$this->assertNull($this->_object->hasReducedMobilityAccess());
		$expected = true;
		$this->_object->setReducedMobilityAccess($expected);
		$this->assertTrue($this->_object->hasReducedMobilityAccess());
	}
	
	public function testHasSaleOnSite() : void
	{
		$this->assertNull($this->_object->hasSaleOnSite());
		$expected = true;
		$this->_object->setSaleOnSite($expected);
		$this->assertTrue($this->_object->hasSaleOnSite());
	}
	
	public function testHasTakeAway() : void
	{
		$this->assertNull($this->_object->hasTakeAway());
		$expected = true;
		$this->_object->setTakeAway($expected);
		$this->assertTrue($this->_object->hasTakeAway());
	}
	
	public function testGetTakesPlaceAt() : void
	{
		$this->assertEquals([], $this->_object->getTakesPlaceAt());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurPeriod::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurPeriod::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setTakesPlaceAt($expected);
		$this->assertEquals($expected, $this->_object->getTakesPlaceAt());
	}
	
	public function testGetTourDistance() : void
	{
		$this->assertNull($this->_object->getTourDistance());
		$expected = 15.2;
		$this->_object->setTourDistance($expected);
		$this->assertEquals($expected, $this->_object->getTourDistance());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurPointOfInterest((new UriParser())->parse('https://test.example.com'), ['azertyuiop'], DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'));
	}
	
}
