<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-datatourisme-diffuseur-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDatatourismeDiffuseur\Test;

use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurLocation;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurMedia;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOrderedListItem;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedProperty;
use PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurTranslatedText;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrDatatourismeDiffuseurOrderedListItemTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrDatatourismeDiffuseur\ApiFrDatatourismeDiffuseurOrderedListItem
 * @internal
 * @small
 */
class ApiFrDatatourismeDiffuseurOrderedListItemTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrDatatourismeDiffuseurOrderedListItem
	 */
	protected ApiFrDatatourismeDiffuseurOrderedListItem $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals((new UriParser())->parse('https://test.example.com'), $this->_object->getId());
		$expected = (new UriParser())->parse('https://admin.example.com');
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetType() : void
	{
		$this->assertEquals(['azertyuiop'], $this->_object->getType());
		$expected = ['qsdfghjklm', 'qsdfghjklm'];
		$this->_object->setType($expected);
		$this->assertEquals($expected, $this->_object->getType());
	}
	
	public function testGetDcDescription() : void
	{
		$this->assertNull($this->_object->getDcDescription());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setDcDescription($expected);
		$this->assertEquals($expected, $this->_object->getDcDescription());
	}
	
	public function testGetRdfsLabel() : void
	{
		$this->assertNull($this->_object->getRdfsLabel());
		$expected = $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedText::class)->disableOriginalConstructor()->getMock();
		$this->_object->setRdfsLabel($expected);
		$this->assertEquals($expected, $this->_object->getRdfsLabel());
	}
	
	public function testGetDuration() : void
	{
		$this->assertNull($this->_object->getDuration());
		$expected = 15.2;
		$this->_object->setDuration($expected);
		$this->assertEquals($expected, $this->_object->getDuration());
	}
	
	public function testGetHighDifference() : void
	{
		$this->assertNull($this->_object->getHighDifference());
		$expected = 15.2;
		$this->_object->setHighDifference($expected);
		$this->assertEquals($expected, $this->_object->getHighDifference());
	}
	
	public function testGetIsLocatedAt() : void
	{
		$this->assertEquals([], $this->_object->getIsLocatedAt());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurLocation::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurLocation::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setIsLocatedAt($expected);
		$this->assertEquals($expected, $this->_object->getIsLocatedAt());
	}
	
	public function testGetHasMainRepresentation() : void
	{
		$this->assertEquals([], $this->_object->getHasMainRepresentation());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurMedia::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurMedia::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasMainRepresentation($expected);
		$this->assertEquals($expected, $this->_object->getHasMainRepresentation());
	}
	
	public function testGetHasRepresentation() : void
	{
		$this->assertEquals([], $this->_object->getHasRepresentation());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurMedia::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurMedia::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasRepresentation($expected);
		$this->assertEquals($expected, $this->_object->getHasRepresentation());
	}
	
	public function testGetHasTranslatedProperty() : void
	{
		$this->assertEquals([], $this->_object->getHasTranslatedProperty());
		$expected = [$this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrDatatourismeDiffuseurTranslatedProperty::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setHasTranslatedProperty($expected);
		$this->assertEquals($expected, $this->_object->getHasTranslatedProperty());
	}
	
	public function testGetTourDistance() : void
	{
		$this->assertNull($this->_object->getTourDistance());
		$expected = 15.2;
		$this->_object->setTourDistance($expected);
		$this->assertEquals($expected, $this->_object->getTourDistance());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrDatatourismeDiffuseurOrderedListItem((new UriParser())->parse('https://test.example.com'), ['azertyuiop']);
	}
	
}
